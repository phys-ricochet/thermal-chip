writename = "Optimization_Results_80mK_Zn_at_10mK_MIT_thin_film_conductance_varying_gold_thickness_1.txt" # create file name for the results to be written to
readname = "file_names copy.txt" # file to read the filenames from 
w = open(writename,"w+") #create the write file
r = open(readname,"r") #create the read file
l = [line.rstrip('\r') for line in open(readname)] #read in the lines, splitting off the carriage return
lines = l[0].split("\r") #split the one string of lines into the lines themselves (('1 2 3 4 5') ---> (('1','2','3','4','5')))
print(lines) #print the lines to make sure we got it right
for line in lines: #write the contents of each file to the main file
    f = open(line,"r")
    w.write(f.read())
w.close()
    