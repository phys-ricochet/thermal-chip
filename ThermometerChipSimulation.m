%% These are the variable definitions for each component of the thermal calorimeter
%% Hello Git
%% Temperatures to be solved for:
T_Target = 0.1;
T_GoldPad2 = 0.0;
T_WireBond = 0.0;
T_SiliconChip = 0.0;
T_TES = 0.0;
T_GoldPad3 = 0.0;
T_Bath = 0.0; %% Fixed thermal bath constant
T_Glue = 0.0;
T_GoldPad1 = 0.0;

%Heat Capacities in microJoules/Kelvin
C = 6.44e-4;  %% target heat capacity
C_Glue = 6.87e-5;
C_GoldPad1 = 4.47e-5;
C_GoldPad2 = 1.12e-7;
C_WireBond = 2.81e-4;
C_SiliconChip = 1.93e-6;
C_TES = 2.54e-7;
C_GoldPad3 = 1.12e-7;

%Thermal Conductances in miliWatts/Kelvin
G_TargetBath = 0; %% target-bath coupling
G_TargetGlue = 5.14e-3;
G_TargetGoldPad1 = 1.56e-2;
G_WireBondGoldPad1 = 1.396e-1;
G_WireBondGoldPad2 = 1.396e-1;
G_SiliconGlue = 5.14e-3;
G_SiliconGoldPad2 = 3.91e-5;
G_SiliconTES = 4.68e-6;
G_SiliconGoldPad3 = 3.91e-5;
G_GoldPad2TES = 1.1e-2;
G_GoldPad3TES = 8.65e-2;
G_GoldPad3Bath = 0.0003; %parameter to be varried
%% Now we must represent the geometry of the calorimeter in terms of a matrix to be diagonalized
%% The matrix reads -i*G where G is:

G = [-(G_TargetBath+G_TargetGlue+G_TargetGoldPad1)/C,G_TargetGlue/C,G_TargetGoldPad1/C,0,0,0,0,0;G_TargetGlue/C_Glue,-(G_TargetGlue+G_SiliconGlue)/C_Glue,0,0,0,G_SiliconGlue/C_Glue,0,0;G_TargetGoldPad1/C_GoldPad1,0,-(G_TargetGoldPad1+G_WireBondGoldPad1)/C_GoldPad1,G_WireBondGoldPad1/C_GoldPad1,0,0,0,0;0,0,G_WireBondGoldPad1/C_WireBond,-(G_WireBondGoldPad1+G_WireBondGoldPad2)/C_WireBond,G_WireBondGoldPad2/C_WireBond,0,0,0;0,0,0,G_WireBondGoldPad2/C_GoldPad2,-(G_WireBondGoldPad2+G_SiliconGoldPad2+G_GoldPad2TES)/C_GoldPad2,G_SiliconGoldPad2/C_GoldPad2,G_GoldPad2TES/C_GoldPad2,0;0,G_SiliconGlue/C_SiliconChip,0,0,G_SiliconGoldPad2/C_SiliconChip,-(G_SiliconGoldPad2+G_SiliconGlue+G_SiliconTES+G_SiliconGoldPad3)/C_SiliconChip,G_SiliconTES/C_SiliconChip,G_SiliconGoldPad3/C_SiliconChip;0,0,0,0,G_GoldPad2TES/C_TES,G_SiliconTES/C_TES,-(G_SiliconTES+G_GoldPad2TES+G_GoldPad3TES)/C_TES,G_GoldPad3TES/C_TES;0,0,0,0,0,G_SiliconGoldPad3/C_GoldPad3,G_GoldPad3TES/C_GoldPad3,-(G_SiliconGoldPad3+G_GoldPad3TES+G_GoldPad3Bath)/C_GoldPad3]%Removed G_GoldPad2TES/C_GoldPad2, was not present in Brian's notes
G = -(1i)*G;
% Solve for the eigenmodes of the linearized motion
[mode,w] = eig(G);

T0 = [T_Target,T_Glue,T_GoldPad1,T_WireBond,T_GoldPad2,T_SiliconChip,T_TES,T_GoldPad3];

%% To obtain the exact flow of heat, we must solve for the coefficients c_n of the general solution
%% from the initial state

C = linsolve(mode,T0.');

%% Thus the most general solution is:
t = linspace(0,20,1600);
imag(diag(w))
Toft = zeros([length(t),8]);
for k = 1:8
    for n = 1:8
        Toft(:,k) = Toft(:,k) + C(n)*mode(k,n)*exp(1i*w(n,n).*t.');
    end
    Toft(:,k) = real(Toft(:,k)); %% at the end of the day, take the real part
end

plot(t,Toft(:,1),t,Toft(:,2),t,Toft(:,3),t,Toft(:,4),t,Toft(:,5),t,Toft(:,6),t,Toft(:,7),t,Toft(:,8))
title('Thermometer Chip Response')
xlabel('Time in ms')
ylabel('Temperature above bath in K')
legend('Target','Glue','Gold Pad 1','Wire Bond','Gold Pad 2','Silicon Chip','TES','Gold Pad 3')
%% Now we will consider the steady state solution with an oscillating drive, after transients have died out
G = 1i*G;

% wd = 0.53059;
% 
% D = det(1i*wd*eye(8)-G);
% for k = 1:8
%     Toft(:,k) = Toft(:,k) + real(D^-0.5*exp(1i*wd.*t.'));
% end
 
