import numpy as np  # Import Numpy
import matplotlib.pyplot as plt # Import plotting functions
from scipy.integrate import odeint # Import Nonlinear Solver
np.set_printoptions(threshold=np.nan) #Let us print the full array if necessary
import yaml

#Use YAML to read the input file and turn it into a Python Dictionary
stream = open("input copy with TES phonon System still in.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')

### Derived Parameters ###

### Electrical Components ###
p['R0']     =p['Rn'] * p['frac']          #Resistance at equilibrium

### Heat Capacity for Igor ###
#else_heat_cap = p['ca']+p['cau1']+p['cg'] +p['cwb1']+p['cau2']+p['csi']+p['ctp']+p['cm']
#total_cap = else_heat_cap + p['cwb2'] + p['cm']
#print("capacity",else_heat_cap)

### Conductivity for Igor ###
#tes_abs_G = 1/(1/p['G_aau1']+2/p['G_au1wb1']+1/p['G_au2te'])
#total_conductance = tes_abs_G + p['G_mwb2'] + p['G_wb2b'] 
#print("conductivity",tes_abs_G)

### Conductance Constants ###

p['kab']    = p['G_ab']/(p['nab']*p['Tc']**(p['nab']-1.))                #Conductance Constant Absorber to Bath [W/K^4]
p['kaau1']  = p['G_aau1']/(p['naau1']*p['Tc']**(p['naau1']-1.))           #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
p['kag']    = p['G_ag']/(p['nag']*p['Tc']**(p['nag']-1.))            #Conductance Constant Absorber to Glue [W/K^3.5]
p['kau1wb1']= p['G_au1wb1']/(p['nau1wb1']*p['Tc']**(p['nau1wb1']-1.))         #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
p['kgsi']   = p['G_gsi']/(p['ngsi']*p['Tc']**(p['ngsi']-1.))           #Conductance Constant Glue to Silicon [W/K^3.5]
p['kwb1au2']= p['G_wb1au2']/(p['nwb1au2']*p['Tc']**(p['nwb1au2']-1.))         #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
p['ksiau2'] = p['G_siau2']/(p['nsiau2']*p['Tc']**(p['nsiau2']-1.))      #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
p['kau2te'] = p['G_au2te']/(p['nau2te']*p['Tc']**(p['nau2te']-1.))          #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
p['ksim']   = p['G_sim']/(p['nsim']*p['Tc']**(p['nsim']-1.))           #Conductance Constant Silicon to Meander [W/K^4]
p['ksite']  = p['G_site']/(p['nsite']*p['Tc']**(p['nsite']-1.))           #Conductance Constant TES Phonon System to TES Electron System [W/K^6]
p['ktem']   = p['G_tem']/(p['ntem']*p['Tc']**(p['ntem']-1.))            #Conductance Constant TES Electron System to MEander [W/K^2]
p['kmwb2']  = p['G_mwb2']/(p['nmwb2']*p['Tc']**(p['nmwb2']-1.))           #Conductance Constant Meander to Wirebond 2 [W/K^2]
p['kwb2b']  = p['G_wb2b']/(p['nwb2b']*p['Tc']**(p['nwb2b']-1.))           #Conductance Constant Wirecond 2 to Bath [W/K^4]

###Power Calculation ###

p['P0']     = p['kmwb2']*((p['Tc'])**2-(p['Tb'])**2)  #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
p['I0_P']   = np.sqrt(p['P0']/p['R0'])              #Equilibrium Current calculated from power, used in resistance equation
p['vb']     = p['I0_P'] * p['Rl']                    #Voltage Bias
#print("P0",p['P0'],"I0",p['I0_P'],"vb",p['vb'])          #Print the equilibrium power, current, and bias voltage





### Definition of Functions ###


### Define the resistance function ###

def resistance(Ttes,Ites):
    R = p['Rn']/2 *(1 + np.tanh((Ttes - p['Tc']) * p['alpha0']/p['Tc'] + (Ites - p['I0_P'])*p['beta0']/p['I0_P']))
    return R

### Set up the Nonliear ODEs ###

#vectorfield(variables,time array)
def vectorfield(w,t):
    ### Variables ###
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Tm,Twb2 = w 
    ### Array of the functions to solve ###
    f1 = [(p['I0_P']*p['R0'] - I*resistance(Tte,I))/p['L'],
         (((p['E']/(p['c']*np.sqrt(2*np.pi)))*np.exp(-(t - p['t0'])**2/(2*p['c']**2)))-p['kab']*(Ta**p['nab']-p['Tb']**p['nab'])-p['kaau1']*(Ta**p['naau1']-Tau1**p['naau1'])-p['kag']*(Ta**p['nag']-Tg**p['nag']))/p['ca'], 
         (-p['kaau1']*(Tau1**p['naau1']-Ta**p['naau1'])-p['kau1wb1']*(Tau1**p['nau1wb1']-Twb1**p['nau1wb1']))/p['cau1'],
         (-p['kag']*(Tg**p['nag']-Ta**p['nag'])-p['kgsi']*(Tg**p['ngsi']-Tsi**p['ngsi']))/p['cg'],
         (-p['kau1wb1']*(Twb1**p['nau1wb1']-Tau1**p['nau1wb1'])-p['kwb1au2']*(Twb1**p['nwb1au2']-Tau2**p['nwb1au2']))/p['cwb1'],
         (-p['ksiau2']*(Tau2**p['nsiau2']-Tsi**p['nsiau2'])-p['kwb1au2']*(Tau2**p['nwb1au2']-Twb1**p['nwb1au2'])-p['kau2te']*(Tau2**p['nau2te']-Tte**p['nau2te']))/p['cau2'],
         (-p['kgsi']*(Tsi**p['ngsi']-Tg**p['ngsi']) - p['ksiau2']*(Tsi**p['nsiau2']-Tau2**p['nsiau2'])-p['ksite']*(Tsi**p['nsite']-Tte**p['nsite'])-p['ksim']*(Tsi**p['nsim']-Tm**p['nsim']))/p['csi'],
         ((I**2.*resistance(Tte,I)-p['kau2te']*(Tte**p['nau2te']-Tau2**p['nau2te'])-p['ksite']*(Tte**p['nsite']-Tsi**p['nsite'])-p['ktem']*(Tte**p['ntem']-Tm**p['ntem'])))/p['cte'],
         (-p['ktem']*(Tm**p['ntem']-Tte**p['ntem'])-p['ksim']*(Tm**p['nsim']-Tsi**p['nsim'])-p['kmwb2']*(Tm**p['nmwb2']-Twb2**p['nmwb2']))/p['cm'],
         (-p['kmwb2']*(Twb2**p['nmwb2']-Tm**p['nmwb2'])-p['kwb2b']*(Twb2**p['nwb2b']-(p['Tb']**p['nwb2b'])))/p['cwb2']] 
    #print(t)
    #print I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2
    #print(f1)
    #print("--------------------")
    return f1





### Solve the System of ODEs ###

t = [p['stoptime'] * float(i) / (p['numpoints'] - 1) for i in range(int(p['numpoints']))] #Make an array of the time points

#Put the initial conditions into an array

#      0  1   2   3   4    5   6   7   8   9  
#w0 = [I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Tm,Twb2]

#Initials from Root Finder
#w0 = [  1.73184513e-06,  2.00000237e-02,   2.00000237e-02,   2.00000236e-02,2.00000237e-02,   2.00000237e-02,   2.00000236e-02,   2.00000237e-02,   1.99999371e-02,   1.00034358e-02]

#initials from nonlinear solver July 18 2017
#w0 = [1.73165509122e-06,0.0200000913177,0.0200000913944,0.0200000912899,0.0200000913953,0.0200000913961,0.020000091262,0.0200000913972,0.0200000047124,0.0100034358387]

#initials from nonlinear solver July 20 2017
#w0 = [2.12488267315e-06,0.0199176837721,0.01991728961,0.01991778612,0.0199172753884,0.0199172579325,0.019917785774,0.0199172349809,0.0199171210924,0.00206110380898]

#w0 = [  1.73199196e-06,   2.00000016e-02,   2.00000068e-02,   2.00000013e-02,2.00000068e-02,   2.00000068e-02,   2.00000009e-02,   2.00000068e-02,1.99999201e-02,   1.00008595e-02]
w0 = [1.7319375842e-06,0.0200000209981,0.0200000261099,0.0200000206123,0.0200000261215,0.0200000261332,0.0200000202265,0.0200000261487,0.0199999394538,0.0100008595057]

### Run the ODE Solver ###
#odeint(equation function,initial condition,time array,absolute error,relative error)
wsol = odeint(vectorfield, w0, t,full_output = 0,atol=p['abserr'], rtol=p['relerr'],mxstep = 1000000,hmax = 0.0000001)


#print(wsol)


### Plot the results ###


### Transpose the solution so we can easily plot it using the map function as in the linear case ###
solution = np.transpose(wsol)

### Separate out the solution into individual arrays that can be plotted ###
I, Ta, Tau1, Tg, Twb1, Tau2, Tsi, Tte, Tm, Twb2 = map(np.array, solution)

### Output to an NPZ file, named above ###
save_name = 'NPZ of Nonlinear Data' #+ identifier #Create an NPZ filename 
np.savez(save_name, time = t, current = I, absorber_temp = Ta, gold_pad_1_temp = Tau1, glue_temp = Tg, wirebond_1_temp = Twb1, gold_pad_2_temp = Tau2, silicon_temp = Tsi, tes_electron_temp = Tte, meander_temp = Tm, wirebond_2_temp = Twb2) #index = index


fig = plt.figure(figsize=(18, 18)) #Make the plots bigger

#Plot flag
plot_flag = 1

if plot_flag == 0:
    print("Thanks for not making me plot")

if plot_flag == 1:

    ### Linear Plot ###
    
    #Plot temperatures
    plt.subplot(5,1,1)
    plt.plot(Ta,label="Absorber Temperature")
    plt.plot(Tau1,label="Gold Pad 1 Temperature") 
    plt.plot(Twb1,label="Wirebond 1 Temperature")
    plt.plot(Tg,label="Glue Temperature")
    plt.plot(Tsi,label="Silicon Chip Temperature")
    plt.plot(Tau2,label="Gold Pad 2 Temperature")
    #plt.plot(Tm,label="Meander Temperature")
    #plt.plot(Twb2,label="Wirebond 2 Temperature")
    plt.plot(Tte,label="TES Electron System Temperature",color = "k")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.title("Nonlinear Solver")
    #plt.legend()
    
    #Plot Current
    plt.subplot(5,1,2)
    plt.plot(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    
    plt.subplot(5,1,3)
    plt.plot(resistance(Tte,I),label = "Resistance")
    plt.ylabel("Resistance [Ohms]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #plot power
    plt.subplot(5,1,4)
    plt.plot((I**2)*resistance(Tte,I),label = "Power")
    plt.ylabel("Power [Watts]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    plt.show() #Display the figure

if plot_flag == 2:
    
    ### Log Plot ###
    
    #Plot temperatures
    plt.subplot(5,1,1)
    plt.semilogy(Ta,label="Absorber Temperature")
    plt.semilogy(Tau1,label="Gold Pad 1 Temperature") 
    plt.semilogy(Twb1,label="Wirebond 1 Temperature")
    plt.semilogy(Tg,label="Glue Temperature")
    plt.semilogy(Tsi,label="Silicon Chip Temperature")
    plt.semilogy(Tte,label="TES Electron System Temperature")
    plt.semilogy(Tau2,label="Gold Pad 2 Temperature")
    #plt.semilogy(Tm,label="Meander Temperature")
    #plt.semilogy(Twb2,label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.title("Nonlinear Solver Log 2mK Pulse")
    plt.legend()
    
    #Plot Current
    plt.subplot(5,1,2)
    plt.semilogy(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    
    #Plot Resistance
    plt.subplot(5,1,3)
    plt.semilogy(resistance(Tte,I),label = "Resistance")
    plt.ylabel("Resistance [Ohms]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #plot power
    plt.subplot(5,1,4)
    plt.semilogy((I**2)*resistance(Tte,I),label = "Power")
    plt.ylabel("Power [Watts]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    plt.show() #Display the figure
    
if plot_flag == 3:
    
    ### Plot mostly individually ###
    
    #Plot absorber Temperature
    plt.subplot(10,1,1)
    plt.plot(Ta,label="Absorber Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    plt.title("Nonlinear Solver")
    fig.subplots_adjust(hspace=.5)
    
    #Plot Current
    plt.subplot(10,1,2)
    plt.plot(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
    plt.subplot(10,1,3)
    plt.plot(Tau1,label="Gold Pad 1 Temperature") 
    plt.plot(Twb1,label="Wirebond 1 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Glue Temperature on same graph
    plt.subplot(10,1,4)
    plt.plot(Tg,label="Glue Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Silicon chip Temperature
    plt.subplot(10,1,5)
    plt.plot(Tsi,label="Silicon Chip Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot TES Electron and Phonon System Temperatures on same graph
    plt.subplot(10,1,6)
    plt.plot(Tte,label="TES Electron System Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 2 Temperature
    plt.subplot(10,1,7)
    plt.plot(Tau2,label="Gold Pad 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Meander and Wirebond 2 Temperatures on same graph
    plt.subplot(10,1,8)
    plt.plot(Tm,label="Meander Temperature")
    plt.plot(Twb2,label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    plt.show() #Display the figure

if plot_flag == 4:
    lower = 9000
    upper = 15000
    ### Pulse displaced from t=0 ###
    
    #Plot absorber Temperature
    plt.subplot(10,1,1)
    plt.plot(Ta[lower:upper],label="Absorber Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    plt.title("Nonlinear Solver 1KeV Split gmwb2_3e-8 11 July 2017 corrected Cs")
    fig.subplots_adjust(hspace=.5)
    
    #Plot Current
    plt.subplot(10,1,2)
    plt.plot(I[lower:upper],label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
    plt.subplot(10,1,3)
    plt.plot(Tau1[lower:upper],label="Gold Pad 1 Temperature") 
    plt.plot(Twb1[lower:upper],label="Wirebond 1 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Glue Temperature on same graph
    plt.subplot(10,1,4)
    plt.plot(Tg[lower:upper],label="Glue Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Silicon chip Temperature
    plt.subplot(10,1,5)
    plt.plot(Tsi[lower:upper],label="Silicon Chip Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot TES Electron and Phonon System Temperatures on same graph
    plt.subplot(10,1,6)
    plt.plot(Tte[lower:upper],label="TES Electron System Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 2 Temperature
    plt.subplot(10,1,7)
    plt.plot(Tau2[lower:upper],label="Gold Pad 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Meander and Wirebond 2 Temperatures on same graph
    plt.subplot(10,1,8)
    plt.plot(Tm[lower:upper],label="Meander Temperature")
    plt.plot(Twb2[lower:upper],label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
#plt.savefig('Nonlinear Solver 1KeV Split gmwb2_3e-8 11 July 2017 corrected Cs.png')



### Print eq values ###

print(I[-1])
print(Ta[-1])
print(Tau1[-1])
print(Tg[-1])
print(Twb1[-1])
print(Tau2[-1])
print(Tsi[-1])
print(Tte[-1])
print(Tm[-1])
print(Twb2[-1])
print("resistance",resistance(Tte[-1],I[-1]))
print("power",resistance(Tte[-1],I[-1])*I[-1])

