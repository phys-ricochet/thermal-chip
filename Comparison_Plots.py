import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object
import yaml
import matplotlib
from scipy.optimize import curve_fit

matplotlib.rc('axes.formatter', useoffset=False)

stream = open("input full 20mK optimum.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')

npzname = '20mK_main_floor_division'
linear_flag = 0
nonlinear_flag = 1
freq_flag = 0
save_fig_flag = 0
savename = "20mK_main_floor_division"

stop = 4000

kmwb2       = p['G_mwb2']/(p['nmwb2']*p['Tc']**(p['nmwb2']-1.))                 #Conductance Constant Meander to Wirebond 2 [W/K^2]
R0          = p['Rn']  * p['frac']                       #Equilibrium resistance of TES [Ohms]
P_0         = p['I0_NL']**2* R0                      #Equilibrium power through TES [J]
V_0         = p['I0_NL']* R0                         #Equilibrium Voltage [V]
P0          = kmwb2 *((p['Tc'])**2-(p['Tb'])**2)    #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
I0_P        = np.sqrt( P0 / R0 )                  #Equilibrium Current calculated from power, used in resistance equation

data = np.load('NPZ of Nonlinear Data ' + npzname + '.npz')
I = data['current']
Ta = data['absorber_temp']
Tau1 = data['gold_pad_1_temp']
Tg = data['glue_temp']
Twb1 = data['wirebond_1_temp']
Tau2 = data['gold_pad_2_temp']
Tsi = data['silicon_temp']
Tte = data['tes_electron_temp']
Tm = data['meander_temp']
Twb2 = data['wirebond_2_temp']
ODEt = data['time']
del data.f
data.close()

npzname = '20mK_main_NO_floor_division'
linear_flag = 0
nonlinear_flag = 1
freq_flag = 0
save_fig_flag = 0
savename = "20mK_main_NO_floor_division"

stop = 4000

kmwb2       = p['G_mwb2']/(p['nmwb2']*p['Tc']**(p['nmwb2']-1.))                 #Conductance Constant Meander to Wirebond 2 [W/K^2]
R0          = p['Rn']  * p['frac']                       #Equilibrium resistance of TES [Ohms]
P_0         = p['I0_NL']**2* R0                      #Equilibrium power through TES [J]
V_0         = p['I0_NL']* R0                         #Equilibrium Voltage [V]
P0          = kmwb2 *((p['Tc'])**2-(p['Tb'])**2)    #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
I0_P        = np.sqrt( P0 / R0 )                  #Equilibrium Current calculated from power, used in resistance equation

data = np.load('NPZ of Nonlinear Data ' + npzname + '.npz')
I2 = data['current']
Ta2 = data['absorber_temp']
Tau12 = data['gold_pad_1_temp']
Tg2 = data['glue_temp']
Twb12 = data['wirebond_1_temp']
Tau22 = data['gold_pad_2_temp']
Tsi2 = data['silicon_temp']
Tte2 = data['tes_electron_temp']
Tm2 = data['meander_temp']
Twb22 = data['wirebond_2_temp']
ODEt2 = data['time']
del data.f
data.close()


I0_P = I[0]
def resistance(Ttes,Ites):
    R = p['Rn']/2 *(1. + np.tanh((Ttes - p['Tc']) * p['alpha0']/p['Tc'] + (Ites - I0_P)*p['beta0'] / I0_P))
    return R


ODEt = np.array(ODEt) #make sure that the nonlinear solver times are in an array

if nonlinear_flag == 1:
    fig = plt.figure(figsize=(9, 15)) #make the plots bigger

    ### Linear Plot ###
    
    #Plot temperatures
    plt.subplot(5,1,1)
    #plt.plot(1000*(ODEt[0:stop] - p['t0']),1e3*Ta[0:stop],label="Absorber Temperature", linewidth = 3)
    #plt.plot(ODEt - p['t0'],1e3*Tau1,label="Gold Pad 1 Temperature") 
    #plt.plot(ODEt - p['t0'],1e3*Twb1,label="Wirebond 1 Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tg,label="Glue Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tsi,label="Silicon Chip Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tau2,label="Gold Pad 2 Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tm,label="Meander Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Twb2,label="Wirebond 2 Temperature")
    plt.plot(1000*(ODEt[0:stop] - p['t0']),1e6*(Tte[0:stop]-0.020000005449227316),label="TES Electron System Temperature Old", linewidth = 1,color = "k")
    plt.plot(1000*(ODEt[0:stop] - p['t0']),1e6*(Tte2[0:stop]-0.020000005449227316),label="TES Electron System Temperature New", linewidth = 1,color = "C3")
    plt.ylabel("$\Delta T$ [$\mu$K]",fontsize = 15)
    #plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.title("Nonlinear Solver",fontsize = 20)
    plt.legend(fontsize = 15)
    plt.tick_params(axis='y', which='major', labelsize=13)
    plt.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom='on',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            labelbottom='off') # labels along the bottom edge are off
    #Plot Current
    plt.subplot(5,1,2)
    plt.plot(1000*(ODEt[0:stop] - p['t0']),1e9*I[0:stop],label="Current Old", linewidth = 1, color = 'C1') 
    plt.plot(1000*(ODEt[0:stop] - p['t0']),1e9*I2[0:stop],label="Current New", linewidth = 1, color = 'C2') 
    plt.tick_params(axis='y', which='major', labelsize=13)
    plt.ylabel("Current [nA]",fontsize = 15)
    #plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend(fontsize = 15)
    plt.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom='on',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            labelbottom='off') # labels along the bottom edge are off
    
    
    plt.subplot(5,1,3)
    plt.plot(1000*(ODEt[0:stop] - p['t0']),resistance(Tte[0:stop],I[0:stop]),label = "Resistance Old", linewidth = 1, color = 'C2')
    plt.plot(1000*(ODEt[0:stop] - p['t0']),resistance(Tte2[0:stop],I2[0:stop]),label = "Resistance New", linewidth = 1, color = 'C1')
    plt.tick_params(axis='y', which='major', labelsize=13)
    plt.ylabel("Resistance [$\Omega$]",fontsize = 15)
    #plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend(fontsize = 15)
    plt.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom='on',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            labelbottom='off') # labels along the bottom edge are off
    
    #plot power
    plt.subplot(5,1,4)
    plt.plot(1000*(ODEt[0:stop] - p['t0']),1e12*(I[0:stop]**2)*resistance(Tte,I)[0:stop],label = "Power Old", linewidth = 1, color = 'C3')
    plt.plot(1000*(ODEt[0:stop] - p['t0']),1e12*(I2[0:stop]**2)*resistance(Tte2,I2)[0:stop],label = "Power New", linewidth = 1, color = 'k')
    plt.ylabel("Power [pW]",fontsize = 15)
    plt.tick_params(axis='y', which='major', labelsize=13)
    plt.tick_params(axis='x', which='major', labelsize=15)
    plt.xlabel("Time [ms]",fontsize = 15)
    plt.legend(fontsize = 15)
    if save_fig_flag == 1:
        namepng = 'nonlinear ' + str(savename) + '.png'   
        plt.savefig(namepng) #Save the figure to current directory
        nameeps = 'nonlinear ' + str(savename) + '.eps'   
        plt.savefig(nameeps) #Save the figure to current directory
    

    
plt.show() #Display the figure
print(min(I)-max(I))
print(min(I2)-max(I2))
print((min(I)-max(I)-(min(I2)-max(I2)))/(min(I)-max(I)))

### Now we derive the parameters we need from those given in the parameter file ###
p['VGC'] = p['AGC']*p['LGC']                                            #Volume that the parameter file Conductances and Capacities are at
p['Lm'] = p['Am']*p['conductivity_m']*p['Tb']/(p['G_mwb2']*p['TGC'])    #Length of the meander, calculated from the user provided conductance in the parameter file
#print(p['Lm']*1e3,p['Lm']*10.0e-6*1.0e3/0.0019) #compute the length that the meander would take up on the chip.  Assumes that you can get 1.9mm every 10 micrometers (becuase 2mm wide chip and 5 micrometer width)

### To make the program run faster we make some values global variables 
Rn =  p['Rn']           # Normal Resistance of TES [Ohms]
Tc = p['Tc']            # Tc of the TES [Kelvin]
alpha0 = p['alpha0']    # Temperature sensitivity [unitless]
beta0 = p['beta0']      # Current Sensitivity [unitless]

### Heat Capacities
#This is a function to calculate the heat capacities for us
def capacity(T,c1,c3,c5,A,L):
    C = c1*(T/p['TGC'])*A*L/p['VGC']+c3*(T/p['TGC'])**3*A*L/p['VGC']+c5*(T/p['TGC'])**5*A*L/p['VGC'] #scale for volume and temperature
    return C
#This is a function that is used to compute the Debye function for the heat capacity of the Silicone Oil (Polydimethylsiloxane i.e. PDMS)
def debye_integrand(x):
    D = x**3/(np.exp(x) - 1.0)
    return D

### Thermal Conductances
# Function to calculate non electron phonon coupling conductances
def conductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*(p['LGC']*A/(p['AGC']*L))#scale for area, length, and temperature
    return G
# Function to calculate electron phonon coupling conductances
def ephconductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*A*L/p['VGC'] #scale for volume and temperature
    return G

def calc_params():
    cg          = 6.947*3.0*(p['Tc']/69.0)**3.0*quad(debye_integrand,0,69.0/p['Tc'])[0]*0.8*p['Ag']*p['Lg']*1.0e+6/74.1539 #Heat Capacity of "Glue", currently PDMS [J/K]
    ca          = 1.*capacity(p['Tc'],p['ca_1_05'],p['ca_3_05'],0.0,p['Aa'],p['La'])                                               #Heat Capacity of Absorber [J/K]
    cau1        = capacity(p['Tc'],p['cau1_1_05'],p['cau1_3_05'],0.0,p['Aau1'],p['Lau1'])                                       #Heat Capacity of Gold Pad 1 [J/K]
    cwb1        = capacity(p['Tc'],p['cwb1_1_05'],p['cwb1_3_05'],0.0,p['Awb1'],p['Lwb1'])                                       #Heat Capacity of Wirebond 1 [J/K]
    cau2        = capacity(p['Tc'],p['cau2_1_05'],p['cau2_3_05'],0.0,p['Aau2'],p['Lau2'])                                       #Heat Capacity of Gold Pad 2 [J/K]
    csi         = capacity(p['Tc'],p['csi_1_05'],p['csi_3_05'],0.0,p['Asi'],p['Lsi'])                                           #Heat Capacity of Silicon Wafer [J/K]
    cte         = capacity(p['Tc'],p['cte_1_05'],p['cte_3_05'],0.0,p['Ate'],p['Lte'])                                           #Heat Capacity of TES Electron System [J/K]
    cm          = (capacity(p['Tb'],p['cm_1_05'],p['cm_3_05'],0.0,p['Am'],p['Lm'])+cau2)                                     #Heat Capacity of Meander [J/K]
    cwb2        = capacity(p['Tb'],p['cwb2_1_05'],p['cwb2_3_05'],0.0,p['Awb2'],p['Lwb2'])                                       #Heat Capacity of Wirebond 2 [J/K]
    ctot        = ca + cau1 +  cg  +  cwb1  +  cau2  +  csi  +  cte  +  cm  +  cwb2      #Heat Capacity of Total [J/K]
    G_ab        = conductance(p['Tc'],p['G_ab_05'],p['nab'],p['Ab'],p['Lb'])                  #Thermal Conductance of Absorber to Bath [W/K]
    G_ag        = conductance(p['Tc'],p['G_ag_05'],p['nag'],p['Ag'],p['Lg'])                  #Thermal Conductance of Absorber to Glue [W/K]
    G_gsi       = conductance(p['Tc'],p['G_gsi_05'],p['ngsi'],p['Ag'],p['Lg'])               #Thermal Conductance of Glue to Silicon Wafer [W/K]
    G_au1wb1    = 1.*conductance(p['Tc'],p['G_au1wb1_05'],p['nau1wb1'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Gold Pad 1 to Wirebond 1 [W/K]
    G_wb1au2    = 1.*conductance(p['Tc'],p['G_wb1au2_05'],p['nwb1au2'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Wirebond 1 to Gold Pad 2 [W/K]
    G_au2te     = 1.*conductance(p['Tc'],p['G_au2te_05'],p['nau2te'],p['Aau2'],p['Lau2'])     #Thermal Conductance of Gold Pad 2 to TES [W/K]
    G_tem       = 1.*conductance(p['Tc'],p['G_tem_05'],p['ntem'],p['Ate'],p['Lte'])             #Thermal Conductance of TES to Meander [W/K]
    G_wb2b      = 1.*conductance(p['Tb'],p['G_wb2b_05'],p['nwb2b'],p['Awb2'],p['Lwb2'])        #Thermal Conductance of Wirebond 2 to Bath [W/K]
    G_aau1      = ephconductance(p['Tc'],p['G_aau1_05'],p['naau1'],p['Aau1'],p['Lau1'])     #Thermal Conductance of Absorber to Gold Pad 2 [W/K]
    G_siau2     = 1.*ephconductance(p['Tc'],p['G_siau2_05'],p['nsiau2'],p['Aau2'],p['Lau2'])  #Thermal Conductance of Silicon Wafer to Gold Pad 2 [W/K]
    G_site      = 1.*ephconductance(p['Tc'],p['G_site_05'],p['nsite'],p['Ate'],p['Lte'])       #Thermal Conductance of Silicon Wafer to TES [W/K]
    G_sim       = 1.*p['G_sim_05']*(p['Tc']/p['TGC'])**(p['nsim'] - 1.)*(p['Am']*p['Lm']+p['Aau2']*p['Lau2'])/p['VGC']            #Thermal Conductance of Silicon Wafer to Meander [W/K]
    kab         = G_ab/(p['nab']*p['Tc']**(p['nab']-1.))                       #Conductance Constant Absorber to Bath [W/K^4]
    kaau1       = G_aau1/(p['naau1']*p['Tc']**(p['naau1']-1.))                 #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
    kag         = G_ag/(p['nag']*p['Tc']**(p['nag']-1.))                       #Conductance Constant Absorber to Glue [W/K^3.5]
    kau1wb1     = G_au1wb1/(p['nau1wb1']*p['Tc']**(p['nau1wb1']-1.))           #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
    kgsi        = G_gsi/(p['ngsi']*p['Tc']**(p['ngsi']-1.))                    #Conductance Constant Glue to Silicon [W/K^3.5]
    kwb1au2     = G_wb1au2/(p['nwb1au2']*p['Tc']**(p['nwb1au2']-1.))           #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
    ksiau2      = G_siau2/(p['nsiau2']*p['Tc']**(p['nsiau2']-1.))              #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
    kau2te      = G_au2te/(p['nau2te']*p['Tc']**(p['nau2te']-1.))              #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
    ksite       = G_site/(p['nsite']*p['Tc']**(p['nsite']-1.))                 #Conductance Constant Silicon to TES Phonon System [W/K^4]
    ksim        = G_sim/(p['nsim']*p['Tc']**(p['nsim']-1.))                    #Conductance Constant Silicon to Meander [W/K^4]
    ktem        = G_tem/(p['ntem']*p['Tc']**(p['ntem']-1.))                    #Conductance Constant TES Electron System to MEander [W/K^2]
    kmwb2       = p['G_mwb2']/(p['nmwb2']*p['Tb']**(p['nmwb2']-1.))                 #Conductance Constant Meander to Wirebond 2 [W/K^2]
    kwb2b       = G_wb2b/(p['nwb2b']*p['Tb']**(p['nwb2b']-1.))                 #Conductance Constant Wirecond 2 to Bath [W/K^4]
    R0          = Rn  * p['frac']                       #Equilibrium resistance of TES [Ohms]
    P_0         = p['I0_NL']**2* R0                      #Equilibrium power through TES [J]
    V_0         = p['I0_NL']* R0                         #Equilibrium Voltage [V]
    P0          = kmwb2 *((p['Tc'])**2 - (p['Tb'])**2)    #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
    I0_P        = np.sqrt( P0 / R0 )                  #Equilibrium Current calculated from power, used in resistance equation
    vb          = I0_P  * p['Rl']                       #Voltage Bias
    return cg ,  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_ag , G_gsi , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, kab , kaau1 , kag , kau1wb1 , kgsi , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, R0 , P_0 , V_0 , P0 , I0_P , vb

### Actually compute the variables 
cg ,  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_ag , G_gsi , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, kab , kaau1 , kag , kau1wb1 , kgsi , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, R0 , P_0 , V_0 , P0 , I0_P , vb = calc_params()
# Exponential function definition
def exponential(x, N, tau, a):
    val = N*np.exp(-x/tau)+a
    return val


start = int(np.where(Tte2 == np.max(Tte2))[0]) #set the starting point for the time values in the curve fit
p0=[p['E']/ctot,ctot*Tte2[0]/alpha0/(I2[0]**2.*resistance(Tte2[0],I2[0])),Tte2[0]] #give some approximate starting values for the curve fit p0[0]*e^(t/p[1])+p[2]
fitParams, fitCovariance = curve_fit(exponential, ODEt[start:len(ODEt)-1], Tte2[start:len(Tte2)-1],p0, sigma = (Tte2[start]-Tte2[0])*0.01*np.ones(len(Tte2)-1-start, dtype = float),bounds = (0.0,[1.0,100.0,1.0]))	# curve_fit(function_fit, x_fitted, y_fitted, initial parameters)
amp,tau, offset = fitParams #assign the result of the fit to these variables
print(tau)

