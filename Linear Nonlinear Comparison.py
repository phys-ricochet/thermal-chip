import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
from math import * 
from scipy import integrate
import inspect, os
import scipy as sp
import sys, traceback
from pylab import *
matplotlib.rc('axes.formatter', useoffset=False)


data = np.load('NPZ of Nonlinear Data.npz')
I = data['current']
Ta = data['absorber_temp']
Tau1 = data['gold_pad_1_temp']
Tg = data['glue_temp']
Twb1 = data['wirebond_1_temp']
Tau2 = data['gold_pad_2_temp']
Tsi = data['silicon_temp']
Tte = data['tes_electron_temp']
Tm = data['meander_temp']
Twb2 = data['wirebond_2_temp']
t = data['time']
del data.f
data.close()

data = np.load('NPZ of Linear Data.npz')
I_L = data['current']
Ta_L = data['absorber_temp']
Tau1_L = data['gold_pad_1_temp']
Tg_L = data['glue_temp']
Twb1_L = data['wirebond_1_temp']
Tau2_L = data['gold_pad_2_temp']
Tsi_L = data['silicon_temp']
Tte_L = data['tes_electron_temp']
Tm_L = data['meander_temp']
Twb2_L = data['wirebond_2_temp']
t_L = data['time']

del data.f
data.close()

### Make Some Plots ###

fig = plt.figure(figsize=(18,18)) #Make the plots biger 

lower = 1500
upper = 10000

adjusted_t = 1e3*(t[lower:upper] - 0.02)
adjusted_t_L = 1e3*t_L
plt.subplots_adjust(bottom = 0)
#Plot Current
plt.subplot(12,1,1)
plt.plot(adjusted_t, 1e6*I[lower:upper],label="Current Nonlinear",color = '#126011',linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(I_L + 2.229592954e-06),label = "Current Linear",color = '#bf159a',linewidth = 2,ls=':') 
plt.ylabel(r'Current [$\mu$A]',fontsize = 15)
plt.title("Nonlinear vs. Linear Comparison, Larger Sigma",fontsize = 30,y=1.25)
plt.legend(fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot absorber Temperature
plt.subplot(12,1,2)
plt.plot(adjusted_t, 1e6*(Ta[lower:upper] - 0.0200000913177),label="Target Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Ta_L),label = "Target Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Gold Pad 1 Temperature
plt.subplot(12,1,3)
plt.plot(adjusted_t, 1e6*(Tau1[lower:upper] - 0.0200000913944),label="Gold Pad 1 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Tau1_L),label = "Gold Pad 1 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Wirebond 1 Temperature
plt.subplot(12,1,4)
plt.plot(adjusted_t, 1e6*(Twb1[lower:upper] - 0.0200000913953),label="Wirebond 1 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Twb1_L),label = "Wirebond 1 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off
        
#Plot Glue Temperature on same graph
plt.subplot(12,1,5)
plt.plot(adjusted_t, 1e6*(Tg[lower:upper] - 0.0200000912899),label="Glue Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Tg_L),label = "Glue Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Silicon chip Temperature
plt.subplot(12,1,6)
plt.plot(adjusted_t, 1e6*(Tsi[lower:upper] - 0.020000091262),label="Silicon Wafer Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Tg_L),label = "Silicon Wafer Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot TES Electron System Temperature
plt.subplot(12,1,7)
plt.plot(adjusted_t, 1e6*(Tte[lower:upper] - 0.0200000913972),label="TES Electron System Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Tte_L),label = "TES Electron System Linear",linewidth = 2,ls=':')  
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Gold Pad 2 Temperature
plt.subplot(12,1,8)
plt.plot(adjusted_t, 1e6*(Tau2[lower:upper] - 0.0200000913961),label="Gold Pad 2 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Tau2_L),label = "Gold Pad 2 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Meander Temperature
plt.subplot(12,1,9)
plt.plot(adjusted_t, 1e6*(Tm[lower:upper] - 0.0200000047124),label="Meander Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Tm_L),label = "Meander Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Wirebond 2 Temperature
plt.subplot(12,1,10)
plt.plot(adjusted_t, 1e6*(Twb2[lower:upper] - 0.010003435838702),label="Wirebond 2 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L, 1e6*(Twb2_L),label = "Wirebond 2 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.tick_params(axis = 'x',labelsize = 15)
plt.xlabel("Time [ms]",fontsize = 15)
plt.legend(fontsize = 15)

plt.show()
plt.savefig('Linear vs. Nonlinear Comparison.png')