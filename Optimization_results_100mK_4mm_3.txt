Met constraints with G = 2.2e-9 V = 5.0e-5 L = 0.0002 A = 1.2e-10 and resolution 96.9955493554 and tau = 0.0305261512735
Met constraints with G = 2.2e-9 V = 1.0e-5 L = 0.0002 A = 1.2e-10 and resolution 84.6641503207 and tau = 0.0231996295633
Met constraints with G = 2.2e-9 V = 7.0e-6 L = 0.0002 A = 1.2e-10 and resolution 83.7347445703 and tau = 0.0226617920458
Met constraints with G = 2.2e-9 V = 1.0e-6 L = 0.0002 A = 1.2e-10 and resolution 83.2015105656 and tau = 0.0218179345618
Met constraints with G = 2.2e-9 V = 5.0e-6 L = 0.0002 A = 1.2e-10 and resolution 83.1622210686 and tau = 0.0223121034075
Met constraints with G = 2.2e-9 V = 3.0e-6 L = 0.0002 A = 1.2e-10 and resolution 82.7250321429 and tau = 7.87242091671e-05
Met constraints with G = 2.0e-9 V = 5.0e-5 L = 0.0002 A = 1.2e-10 and resolution 94.5315126568 and tau = 0.0335813715194
Met constraints with G = 2.0e-9 V = 1.0e-5 L = 0.0002 A = 1.2e-10 and resolution 82.5143361731 and tau = 0.025520793823
Met constraints with G = 2.0e-9 V = 5.0e-6 L = 0.0002 A = 1.2e-10 and resolution 81.0486988151 and tau = 0.0245414636318
Met constraints with G = 2.0e-9 V = 7.0e-6 L = 0.0002 A = 1.2e-10 and resolution 81.6078058936 and tau = 0.0249278439018
Met constraints with G = 2.0e-9 V = 3.0e-6 L = 0.0002 A = 1.2e-10 and resolution 80.6198128521 and tau = 0.024178499362
Met constraints with G = 1.8e-9 V = 5.0e-5 L = 0.0002 A = 1.2e-10 and resolution 91.8894948146 and tau = 0.0373174733357
Met constraints with G = 2.0e-9 V = 1.0e-6 L = 0.0002 A = 1.2e-10 and resolution 81.0703408447 and tau = 0.0239734618778
Met constraints with G = 1.8e-9 V = 1.0e-5 L = 0.0002 A = 1.2e-10 and resolution 80.209659648 and tau = 0.0283596461789
Met constraints with G = 1.8e-9 V = 7.0e-6 L = 0.0002 A = 1.2e-10 and resolution 79.3277680528 and tau = 0.0276993524191
Met constraints with G = 1.8e-9 V = 5.0e-6 L = 0.0002 A = 1.2e-10 and resolution 78.7831785036 and tau = 0.0272680970305
Met constraints with G = 1.8e-9 V = 3.0e-6 L = 0.0002 A = 1.2e-10 and resolution 78.3634786965 and tau = 0.0268603979707
Met constraints with G = 1.8e-9 V = 1.0e-6 L = 0.0002 A = 1.2e-10 and resolution 78.7874614773 and tau = 0.0266103516983
DID NOT MEET CONSTRAINTS with G = 1.6e-9 V = 5.0e-5 L = 0.0002 A = 1.2e-10
Met constraints with G = 1.6e-9 V = 1.0e-5 L = 0.0002 A = 1.2e-10 and resolution 77.7189425517 and tau = 0.0319109350739
Met constraints with G = 1.6e-9 V = 7.0e-6 L = 0.0002 A = 1.2e-10 and resolution 76.8638122187 and tau = 0.0311666731843
Met constraints with G = 1.6e-9 V = 5.0e-6 L = 0.0002 A = 1.2e-10 and resolution 76.3350652686 and tau = 0.030679342533
Met constraints with G = 1.6e-9 V = 3.0e-6 L = 0.0002 A = 1.2e-10 and resolution 75.9256130496 and tau = 0.0302155048278
Met constraints with G = 1.6e-9 V = 1.0e-6 L = 0.0002 A = 1.2e-10 and resolution 76.3223258057 and tau = 0.0299090440158
Met constraints with G = 1.4e-9 V = 1.0e-5 L = 0.0002 A = 1.2e-10 and resolution 74.999824074 and tau = 0.0364815428636
DID NOT MEET CONSTRAINTS with G = 1.4e-9 V = 5.0e-5 L = 0.0002 A = 1.2e-10
Met constraints with G = 1.4e-9 V = 3.0e-6 L = 0.0002 A = 1.2e-10 and resolution 73.2648938625 and tau = 0.0345338564047
Met constraints with G = 1.4e-9 V = 5.0e-6 L = 0.0002 A = 1.2e-10 and resolution 73.6627939444 and tau = 0.0350697936633
Met constraints with G = 1.4e-9 V = 7.0e-6 L = 0.0002 A = 1.2e-10 and resolution 74.1740677416 and tau = 0.0356292553189
Met constraints with G = 1.4e-9 V = 1.0e-6 L = 0.0002 A = 1.2e-10 and resolution 73.6334338832 and tau = 0.0341549677433
DID NOT MEET CONSTRAINTS with G = 1.2e-9 V = 5.0e-5 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.2e-9 V = 1.0e-5 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.2e-9 V = 5.0e-6 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.2e-9 V = 7.0e-6 L = 0.0002 A = 1.2e-10
Met constraints with G = 1.2e-9 V = 1.0e-6 L = 0.0002 A = 1.2e-10 and resolution 70.6621596027 and tau = 0.0398245388436
DID NOT MEET CONSTRAINTS with G = 1.2e-9 V = 3.0e-6 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.0e-9 V = 5.0e-5 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.0e-9 V = 1.0e-5 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.0e-9 V = 7.0e-6 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.0e-9 V = 1.0e-6 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.0e-9 V = 5.0e-6 L = 0.0002 A = 1.2e-10
DID NOT MEET CONSTRAINTS with G = 1.0e-9 V = 3.0e-6 L = 0.0002 A = 1.2e-10
