import numpy as np  # Import Numpy
import matplotlib.pyplot as plt # Import plotting functions
from scipy.integrate import odeint # Import Nonlinear Solver
from scipy.optimize import fsolve # Import equilibrium point finder
from scipy.optimize import root,anderson,minimize,differential_evolution# Alternative equilibrium point finder
from sympy.functions.special.delta_functions import DiracDelta
np.set_printoptions(threshold=np.nan) #Let us print the full array if necessary


### Needs to be done:
    # Make values read from external source
    # Figure out why there are so many solutions to the equations

### To try:
    #Try hand calculating the equations to make sure that you are actually computing what you think you are
    #Try and impose boundaries on the solutions





### Externally Define the Parameters ###
### The ultimate goal will to be to read these from a text file ###

### Physical Constants ###
kb=1.38e-23             #Boltzman Constant [J/K]
hbar=1.05e-34           #Plank's Constant over 2 pi [J*s]   

### Electrical Components ###
L=1e-7                  #Inductor in Thevenin Equivalent [Henries]
Rl=5e3                  #Load Resistor [Ohms]
alpha0=100.             #Temperature Sensetivity [Unitless]
beta0 = 1.
Rn=1.                   #normal resistance of the TES [Ohms]
Ccap=1e12               #Capacitor in Thevinin equivalent Circuit [Farads]
Tc=0.10                 #Temperature Half way up the transition [K], NOT AT BIAS.  Number is a guess from Pyle's Thesis
frac = 0.5              #Fraction of normal resistance desired at equilibrium
R0 = Rn * frac          #Resistance at equilibrium

### Heat Capacities ###
ca=6.44e-10             #Heat Capacity of Absorber [J/K]
cau1=4.47e-11           #Heat Capacity of Gold Pad 1 [J/K]
cg=6.87e-11             #Heat Capacity of Glue [J/K]
cwb1=2.81e-10           #Heat Capacity of Wirebond 1 [J/K]
cau2=1.12e-13           #Heat Capacity of Gold Pad 2 [J/K]
csi=1.93e-12            #Heat Capacity of Silicon [J/K]
cte=3.41e-10            #Heat Capacity of TES Electron System [J/K]
ctp=3.12e-10            #Heat Capacity of TES Phonon system [J/K]
cm=1.12e-13             #Heat Capacity of Meander [J/K]
cwb2=2.81e-10           #Heat Capacity of Wirebond 2 [J/K]
cb=100.                 #Heat Capacity of Bath [J/K]

### Conductance Constants ###
kab=0.                  #Conductance Constant Absorber to Bath [W/K^4]
kaau1=7.8e-5            #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
kag = 1.4e-5             #Conductance Constant Absorber to Glue [W/K^3.5]
kau1wb1=6.98e-4          #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
kgsi = 1.4e-5            #Conductance Constant Glue to Silicon [W/K^3.5]
kwb1au2=6.98e-4          #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
ksiau2 = 9.8e-7       #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
kau2te=5.5e-5           #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
ksitp=1.2e-5           #Conductance Constant Silicon to TES Phonon System [W/K^4]
ksim=9.8e-6            #Conductance Constant Silicon to Meander [W/K^4]
ktpte=1.7e-1            #Conductance Constant TES Phonon System to TES Electron System [W/K^6]
ktem=5.5e-5             #Conductance Constant TES Electron System to MEander [W/K^2]
kmwb2=1.5e-6            #Conductance Constant Meander to Wirebond 2 [W/K^2]
kwb2b=1.8e-2            #Conductance Constant Wirecond 2 to Bath [W/K^4]

### Exponents for Power ###

nab=4                  #Exponent Absorber to Bath [Unitless]
naau1=2                #Exponent Absorber to Gold Pad 1 [Unitless]
nag=3.5                  #Exponent Absorber to Glue [Unitless] 
nau1wb1=2              #Exponent Gold Pad 1 o Wirebond 1 [Unitless]
ngsi=3.5                 #Exponent Glue to Silicon Chip [Unitless]
nwb1au2=2              #Exponent Wirebond 1 to Gold Pad 2 [Unitless]
nsiau2=4               #Exponent Silicon Chip to Gold Pad 2 [Unitless]
nau2te=2               #Exponent Gold Pad 2 to TES Electron System [Unitless]
nsitp=4                #Exponent Silicon Chip to TES Phonon System [Unitless]
nsim=4                 #Exponent Silicon chip to Meander [Unitless]
ntpte=5                #Exponent TES Phonon System to TES Electron System [Unitless]
ntem=2                 #Exponent TES Electron System to Meander [Unitless]
nmwb2=2                #Exponent Meander to Wirebond 2 [Unitless]
nwb2b=4                #Exponent Wirecond 2 to Bath [Unitless]

###Power Calculation ###

P0 = kmwb2*((0.1)**2-(0.08)**2)  #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
I0 = np.sqrt(P0/R0)              #Equilibrium Current
vb = I0 * Rl                    #Voltage Bias
print("P",P0,"I0",I0,"vb",vb)







### Definition of Functions ###


### Define the resistance function ###

def resistance(Ttes,Ites):
    R = Rn/2 *(1 + np.tanh((Ttes - Tc) * alpha0/Tc + (Ites - I0)*beta0/I0))
    return R

### Equations to be solved for the equilibrium point using np.optimize.root ###

def equations(w,p):
    # variables to solve for
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = w
    # parameters 
    R0,L,Rl,alpha0,Rn,Ccap,kb,hbar,Tc,vb,ca,kab,kaau1,kag,cau1,kau1wb1,cg,kgsi,cwb1,kwb1au2,cau2,kau2te,csi,ksiau2,ksitp,ksim,cte,ktpte,ktem,ctp,cm,kmwb2,cwb2,kwb2b,cb,nab,naau1,nag,nau1wb1,ngsi,nwb1au2,nsiau2,nau2te,nsitp,nsim,ntpte,ntem,nmwb2,nwb2b = p
    ### Array of functions to solve ###
    f1 = [((resistance(Tte,I)-R0)),
         (-kab*(Ta**nab-(0.08)**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**(nag)-Tg**(nag)))*1e12,
         (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1))*1e12,
         (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi))*1e12,
         (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2))*1e12,
         (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te))*1e12,
         (-kgsi*(Tsi**ngsi-Tg**ngsi) - ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim))*1e12,
         ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem)))*1e12,
         (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte))*1e12,
         (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2))*1e12,
         (-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-(0.08)**nwb2b))*1e12]

    return f1

### Set up the Nonliear ODEs ###
def vectorfield(w,t):
    # Variables
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = w 
    ### Array of the functions to solve ###
    I0 = 0.000102063349559 #[Amps], taken from the nonlinear solver at large time after equilibrium had been reached (1000s of seconds)
    #I0_Ziqing = 1.288765e-04 #[Amps], Taken from Ziqing's minimizer
    #Gaussian Parameters
    E = 1.60218e-15             #Total Energy input
    c = 1e-4                    #Width of Gaussian
    #((E/(c*np.sqrt(2*np.pi)))*np.exp(-(t - 2000.)**2/(2*c**2)))
    f1 = [(I0*R0 - I*resistance(Tte,I))/L,
         (-kab*(Ta**nab-(0.08)**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**nag-Tg**nag))/ca,
         (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1))/cau1,
         (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi))/cg,
         (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2))/cwb1,
         (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te))/cau2,
         (-kgsi*(Tsi**ngsi-Tg**ngsi) - ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim))/csi,
         ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem)))/cte,
         (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte))/ctp,
         (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2))/cm,
         (-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-(0.08)**nwb2b))/cwb2]
    return f1




"""
### Methods to find equilibrium point ###

### Minimization Method ###

def sum_of_squares(w):
    #Variables
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = w
    #Parameters     
    
    ### Conductance Constants ###
    kab=0.                  #Conductance Constant Absorber to Bath [W/K^4]
    kaau1=7.8e-5            #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
    kag=1.4e-5              #Conductance Constant Absorber to Glue [W/K^3.5]
    kau1wb1=6.98e-4          #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
    kgsi=1.4e-5             #Conductance Constant Glue to Silicon [W/K^3.5]
    kwb1au2=6.98e-4          #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
    ksiau2=9.8e-6          #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
    kau2te=5.5e-5           #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
    ksitp=1.2e-5           #Conductance Constant Silicon to TES Phonon System [W/K^4]
    ksim=9.8e-6            #Conductance Constant Silicon to Meander [W/K^4]
    ktpte=1.7e-1            #Conductance Constant TES Phonon System to TES Electron System [W/K^6]
    ktem=5.5e-5             #Conductance Constant TES Electron System to MEander [W/K^2]
    kmwb2=1.5e-6            #Conductance Constant Meander to Wirebond 2 [W/K^2]
    kwb2b=1.8e-2            #Conductance Constant Wirecond 2 to Bath [W/K^4]
    
    ### Electrical Components ###
    Rl = 5e3                  #Load Resistor [Ohms]
    frac = 0.5              #Fraction of normal resistance at equilibrium
    Rn = 1.                 #Normal Resistance of TES [Ohms]
    R0 = frac * Rn          #Resistance of TES at equilibrium [Ohms]
    Rsh = 0.02              #Shunt Resistance [Ohms]
    P0 = kmwb2*((0.1)**2-(0.08)**2)  #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
    I0 = np.sqrt(P0/R0)              #Equilibrium Current

    ### Exponents for Power ###
    
    nab=4.                  #Exponent Absorber to Bath [Unitless]
    naau1=2.                #Exponent Absorber to Gold Pad 1 [Unitless]
    nag=3.5                 #Exponent Absorber to Glue [Unitless] 
    nau1wb1=2.              #Exponent Gold Pad 1 to Wirebond 1 [Unitless]
    ngsi=3.5                #Exponent Glue to Silicon Chip [Unitless]
    nwb1au2=2.              #Exponent Wirebond 1 to Gold Pad 2 [Unitless]
    nsiau2=4.               #Exponent Silicon Chip to Gold Pad 2 [Unitless]
    nau2te=2.               #Exponent Gold Pad 2 to TES Electron System [Unitless]
    nsitp=4.                #Exponent Silicon Chip to TES Phonon System [Unitless]
    nsim=4.                 #Exponent Silicon chip to Meander [Unitless]
    ntpte=5.                #Exponent TES Phonon System to TES Electron System [Unitless]
    ntem=2.                 #Exponent TES Electron System to Meander [Unitless]
    nmwb2=2.                #Exponent Meander to Wirebond 2 [Unitless]
    nwb2b=4.                #Exponent Wirecond 2 to Bath [Unitless]
    
    ### Array of functions to solve ###

    f1 = [0.0001*((resistance(Tte,I)-R0)),
         (-kab*(Ta**nab-(0.08)**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**(nag)-Tg**(nag))),
         (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1)),
         (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi)),
         (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2)),
         (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te)),
         (-kgsi*(Tsi**ngsi-Tg**ngsi)-ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim)),
         ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem))),
         (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte)),
         (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2)),
         0.1*(-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-(0.08)**nwb2b))]
   ### Compute the sum of squares, weighted by 1e24 because we expect the equations are in pW and are squared ###
    s1 = 0.
    for i in range(0,len(f1)):
        print(f1[i])
        s1 = s1 + (f1[i]**2)
    print("---------------")
    return s1

### Use Differential Evolution ###

bnds = [(0.,2e-4),(0.7,0.105),(0.7,0.105),(0.7,0.105),(0.7,0.105),(0.7,0.105),(0.7,0.105),(0.7,0.105),(0.7,0.105),(0.7,0.105),(0.7,0.105)] #define the bounds for the minimizer
initials = differential_evolution(sum_of_squares,bnds,maxiter = 100000) #use Differential Evolution to find the solution
print("R=",resistance(initials.x[7],initials.x[0]),"solution",initials) #print the resistance obtained and the solution

### Checks of differential evolution ###

I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = initials.x #retrieve the parameters
#print the function evaluated at these parameters
print("F1",[((resistance(Tte,I)-R0)),
 (-kab*(Ta**nab-(0.08)**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**(nag)-Tg**(nag))),
 (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1)),
 (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi)),
 (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2)),
 (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te)),
 (-kgsi*(Tsi**ngsi-Tg**ngsi)-ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim)),
 ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem))),
 (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte)),
 (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2)),
 (-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-(0.08)**nwb2b))])

### define our own versions of the parameters
I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = [I,Tte,Tte,Tte,Tte,Tte,Tte,Tte,Tte,Tte,.08]
#print the function evaluated at the parameters
print("F1",[((resistance(Tte,I)-R0)),
 (-kab*(Ta**nab-(0.08)**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**(nag)-Tg**(nag))),
 (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1)),
 (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi)),
 (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2)),
 (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te)),
 (-kgsi*(Tsi**ngsi-Tg**ngsi)-ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim)),
 ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem))),
 (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte)),
 (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2)),
 (-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-(0.08)**nwb2b))])
#Print the sum of squares evaluated at our choice of the parameters
print("squares",sum_of_squares([I,Tte,Tte,Tte,Tte,Tte,Tte,Tte,Tte,Tte,.08]))


### Create Empty Arrays ###
test0=[]
test1=[]
test2=[]
test3=[]
test4=[]
test5=[]
test6=[]
test7=[]
test8=[]
test9=[]
test10=[]
test11=[]
squares = []
fun = []

### Evaluate Differential Evolution for 30 different boundary conditions and store the values in the arrays above ###
for i in range(1,30):
    print(10*i)
    bnds = [(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i),(0.,10*i)]
    initials = differential_evolution(sum_of_squares, bnds)
    test0.append(initials.x[0])
    test1.append(initials.x[1])
    test2.append(initials.x[2])
    test3.append(initials.x[3])
    test4.append(initials.x[4])
    test5.append(initials.x[5])
    test6.append(initials.x[6])
    test7.append(initials.x[7])
    test8.append(initials.x[8])
    test9.append(initials.x[9])
    test10.append(initials.x[10])
    test11.append(initials.x[11])
    squares.append(initials.fun)

#save_name = 'NPZ of data' #+ identifier #Create an NPZ filename 
### Output to an NPZ file, named above ###
#np.savez(save_name, current = test0, voltage = test1, absorber_temp = test2, gold_pad_1_temp = test3, glue_temp = test4, wirebond_1_temp = test5, gold_pad_2_temp = test6, silicon_temp = test7, tes_electron_temp = test8, tes_phonon_temp = test9, meander_temp = test10, wirebond_2_temp = test11, squares = squares) #index = index

### Plot The Results###

fig = plt.figure(figsize=(18, 18)) #make the plots bigger

#Plot absorber Temperature
plt.subplot(11,1,1)
plt.plot(test2,label="Absorber Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("upper bound/10 -1 [K]")
plt.legend()
plt.title("Nonlinear Solver")
fig.subplots_adjust(hspace=.5)

#Plot Current
plt.subplot(11,1,2)
plt.plot(test0,label="Current") 
plt.ylabel("Current [I]")
plt.xlabel("upper bound/10 -1 [I]")
plt.legend()

#Plot Voltage
plt.subplot(11,1,3)
plt.plot(test1,label="Voltage") 
plt.ylabel("Voltage [V]")
plt.xlabel("upper bound/10 -1 [V]")
plt.legend()

#Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
plt.subplot(11,1,4)
plt.plot(test3,label="Gold Pad 1 Temperature") 
plt.plot(test5,label="Wirebond 1 Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("upper bound/10 -1 [K]")
plt.legend()

#Plot Glue Temperature on same graph
plt.subplot(11,1,5)
plt.plot(test4,label="Glue Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("upper bound/10 -1 [K]")
plt.legend()

#Plot Silicon chip Temperature
plt.subplot(11,1,6)
plt.plot(test7,label="Silicon Chip Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("upper bound/10 -1 [K]")
plt.legend()

#Plot TES Electron and Phonon System Temperatures on same graph
plt.subplot(11,1,7)
plt.plot(test8,label="TES Electron System Temperature")
plt.plot(test9,label="TES Phonon System Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("upper bound/10 -1 [K]")
plt.legend()

#Plot Gold Pad 2 Temperature
plt.subplot(11,1,8)
plt.plot(test6,label="Gold Pad 2 Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("upper bound/10 -1 [K]")
plt.legend()

#Plot Meander and Wirebond 2 Temperatures on same graph
plt.subplot(11,1,9)
plt.plot(test10,label="Meander Temperature")
plt.plot(test11,label="Wirebond 2 Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("upper bound/10 -1 [K]")
plt.legend()

plt.subplot(11,1,10)
plt.plot(fun,label = "function value")
plt.ylabel("minimum function value")
plt.xlabel("upper bound/10 -1")
plt.legend()

plt.subplot(11,1,11)
plt.plot(squares,label = "sum of squares at solution")
plt.ylabel("sum of squares at solution")
plt.xlabel("upper bound/10 -1")
plt.legend()

plt.savefig('Nonlinear Solver Differential Evolution Test with function plot.png') #Save the figure to current directory
plt.show() #Display the figure     
"""    


"""
### Root Finding ###

# Initial Guesses
x0 = np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])
x1 = x0*10
x2 = x1*2
x3 = x0*0

# Method 1, currently diverges for each of the initial guesses (14 June 2017)

initials0 = fsolve(equations,x0,args=(p,))
initials1 = fsolve(equations,x1,args=(p,))
initials2 = fsolve(equations,x2,args=(p,))
initials3 = fsolve(equations,x3,args=(p,))

print("0",initials0)
print("1",initials1)
print("2",initials2)
print("3",initials3)

#Method 2, converges for all guesses (14 June 2017), raises errors at decimals in exponents

initials = root(equations,x0,args = (p),jac = False,method='lm',tol = 1e-15)
initials1 = root(equations,x1,args = (p),jac = False,method='lm')
initials2 = root(equations,x2,args = (p),jac = False,method='lm')
initials3 = root(equations,x3,args = (p),jac = False,method='lm')

#Print whether the guesses led to a successful determination of the initial conditions, and print those initial conditions

print("0",initials0.success,initials0.x)
print("1",initials1.success,initials1.x)
print("2",initials2.success,initials2.x)
print("3",initials3.success,initials3.x)

#compute root for a number of different initial conditions

test2=[] #array for an indicator variable to be placed in
error2=[] #array to hold the tests that didnt succeed
for i in range(1,100):
    x0 = np.array([float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i)]) #initial guess
    x0 = 0.1*x0 #Scale the initial guess
    initials0 = root(equations,x0,args = (p),jac = False,method='lm') #compute with root
    if initials0.x[4]>(2.*x0[4]): #if the answer blew up (greater than twice the initial), send it to the initial
        initials0.x[4]=x0[4]
    test2.append(initials0.x[4]) #Append the indicator variable
    if initials0.success == False: #if the routine failed append to the error array
        error1.append(x0[4])
print(error2)
plt.plot(test2)    
"""


### ODE Solver Parameters ###
abserr = 1.0e-8         #Absolute Error threshold in Solver
relerr = 1.0e-6         #Relative Error Threshold in solver
#stoptime = 2100.          #How long do you want the solution to be for [s]
#numpoints = 21000000        #How many time points do you want in your solution
stoptime = 1000.
numpoints = 10000
print(stoptime/numpoints)
t = [stoptime * float(i) / (numpoints - 1) for i in range(numpoints)] #Make an array of those time points

#Put the initial conditions into an array
#w0 = [I0,I0 * R0,.1,.1,.1,.1,.1,.1,.1,.1,.1,.08]
#w0 = [0.000101981890052,0.0997296110865,0.0997296111079,0.0997296119813,0.0997296111105,0.0997296111146,0.0997296132017,0.1000260502,0.100024668565,0.0995500984494,0.0801415330134]
#w0 = [1.288765e-04,9.945124e-02,9.948128e-02,9.947379e-02,9.953269e-02,9.959576e-02,9.952707e-02,9.985873e-02,9.985867e-02,9.937393e-02,8.014015e-02]
#w0 = [0.000102493494064,0.100006574195,0.100006574195,0.100006574209,0.100006574195,0.100006574195,0.100006574228,0.100008764206,0.100008762961,0.0995325690215,0.0801413926739]
w0 = [0.000102063349559,0.100020637726,0.100021192109,0.0999648025488,0.10002125406,0.10002131601,0.0999088892956,0.100022102219,0.100022038446,0.0995468688251,0.0801415071554]
#w0 = [3.51207950436e-05,0.0997928985181,0.0997929537219,0.099787323092,0.0997929598908,0.0997929660597,0.099781746887,0.0997930483295,0.0997930419389,0.0997447036129,0.0800144276447]
#w0 = [3.09751078755e-05,0.100191951036,0.100192007638,0.100186268504,0.100192013963,0.100192020288,0.100180585166,0.10019210465,0.100192098162,0.100142873495,0.0800147511064]
#w0 = [0.000279891353429,0.0968868333602,0.0968872722095,0.0968404771933,0.0968873212498,0.0968873702901,0.0967940654848,0.0968880238217,0.09688796917,0.0964814753813,0.0801173314778]

#Set the pulse height in the absorber [K]
#Make this a thin gaussian whose integral is the energy we want
#w0[1]=0.102

### Solve the System of Nonlinear ODEs ###
#comment arguments
wsol = odeint(vectorfield, w0, t,
              atol=abserr, rtol=relerr)




### Plot the results ###

### Transpose the solution so we can easily plot it using the map function as in the linear case ###
solution = np.transpose(wsol)

### Separate out the solution into individual arrays that can be plotted ###
I, Ta, Tau1, Tg, Twb1, Tau2, Tsi, Tte, Ttp, Tm, Twb2 = map(np.array, solution)

#Plot flag
fig = plt.figure(figsize=(18, 18)) #Make the plots bigger

plot_flag = 1

if plot_flag == 0:
    print("Thanks for not making me plot")

if plot_flag == 1:
    
    ### Linear Plot ###
    
    #Plot temperatures
    plt.subplot(5,1,1)
    plt.plot(Ta,label="Absorber Temperature")
    plt.plot(Tau1,label="Gold Pad 1 Temperature") 
    plt.plot(Twb1,label="Wirebond 1 Temperature")
    plt.plot(Tg,label="Glue Temperature")
    plt.plot(Tsi,label="Silicon Chip Temperature")
    
    plt.plot(Ttp,label="TES Phonon System Temperature")
    plt.plot(Tau2,label="Gold Pad 2 Temperature")
    #plt.plot(Tm,label="Meander Temperature")
    #plt.plot(Twb2,label="Wirebond 2 Temperature")
    plt.plot(Tte,label="TES Electron System Temperature",color = "k")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [0.1 ms]")
    plt.title("Nonlinear Solver long baseline eq point")
    plt.legend()
    
    #Plot Current
    plt.subplot(5,1,2)
    plt.plot(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [0.1 ms]")
    plt.legend()
    
    
    #Plot Resistance
    plt.subplot(5,1,3)
    plt.plot(resistance(Tte,I),label = "Resistance")
    plt.ylabel("Resistance [Ohms]")
    plt.xlabel("Time [0.1 ms]")
    plt.legend()
    
    #plot power
    plt.subplot(5,1,4)
    plt.plot((I**2)*resistance(Tte,I),label = "Power")
    plt.ylabel("Power [Watts]")
    plt.xlabel("Time [0.1 ms]")
    plt.legend()
    
    plt.show() #Display the figure

if plot_flag == 2:
    
    ### Log Plot ###
    
    #Plot temperatures
    plt.subplot(5,1,1)
    plt.semilogy(Ta,label="Absorber Temperature")
    plt.semilogy(Tau1,label="Gold Pad 1 Temperature") 
    plt.semilogy(Twb1,label="Wirebond 1 Temperature")
    plt.semilogy(Tg,label="Glue Temperature")
    plt.semilogy(Tsi,label="Silicon Chip Temperature")
    plt.semilogy(Tte,label="TES Electron System Temperature")
    plt.semilogy(Ttp,label="TES Phonon System Temperature")
    plt.semilogy(Tau2,label="Gold Pad 2 Temperature")
    plt.semilogy(Tm,label="Meander Temperature")
    #plt.semilogy(Twb2,label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [0.1 ms]")
    plt.title("Nonlinear Solver Log 2mK Pulse")
    plt.legend()
    
    #Plot Current
    plt.subplot(5,1,2)
    plt.semilogy(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [0.1 ms]")
    plt.legend()
    
    
    #Plot Resistance
    plt.subplot(5,1,3)
    plt.semilogy(resistance(Tte,I),label = "Resistance")
    plt.ylabel("Resistance [Ohms]")
    plt.xlabel("Time [0.1 ms]")
    plt.legend()
    
    #plot power
    plt.subplot(5,1,4)
    plt.semilogy((I**2)*resistance(Tte,I),label = "Power")
    plt.ylabel("Power [Watts]")
    plt.xlabel("Time [0.1 ms]")
    plt.legend()
    
    plt.show() #Display the figure
    
if plot_flag == 3:
    
    ### Plot mostly individually ###
    
    #Plot absorber Temperature
    plt.subplot(10,1,1)
    plt.plot(Ta,label="Absorber Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    plt.title("Nonlinear Solver")
    fig.subplots_adjust(hspace=.5)
    
    #Plot Current
    plt.subplot(10,1,2)
    plt.plot(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    
    #Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
    plt.subplot(10,1,4)
    plt.plot(Tau1,label="Gold Pad 1 Temperature") 
    plt.plot(Twb1,label="Wirebond 1 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    
    #Plot Glue Temperature on same graph
    plt.subplot(10,1,5)
    plt.plot(Tg,label="Glue Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    
    #Plot Silicon chip Temperature
    plt.subplot(10,1,6)
    plt.plot(Tsi,label="Silicon Chip Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    
    #Plot TES Electron and Phonon System Temperatures on same graph
    plt.subplot(10,1,7)
    plt.plot(Tte,label="TES Electron System Temperature")
    plt.plot(Ttp,label="TES Phonon System Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    
    #Plot Gold Pad 2 Temperature
    plt.subplot(10,1,8)
    plt.plot(Tau2,label="Gold Pad 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    
    #Plot Meander and Wirebond 2 Temperatures on same graph
    plt.subplot(10,1,9)
    plt.plot(Tm,label="Meander Temperature")
    plt.plot(Twb2,label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [mu s]")
    plt.legend()
    
    plt.show() #Display the figure


#plt.savefig('Nonlinear Solver gaussian pulse c-10 E-1.6e-11.png')



### Print eq values ###

print(I[9999])
print(Ta[9999])
print(Tau1[9999])
print(Tg[9999])
print(Twb1[9999])
print(Tau2[9999])
print(Tsi[9999])
print(Tte[9999])
print(Ttp[9999])
print(Tm[9999])
print(Twb2[9999])
print("resistance",resistance(Tte[2999],I[2999]))
