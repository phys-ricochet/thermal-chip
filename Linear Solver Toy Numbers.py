#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  9 12:16:02 2017

@author: DP3
"""
import matplotlib.pyplot as plt
import numpy as np

from numpy import linalg as LA
from numpy.linalg import inv
np.set_printoptions(threshold=np.nan)


### Electrical Components ###
R_L = 1.                  #Load Resistor
R_tes_0 = 1.              #Equilibium TES resistance
L = 1.                 #Circuit Inductance
C_cap = 1.               #Circuit Capacitance
beta = 1.               #Current Sensitivity
P_0 = 1.              #Equilibrium power through TES 
alpha = 1.             #Temperature Sensitivity
V_0 = 1.            #Equilibrium Voltage
T_Te_0 = 1.           #Temperature of TES Electron System at equilibrium

### Thermal Conductances ###
G_ab =  0.                #Conductance from Absorber to Bath 
G_aAu1 = 1.               #Conductance from Absorber to Gold Pad 1
G_ag = 1.         #Conductance from Absorber to Glue
G_gSi = 1.             #Conductance from Glue to Silicon
G_Au1wb1 = 1.            #Conductance from Gold Pad 1 to Wirebond 1
G_wb1Au2 = 1.           #Conductance from Wirebond 1 to Gold Pad 2
G_SiAu2 = 3.          #Conductance from Silicon Chip to Gold Pad 2
G_SiTp = 4.         #Conductance from Silicon Chip to TES phonon System
G_TpTe = 3        #Conductance from TES Phonon System to TES Electron System
G_Au2Te = 1.       #Conductance from Gold Pad 2 to TES Electron System
G_Tem = 8.      #Conductance from TES Electron System to Meander
G_Sim = 3.     #Conductance from Silicon chip to Meander
G_mwb2 = 3.    #Conductance from Meander to Wirebond 2
G_wb2b = 1e0   #Conductance from Wirebond 2 to Bath

### Heat Capacities ###
C_b =1000                   #Heat Capacity of Bath
C_a = 6e-0                  #Heat Capacity of Absorber
C_g = 6e-0                 #Heat Capacity of Glue
C_Au1 = 4e-0                #Heat Capacity of Gold Pad 1
C_Si = 1e-0               #Heat Capacity of Silicon Chip
C_wb1 = 2e-0            #Heat Capacity of Wirebond 1
C_Au2 = 1e-0            #Heat Capacity of Gold Pad 2
C_Tp = 3e-0            #Heat Capacity of TES Phonon system
C_Te = 3e-0           #Heat Capacity of TES Electron System
C_m = 1e-0          #Heat Capacity of Meander
C_wb2 = 2e-0         #Heat Capacity of Wirebond 2

### Enter the Matrix ###
N = np.array([[-R_tes_0*(1+beta)/L, 1/L, -alpha*V_0/(T_Te_0*L),0,0,0,0,0,0,0,0,0,0],
             [-1/C_cap,-1/(R_L*C_cap),0,0,0,0,0,0,0,0,0,0,0],
             [(2+beta)*V_0/(C_Te),0,-(G_Au2Te+G_TpTe+G_Tem-alpha*P_0/T_Te_0)/C_Te,0,0,0,0,G_Au2Te/C_Te,0,G_TpTe/C_Te,G_Tem/C_Te,0,0],
             [0,0,0, -(G_ab+G_aAu1+G_ag)/C_a,G_aAu1/C_a,G_ag/C_a,0,0,0,0,0,0,G_ab/C_a],
             [0,0,0,G_aAu1/C_Au1,-(G_aAu1+G_Au1wb1)/C_Au1,0,G_Au1wb1/C_Au1,0,0,0,0,0,0],
             [0,0,0,G_ag/C_g,0,-(G_ag+G_gSi)/C_g,0,0,G_gSi/C_g,0,0,0,0],
             [0,0,0,0,G_Au1wb1/C_wb1,0,-(G_Au1wb1+G_wb1Au2)/C_wb1,G_wb1Au2/C_wb1,0,0,0,0,0],
             [0,0,G_Au2Te/C_Au2,0,0,0,G_wb1Au2/C_Au2,-(G_Au2Te+G_wb1Au2+G_SiAu2)/C_Au2,G_SiAu2/C_Au2,0,0,0,0],
             [0,0,0,0,0,G_gSi/C_Si,0,G_SiAu2/C_Si,-(G_gSi+G_SiAu2+G_SiTp+G_Sim)/C_Si,G_SiTp/C_Si,G_Sim/C_Si,0,0],
             [0,0,G_TpTe/C_Tp,0,0,0,0,0,G_SiTp/C_Tp,-(G_TpTe+G_SiTp)/C_Tp,0,0,0],
             [0,0,G_Tem/C_m,0,0,0,0,0,G_Sim/C_m,0,-(G_Tem+G_Sim+G_mwb2)/C_m,G_mwb2/C_m,0],
             [0,0,0,0,0,0,0,0,0,0,G_mwb2/C_wb2,-(G_mwb2+G_wb2b)/C_wb2,G_wb2b/C_wb2],
             [0,0,0,G_ab/C_b,0,0,0,0,0,0,0,G_wb2b/C_b,-(G_wb2b+G_ab)/C_b]])

### Solving the equations ###
Eig, P = LA.eig(N) # Compute eigenvalues and eigenvectors of M
P_1 = inv(P) # Compute inverse of the eigenvectors
phi0 = np.array([0,0,0,1,0,0,0,0,0,0,0,0,0])  # Input pulse
#phi0 = np.array([0,0,1,0,0,0,0,0,0,0,0,0,0])  # Input pulse
#phi0 = np.array([0,0,0,0,0,0,0,0,1,0,0,0,0])  # Input pulse
A = P_1.dot(phi0)
print("eigenvalues",Eig)
    

deltaT_Mat = P.dot(A)
tau = 1.0/Eig
t = np.linspace(0,300,num=300,endpoint=True)

exp_vec = map(lambda x,y: y*np.exp(t/x),tau,A)
deltaT_exp = P.dot(exp_vec)

dI, dV, dT_Te, dT_a, dT_Au1, dT_g, dT_wb1, dT_Au2, dT_Si, dT_Tp, dT_m, dT_wb2, dT_b = map(np.array, deltaT_exp)


### Plot The Results###
fig = plt.figure(figsize=(18, 18))

plt.subplot(10,1,1)
plt.plot(dT_a,label="Absorber Temperature")
plt.ylabel("Temperature")
plt.legend()
plt.title("Linear Solver")

plt.subplot(10,1,2)
plt.plot(dI,label="Current")
plt.ylabel("Current")
plt.legend()


plt.subplot(10,1,3)
plt.plot(dV,label="Voltage")
plt.ylabel("Voltage")
plt.legend()

plt.subplot(10,1,4)
plt.plot(dT_Au1,label="Gold Pad 1 Temperature")
plt.plot(dT_wb1,label="Wirebond 1 Temperature")
plt.ylabel("Temperature")
plt.legend()

plt.subplot(10,1,5)
plt.plot(dT_g,label="Glue Temperature")
plt.ylabel("Temperature")
plt.legend()

plt.subplot(10,1,6)
plt.plot(dT_Si,label="Silicon Chip Temperature")
plt.ylabel("Temperature")
plt.legend()

plt.subplot(10,1,7)
plt.plot(dT_Te,label="TES Electron System Temperature")
plt.plot(dT_Tp,label="TES Phonon System Temperature")
plt.ylabel("Temperature")
plt.legend()

plt.subplot(10,1,8)
plt.plot(dT_Au2,label="Gold Pad 2 Temperature")
plt.ylabel("Temperature")
plt.legend()

plt.subplot(10,1,9)
plt.plot(dT_m,label="Meander Temperature")
plt.plot(dT_wb2,label="Wirebond 2 Temperature")
plt.ylabel("Temperature")
plt.legend()

plt.subplot(10,1,10)
plt.plot(dT_b,label="Bath Temperature")
plt.ylabel("Temperature")
plt.legend()
plt.xlabel("Time [0.01ms]")

