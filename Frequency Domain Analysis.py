import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions

from numpy.linalg import inv #import inversion function
from scipy.integrate import quad
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object
import yaml

#Use YAML to read the input file and turn it into a Python Dictionary
stream = open("input.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')

p['R0']     = p['Rn'] * p['frac']
p['P_0']    = p['I0_NL']**2*p['R0']              #Equilibrium power through TES [J]
p['V_0']    = p['I0_NL']*p['R0']            #Equilibrium Voltage [V]
z = 1j

#      0   1   2  3     4   5   6   7  8     
#T = [Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Tm,Twb2]
#T = [0.0200000913177,0.0200000913944,0.0200000912899,0.0200000913953,0.0200000913961,0.020000091262,0.0200000913972,0.0200000047124,0.0100034358387]
#T = [0.020,0.020,0.200,0.020,0.020,0.020,0.020,0.020,0.01]
T = [2.00000016e-02,   2.00000068e-02,   2.00000012e-02,2.00000068e-02,   2.00000068e-02,   2.00000009e-02,   2.00000068e-02,1.99999200e-02,   1.00008617e-02]

p['I0_NL'] = 1.73199182e-06

def matrix(w):
    N = np.array([[1.*p['R0']*(1.+p['beta0'])+z*w*p['L'],-1.,1.*p['alpha0']*p['V_0']/(p['Tc']),0,0,0,0,0,0,0,0],
             [p['Rl'],1.+z*w*p['Rl']*p['Ccap'],0,0,0,0,0,0,0,0,0],  
             [-(2.+p['beta0'])*p['V_0'],0,(p['G_au2te']+p['G_site']+p['G_tem']-p['alpha0']*p['P_0']/p['Tc'])+z*w*p['cte'],0,0,0,0,-p['G_au2te'],-p['G_site'],-p['G_tem'],0],
             [0,0,0,(p['G_ab']+p['G_aau1']+p['G_ag'])+z*w*p['ca'],-p['G_aau1'],-p['G_ag'],0,0,0,0,0],
             [0,0,0,-p['G_aau1'],(p['G_aau1']+p['G_au1wb1'])+z*w*p['cau1'],0,-p['G_au1wb1'],0,0,0,0],
             [0,0,0,-p['G_ag'],0,(p['G_ag']+p['G_gsi'])+z*w*p['cg'],0,0,-p['G_gsi'],0,0],
             [0,0,0,0,-p['G_au1wb1'],0,(p['G_au1wb1']+p['G_wb1au2'])+z*w*p['cwb1'],-p['G_wb1au2'],0,0,0],
             [0,0,-p['G_au2te'],0,0,0,-p['G_wb1au2'],(p['G_au2te']+p['G_wb1au2']+p['G_siau2'])+z*w*p['cau2'],-p['G_siau2'],0,0],
             [0,0,-p['G_site'],0,0,-p['G_gsi'],0,-p['G_siau2'],(p['G_gsi']+p['G_siau2']+p['G_site']+p['G_sim'])+z*w*p['csi'],-p['G_sim'],0],
             [0,0,-p['G_tem'],0,0,0,0,0,-p['G_sim'],(p['G_tem']+p['G_sim']+p['G_mwb2'])+z*w*p['cm'],-p['G_mwb2']],
             [0,0,0,0,0,0,0,0,0,-p['G_mwb2'],(p['G_mwb2']+p['G_wb2b'])+z*w*p['cwb2']]]) 
    return N

def phononnoise(G,T1,T2,n):
    noise = np.sqrt(2.*p['kb']*G*(T1**(2) + T2**(2)))
    return noise

eint = np.sqrt(4.*p['kb']*T[6]*p['R0'])      #Internal Johnson Noise
eext = np.sqrt(4.*p['kb']*p['Tb']*p['Rl'])     #external Johnson Noise across shunt resistor at bath temperature
pau2te = phononnoise(p['G_au2te'],T[4],T[6],p['nau2te'])
ptem = phononnoise(p['G_tem'],T[6],T[7],p['ntem'])
psite = phononnoise(p['G_site'],T[5],T[6],p['nsite'])
pag = phononnoise(p['G_ag'],T[0],T[2],p['nag'])
paau1 = phononnoise(p['G_aau1'],T[0],T[1],p['naau1'])
pab = phononnoise(p['G_ab'],T[0],p['Tb'],p['nau2te'])
pgsi = phononnoise(p['G_gsi'],T[5],T[2],p['ngsi'])
pau1wb1 = phononnoise(p['G_au1wb1'],T[1],T[3],p['nau1wb1'])
pwb1au2 = phononnoise(p['G_wb1au2'],T[3],T[4],p['nwb1au2'])
psiau2 = phononnoise(p['G_siau2'],T[5],T[4],p['nsiau2'])
psim = phononnoise(p['G_sim'],T[5],T[7],p['nsim'])
pmwb2 = phononnoise(p['G_mwb2'],T[7],T[8],p['nmwb2'])
pwb2b = phononnoise(p['G_wb2b'],T[8],p['Tb'],p['nwb2b'])
pulse = 0.

def S(w):
    s = inv(matrix(w))
    return s

x_eint      = np.array([eint,0,   -eint*p['I0_NL'],0,     0,       0,       0,       0,      0,      0,      0])
x_eext      = np.array([0,   eext,0,               0,     0,       0,       0,       0,      0,      0,      0])
x_pau2te    = np.array([0,   0,   pau2te,          0,     0,       0,       0,       -pau2te,0,      0,      0])
x_ptem      = np.array([0,   0,   -ptem,           0,     0,       0,       0,       0,      0,      ptem,   0])
x_psite     = np.array([0,   0,   psite,           0,     0,       0,       0,       0,      -psite, 0,      0])
x_pag       = np.array([0,   0,   0,               -pag,  0,       pag,     0,       0,      0,      0,      0])
x_paau1     = np.array([0,   0,   0,               -paau1,paau1,   0,       0,       0,      0,      0,      0])
x_pab       = np.array([0,   0,   0,               -pab,  0,       0,       0,       0,      0,      0,      0])
x_pulse     = np.array([0,   0,   0,               pulse, 0,       0,       0,       0,      0,      0,      0])
x_pgsi      = np.array([0,   0,   0,               0,     0,       -pgsi,   0,       0,      pgsi,   0,      0])
x_pau1wb1   = np.array([0,   0,   0,               0,     -pau1wb1,0,       pau1wb1, 0,      0,      0,      0])
x_pwb1au2   = np.array([0,   0,   0,               0,     0,       0,       -pwb1au2,pwb1au2,0,      0,      0])
x_psiau2    = np.array([0,   0,   0,               0,     0,       0,       0,       psiau2, -psiau2,0,      0])
x_psim      = np.array([0,   0,   0,               0,     0,       0,       0,       0,      -psim,  psim,   0])
x_pmwb2     = np.array([0,   0,   0,               0,     0,       0,       0,       0,      0,      -pmwb2, pmwb2])
x_pwb2b     = np.array([0,   0,   0,               0,     0,       0,       0,       0,      0,      0,      -pwb2b])
def noise_eint(w):
    y = np.matmul(S(w),x_eint)
    return np.abs(y[0])
def noise_eext(w):
    y = np.matmul(S(w),x_eext)
    return np.abs(y[0])
def noise_pau2te(w):
    y = np.matmul(S(w),x_pau2te)
    return np.abs(y[0])
def noise_ptem(w):
    y = np.matmul(S(w),x_ptem)
    return np.abs(y[0])
def noise_psite(w):
    y = np.matmul(S(w),x_psite)
    return np.abs(y[0])
def noise_pag(w):
    y = np.matmul(S(w),x_pag)
    return np.abs(y[0])
def noise_paau1(w):
    y = np.matmul(S(w),x_paau1)
    return np.abs(y[0])
def noise_pab(w):
    y = np.matmul(S(w),x_pab)
    return np.abs(y[0])
def noise_pulse(w):
    y = np.matmul(S(w),x_pulse)
    return np.abs(y[0])
def noise_pgsi(w):
    y = np.matmul(S(w),x_pgsi)
    return np.abs(y[0])
def noise_pau1wb1(w):
    y = np.matmul(S(w),x_pau1wb1)
    return np.abs(y[0])
def noise_pwb1au2(w):
    y = np.matmul(S(w),x_pwb1au2)
    return np.abs(y[0])
def noise_psiau2(w):
    y = np.matmul(S(w),x_psiau2)
    return np.abs(y[0])
def noise_psim(w):
    y = np.matmul(S(w),x_psim)
    return np.abs(y[0])
def noise_pmwb2(w):
    y = np.matmul(S(w),x_pmwb2)
    return np.abs(y[0])
def noise_pwb2b(w):
    y = np.matmul(S(w),x_pwb2b)
    return np.abs(y[0])
def total_noise(w):
    noise = np.sqrt(noise_eint(w)**2+noise_eext(w)**2+noise_pau2te(w)**2+noise_ptem(w)**2+noise_psite(w)**2+noise_pag(w)**2+noise_paau1(w)**2+noise_pab(w)**2+noise_pulse(w)**2+noise_pgsi(w)**2+noise_pau1wb1(w)**2+noise_pwb1au2(w)**2+noise_psiau2(w)**2+noise_psim(w)**2+noise_pmwb2(w)**2+noise_pwb2b(w)**2)
    return noise


def NEP_eint(w):
    nep = np.abs(noise_eint(w)/S(w)[0,3])
    return nep
def NEP_eext(w):
    nep = np.abs(noise_eext(w)/S(w)[0,3])
    return nep
def NEP_pau2te(w):
    nep = np.abs(noise_pau2te(w)/S(w)[0,3])
    return nep
def NEP_ptem(w):
    nep = np.abs(noise_ptem(w)/S(w)[0,3])
    return nep
def NEP_psite(w):
    nep = np.abs(noise_psite(w)/S(w)[0,3])
    return nep
def NEP_pag(w):
    nep = np.abs(noise_pag(w)/S(w)[0,3])
    return nep
def NEP_paau1(w):
    nep = np.abs(noise_paau1(w)/S(w)[0,3])
    return nep
def NEP_pab(w):
    nep = np.abs(noise_pab(w)/S(w)[0,3])
    return nep
def NEP_pulse(w):
    nep = np.abs(noise_pulse(w)/S(w)[0,3])
    return nep
def NEP_pgsi(w):
    nep = np.abs(noise_pgsi(w)/S(w)[0,3])
    return nep
def NEP_pau1wb1(w):
    nep = np.abs(noise_pau1wb1(w)/S(w)[0,3])
    return nep
def NEP_pwb1au2(w):
    nep = np.abs(noise_pwb1au2(w)/S(w)[0,3])
    return nep
def NEP_psiau2(w):
    nep = np.abs(noise_psiau2(w)/S(w)[0,3])
    return nep
def NEP_psim(w):
    nep = np.abs(noise_psim(w)/S(w)[0,3])
    return nep
def NEP_pmwb2(w):
    nep = np.abs(noise_pmwb2(w)/S(w)[0,3])
    return nep
def NEP_pwb2b(w):
    nep = np.abs(noise_pwb2b(w)/S(w)[0,3])
    return nep

def integrand(f):
    w = 2*np.pi*f
    NEP_total_squared = NEP_eint(w)**2+NEP_eext(w)**2+NEP_pau2te(w)**2+NEP_ptem(w)**2+NEP_psite(w)**2+NEP_pag(w)**2+NEP_paau1(w)**2+NEP_pab(w)**2+NEP_pulse(w)**2+NEP_pgsi(w)**2+NEP_pau1wb1(w)**2+NEP_pwb1au2(w)**2+NEP_psiau2(w)**2+NEP_psim(w)**2+NEP_pmwb2(w)**2+NEP_pwb2b(w)**2
    integrand = 1./NEP_total_squared
    return integrand

integral = quad(integrand,0,np.inf)
resolution = 2.35*np.sqrt(4.*integral[0])**(-1.)
resolution_ev = resolution/1.60218e-19
print("resolution in eV = ",resolution_ev)
#dont hardcode, ideal tes resolution
rough_resolution = 2.355*np.sqrt(4*p['kb']*p['Tc']**2*5.5824895686e-11/100.*np.sqrt(2./2.))
rough_resolution_ev = rough_resolution/1.60218e-19
print("rough_resolution ev",rough_resolution_ev)


### Noise Plots ### 
frequencies = np.logspace(p['freq_min_exp'],p['freq_max_exp'],int(p['log_points']))

fig = plt.figure(figsize=(18, 18))
plot_flag = 0
array_eint = []
array_eext = []
array_pau2te = []
array_ptem = []
array_psite = []
array_pag = []
array_paau1 = []
array_pab = []
array_pulse = []
array_pgsi = []
array_pau1wb1 = []
array_pwb1au2 = []
array_psiau2 = []
array_psim = []
array_pmwb2 = []
array_pwb2b = []
array_total = []
if plot_flag == 0:
    for i in range(0,len(frequencies)):
            array_eint.append(noise_eint(2.*np.pi*frequencies[i]))
            array_eext.append(noise_eext(2.*np.pi*frequencies[i]))
            array_pau2te.append(noise_pau2te(2.*np.pi*frequencies[i]))
            array_ptem.append(noise_ptem(2.*np.pi*frequencies[i]))
            array_psite.append(noise_psite(2.*np.pi*frequencies[i]))
            array_pag.append(noise_pag(2.*np.pi*frequencies[i]))
            array_paau1.append(noise_paau1(2.*np.pi*frequencies[i]))
            array_pab.append(noise_pab(2.*np.pi*frequencies[i]))
            array_pulse.append(noise_pulse(2.*np.pi*frequencies[i]))
            array_pgsi.append(noise_pgsi(2.*np.pi*frequencies[i]))
            array_pau1wb1.append(noise_pau1wb1(2.*np.pi*frequencies[i]))
            array_pwb1au2.append(noise_pwb1au2(2.*np.pi*frequencies[i]))
            array_psiau2.append(noise_psiau2(2.*np.pi*frequencies[i]))
            array_psim.append(noise_psim(2.*np.pi*frequencies[i]))
            array_pmwb2.append(noise_pmwb2(2.*np.pi*frequencies[i]))
            array_pwb2b.append(noise_pwb2b(2.*np.pi*frequencies[i]))
            array_total.append(total_noise(2.*np.pi*frequencies[i]))
if plot_flag == 1:
    for i in range(0,len(frequencies)):
            array_eint.append(NEP_eint(2.*np.pi*frequencies[i]))
            array_eext.append(NEP_eext(2.*np.pi*frequencies[i]))
            array_pau2te.append(NEP_pau2te(2.*np.pi*frequencies[i]))
            array_ptem.append(NEP_ptem(2.*np.pi*frequencies[i]))
            array_psite.append(NEP_psite(2.*np.pi*frequencies[i]))
            array_pag.append(NEP_pag(2.*np.pi*frequencies[i]))
            array_paau1.append(NEP_paau1(2.*np.pi*frequencies[i]))
            array_pab.append(NEP_pab(2.*np.pi*frequencies[i]))
            array_pulse.append(NEP_pulse(2.*np.pi*frequencies[i]))
            array_pgsi.append(NEP_pgsi(2.*np.pi*frequencies[i]))
            array_pau1wb1.append(NEP_pau1wb1(2.*np.pi*frequencies[i]))
            array_pwb1au2.append(NEP_pwb1au2(2.*np.pi*frequencies[i]))
            array_psiau2.append(NEP_psiau2(2.*np.pi*frequencies[i]))
            array_psim.append(NEP_psim(2.*np.pi*frequencies[i]))
            array_pmwb2.append(NEP_pmwb2(2.*np.pi*frequencies[i]))
            array_pwb2b.append(NEP_pwb2b(2.*np.pi*frequencies[i]))
            array_total.append(np.sqrt(1./integrand(2.*np.pi*frequencies[i])))

plt.loglog(frequencies,array_eint,label = 'eint')
plt.loglog(frequencies,array_eext,label = 'eext')
plt.loglog(frequencies,array_pau2te,label = 'pau2te')
plt.loglog(frequencies,array_ptem,label = 'ptem')
plt.loglog(frequencies,array_psite,label = 'psite')
plt.loglog(frequencies,array_pag,label = 'pag')
plt.loglog(frequencies,array_paau1,label = 'paau1')
plt.loglog(frequencies,array_pab,label = 'pab')
plt.loglog(frequencies,array_pulse,label = 'pulse')
plt.loglog(frequencies,array_pgsi,label = 'pgsi')
plt.loglog(frequencies,array_pau1wb1,label = 'pau1wb1',linestyle = ':')
plt.loglog(frequencies,array_pwb1au2,label = 'pwb1au2',linestyle = ':')
plt.loglog(frequencies,array_psiau2,label = 'psiau2',linestyle = ':')
plt.loglog(frequencies,array_psim,label = 'psim',linestyle = ':')
plt.loglog(frequencies,array_pmwb2,label = 'pmwb2',linestyle = ':')
plt.loglog(frequencies,array_pwb2b,label = 'pwb2b',linestyle = ':')
plt.loglog(frequencies,array_total,label = 'total',linewidth = 2.,color = 'k')
axes = plt.gca()
#axes.set_ylim([1e-14,2e-11])
if plot_flag == 0:
    plt.title("Frequency vs Noise")
    plt.ylabel("Log Current Noise [A/$\sqrt{Hz}$]")
if plot_flag == 1:
    plt.title("Frequency vs NEP")
    plt.ylabel("Log Current NEP [W/$\sqrt{Hz}$]")
plt.xlabel("Log Frequency")
plt.legend()
plt.grid()
plt.savefig('Frequency vs Noise 24 July 2017',format = 'png')
plt.show()