import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object

PSD = np.zeros(2048)
nyquist = float(len(PSD))*2.

PSD[2] = 2.

PSD = np.append(PSD,np.conj(np.flip(PSD,0)))
PSD = np.insert(PSD,0,0.)

IFFT = np.fft.ifft(PSD)

plt.plot(PSD[1:2048])
plt.xlabel("Frequency in Hz")
plt.show()

times = np.zeros(4097)
for i in range(0,len(times)):
    times[i] = float(i)/nyquist

plt.plot(times,IFFT)
plt.xlabel("Time")
plt.show()

FFTofIFFT = np.fft.fft(IFFT)

plt.plot(FFTofIFFT)
plt.show()