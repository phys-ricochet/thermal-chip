import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions
from numpy.linalg import inv #import inversion function
from numpy import linalg as LA #Import Linear Algebra
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object
from iminuit import Minuit
import yaml
from scipy.integrate import quad # Import integrator
from scipy import stats,special
from scipy.optimize import curve_fit

#Define number of traces desired
trace_num = 100
noise_trace_multiplier = 1.
percent_variance = 0.0
stdev_trace_num = 2000

plt.rcParams["figure.figsize"] = [10,8]

stream = open("mock data generator.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')
        
### To make the program run faster we make some values global variables 
Rn =  p['Rn']           # Normal Resistance of TES [Ohms]
Tc = p['Tc']            # Tc of the TES [Kelvin]
alpha0 = p['alpha0']    # Temperature sensitivity [unitless]
beta0 = p['beta0']      # Current Sensitivity [unitless]

p['VGC'] = p['AGC']*p['LGC']                                            #Volume that the parameter file Conductances and Capacities are at
p['Lm'] = p['Am']*p['conductivity_m']*p['Tb']/(p['G_mwb2']*p['TGC'])    #Length of the meander, calculated from the user provided conductance in the parameter file

### Heat Capacities
#This is a function to calculate the heat capacities for us
def capacity(T,c1,c3,c5,A,L):
    C = c1*(T/p['TGC'])*A*L/p['VGC']+c3*(T/p['TGC'])**3*A*L/p['VGC']+c5*(T/p['TGC'])**5*A*L/p['VGC'] #scale for volume and temperature
    return C
#This is a function that is used to compute the Debye function for the heat capacity of the Silicone Oil (Polydimethylsiloxane i.e. PDMS)
def debye_integrand(x):
    D = x**3/(np.exp(x) - 1.0)
    return D

### Thermal Conductances
# Function to calculate non electron phonon coupling conductances
def conductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*(p['LGC']*A/(p['AGC']*L))#scale for area, length, and temperature
    return G
# Function to calculate electron phonon coupling conductances
def ephconductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*A*L/p['VGC'] #scale for volume and temperature
    return G

def calc_params(std):
    cg          = np.random.normal(1.,std)*6.947*3.0*(p['Tc']/69.0)**3.0*quad(debye_integrand,0,69.0/p['Tc'])[0]*0.8*p['Ag']*p['Lg']*1.0e+6/74.1539 #Heat Capacity of "Glue", currently PDMS [J/K]
    ca          = np.random.normal(1.,std)*1.*capacity(p['Tc'],p['ca_1_05'],p['ca_3_05'],0.0,p['Aa'],p['La'])                                               #Heat Capacity of Absorber [J/K]
    cau1        = np.random.normal(1.,std)*capacity(p['Tc'],p['cau1_1_05'],p['cau1_3_05'],0.0,p['Aau1'],p['Lau1'])                                       #Heat Capacity of Gold Pad 1 [J/K]
    cwb1        = np.random.normal(1.,std)*capacity(p['Tc'],p['cwb1_1_05'],p['cwb1_3_05'],0.0,p['Awb1'],p['Lwb1'])                                       #Heat Capacity of Wirebond 1 [J/K]
    cau2        = np.random.normal(1.,std)*capacity(p['Tc'],p['cau2_1_05'],p['cau2_3_05'],0.0,p['Aau2'],p['Lau2'])                                       #Heat Capacity of Gold Pad 2 [J/K]
    csi         = np.random.normal(1.,std)*capacity(p['Tc'],p['csi_1_05'],p['csi_3_05'],0.0,p['Asi'],p['Lsi'])                                           #Heat Capacity of Silicon Wafer [J/K]
    cte         = np.random.normal(1.,std)*capacity(p['Tc'],p['cte_1_05'],p['cte_3_05'],0.0,p['Ate'],p['Lte'])                                           #Heat Capacity of TES Electron System [J/K]
    cm          = np.random.normal(1.,std)*capacity(p['Tb'],p['cm_1_05'],p['cm_3_05'],0.0,p['Am'],p['Lm'])+cau2                                     #Heat Capacity of Meander [J/K]
    cwb2        = np.random.normal(1.,std)*capacity(p['Tb'],p['cwb2_1_05'],p['cwb2_3_05'],0.0,p['Awb2'],p['Lwb2'])                                       #Heat Capacity of Wirebond 2 [J/K]
    ctot        = ca + cau1 +  cg  +  cwb1  +  cau2  +  csi  +  cte  +  cm  +  cwb2      #Heat Capacity of Total [J/K]
    G_ab        = np.random.normal(1.,std)*conductance(p['Tc'],p['G_ab_05'],p['nab'],p['Ab'],p['Lb'])                  #Thermal Conductance of Absorber to Bath [W/K]
    G_ag        = np.random.normal(1.,std)*conductance(p['Tc'],p['G_ag_05'],p['nag'],p['Ag'],p['Lg'])                  #Thermal Conductance of Absorber to Glue [W/K]
    G_gsi       = np.random.normal(1.,std)*1*conductance(p['Tc'],p['G_gsi_05'],p['ngsi'],p['Ag'],p['Lg'])               #Thermal Conductance of Glue to Silicon Wafer [W/K]
    G_au1wb1    = np.random.normal(1.,std)*conductance(p['Tc'],p['G_au1wb1_05'],p['nau1wb1'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Gold Pad 1 to Wirebond 1 [W/K]
    G_wb1au2    = np.random.normal(1.,std)*1*conductance(p['Tc'],p['G_wb1au2_05'],p['nwb1au2'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Wirebond 1 to Gold Pad 2 [W/K]
    G_au2te     = np.random.normal(1.,std)*1*conductance(p['Tc'],p['G_au2te_05'],p['nau2te'],p['Aau2'],p['Lau2'])     #Thermal Conductance of Gold Pad 2 to TES [W/K]
    G_tem       = np.random.normal(1.,std)*1*conductance(p['Tc'],p['G_tem_05'],p['ntem'],p['Ate'],p['Lte'])             #Thermal Conductance of TES to Meander [W/K]
    G_wb2b      = np.random.normal(1.,std)*conductance(p['Tb'],p['G_wb2b_05'],p['nwb2b'],p['Awb2'],p['Lwb2'])        #Thermal Conductance of Wirebond 2 to Bath [W/K]
    G_aau1      = np.random.normal(1.,std)*ephconductance(p['Tc'],p['G_aau1_05'],p['naau1'],p['Aau1'],p['Lau1'])     #Thermal Conductance of Absorber to Gold Pad 2 [W/K]
    G_siau2     = np.random.normal(1.,std)*1*ephconductance(p['Tc'],p['G_siau2_05'],p['nsiau2'],p['Aau2'],p['Lau2'])  #Thermal Conductance of Silicon Wafer to Gold Pad 2 [W/K]
    G_site      = np.random.normal(1.,std)*1*ephconductance(p['Tc'],p['G_site_05'],p['nsite'],p['Ate'],p['Lte'])       #Thermal Conductance of Silicon Wafer to TES [W/K]
    G_sim       = np.random.normal(1.,std)*1*p['G_sim_05']*(p['Tc']/p['TGC'])**(p['nsim'] - 1.)*(p['Am']*p['Lm']+p['Aau2']*p['Lau2'])/p['VGC']            #Thermal Conductance of Silicon Wafer to Meander [W/K]
    kab         = G_ab/(p['nab']*p['Tc']**(p['nab']-1.))                       #Conductance Constant Absorber to Bath [W/K^4]
    kaau1       = G_aau1/(p['naau1']*p['Tc']**(p['naau1']-1.))                 #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
    kag         = G_ag/(p['nag']*p['Tc']**(p['nag']-1.))                       #Conductance Constant Absorber to Glue [W/K^3.5]
    kau1wb1     = G_au1wb1/(p['nau1wb1']*p['Tc']**(p['nau1wb1']-1.))           #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
    kgsi        = G_gsi/(p['ngsi']*p['Tc']**(p['ngsi']-1.))                    #Conductance Constant Glue to Silicon [W/K^3.5]
    kwb1au2     = G_wb1au2/(p['nwb1au2']*p['Tc']**(p['nwb1au2']-1.))           #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
    ksiau2      = G_siau2/(p['nsiau2']*p['Tc']**(p['nsiau2']-1.))              #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
    kau2te      = G_au2te/(p['nau2te']*p['Tc']**(p['nau2te']-1.))              #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
    ksite       = G_site/(p['nsite']*p['Tc']**(p['nsite']-1.))                 #Conductance Constant Silicon to TES Phonon System [W/K^4]
    ksim        = G_sim/(p['nsim']*p['Tc']**(p['nsim']-1.))                    #Conductance Constant Silicon to Meander [W/K^4]
    ktem        = G_tem/(p['ntem']*p['Tc']**(p['ntem']-1.))                    #Conductance Constant TES Electron System to MEander [W/K^2]
    kmwb2       = p['G_mwb2']/(p['nmwb2']*p['Tb']**(p['nmwb2']-1.))                 #Conductance Constant Meander to Wirebond 2 [W/K^2]
    kwb2b       = G_wb2b/(p['nwb2b']*p['Tb']**(p['nwb2b']-1.))                 #Conductance Constant Wirecond 2 to Bath [W/K^4]
    R0          = Rn  * p['frac']                       #Equilibrium resistance of TES [Ohms]
    P_0         = p['I0_NL']**2* R0                      #Equilibrium power through TES [J]
    V_0         = p['I0_NL']* R0                         #Equilibrium Voltage [V]
    P0          = kmwb2 *((p['Tc'])**2-(p['Tb'])**2)    #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
    I0_P        = np.sqrt( P0 / R0 )                  #Equilibrium Current calculated from power, used in resistance equation
    vb          = I0_P  * p['Rl']                       #Voltage Bias
    return cg ,  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_ag , G_gsi , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, kab , kaau1 , kag , kau1wb1 , kgsi , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, R0 , P_0 , V_0 , P0 , I0_P , vb


def compute_pulse(x1,x2,x3,calc_params_std):
    #Calculate the parameters from the input vector x
    cg ,  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_ag , G_gsi , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, kab , kaau1 , kag , kau1wb1 , kgsi , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, R0 , P_0 , V_0 , P0 , I0_P , vb = calc_params(calc_params_std)
    
    p['E'] = x1
    p['G_mwb2'] = x2
    G_aau1 = x3
    
    # Define the matrix for linear solver
    def N():
        
        p['E'] = x1
        p['G_mwb2'] = x2
        G_aau1 = x3

        N = np.array([[-1.* R0 *(1.+ beta0 )/ p['L'] , -1.* alpha0 * V_0 /( Tc * p['L'] ),0,0,0,0,0,0,0,0],
             [(2.+ beta0 )* V_0 /( cte ),-( G_au2te + G_site + G_tem - alpha0 * P_0 / Tc )/ cte ,0,0,0,0, G_au2te / cte , G_site / cte , G_tem / cte ,0],
             [0,0,-( G_ab + G_aau1 + G_ag )/ ca , G_aau1 / ca , G_ag / ca ,0,0,0,0,0],
             [0,0, G_aau1 / cau1 ,-( G_aau1 + G_au1wb1 )/ cau1 ,0, G_au1wb1 / cau1 ,0,0,0,0],
             [0,0, G_ag / cg ,0,-( G_ag + G_gsi )/ cg ,0,0, G_gsi / cg ,0,0],
             [0,0,0, G_au1wb1 / cwb1 ,0,-( G_au1wb1 + G_wb1au2 )/ cwb1 , G_wb1au2 / cwb1 ,0,0,0],
             [0, G_au2te / cau2 ,0,0,0, G_wb1au2 / cau2 ,-( G_au2te + G_wb1au2 + G_siau2 )/ cau2 , G_siau2 / cau2 ,0,0],
             [0, G_site / csi ,0,0, G_gsi / csi ,0, G_siau2 / csi ,-( G_gsi + G_siau2 + G_site + G_sim )/ csi , G_sim / csi ,0],
             [0, G_tem / cm ,0,0,0,0,0, G_sim / cm ,-( G_tem + G_sim + p['G_mwb2'] )/ cm , p['G_mwb2'] / cm ],
             [0,0,0,0,0,0,0,0, p['G_mwb2'] / cwb2 ,-( p['G_mwb2'] + G_wb2b )/ cwb2 ]])
        return N
    matrix = N()
    #Solve the linear case
    Eig, P = LA.eig(matrix) # Compute eigenvalues (Eig) and eigenvectors (P) of M
    P_1 = inv(P) # Compute inverse of the eigenvectors
    phi0 = np.array([0,0,p['E']/ca,0,0,0,0,0,0,0])  # Pulse to Absorber
    A = P_1.dot(phi0) #Dot the inverse of the eigenvector with the input pulse to get our coefficients
    tau = 1.0/Eig #get the time constants from the eigenvalues
    LINt = np.linspace(0,p['lintime'],num = int(p['numlinpts']),endpoint=True) #create an array of times to evaluate our solutions at   
    exp_vec = map(lambda x,y: y*np.exp(LINt/x),tau,A) # Form the exponential basis terms
    deltaT_exp = P.dot(exp_vec) # create a vector of the solutions
    dI, dT_Te, dT_a, dT_Au1, dT_g, dT_wb1, dT_Au2, dT_Si, dT_m, dT_wb2 = map(np.array, deltaT_exp)
    for i in range(0,819):
        dI = np.insert(dI,0,dI[-1])
    return dI

#Import the raw pulse and the noise spectrum
data = np.load('NPZ of Linear Data Electronics Noise Spectrum.npz')
I_L = data['current']
t_L = data['time']
del data.f
data.close()

"""
#The pulse does not have a baseline in the beginning, so this adds the baseline
#by inserting the equilibrium value (I_L[-1]) at the beginning.  This is done
#until there are 4096 points in the trace
for i in range(0,819):
    I_L = np.insert(I_L,0,I_L[-1])
    t_L = np.append(t_L,t_L[-1] + 0.00002441)

#Define a random generator for the noise traces
def get_rand():
    return np.random.random()

#Define a function to generate a single noise trace
def get_noise_trace(spectrum):
    #Multiply each of the noise terms by a complex exponential, giving the noise a 
    #random phase at each bin
    for i in range(0,len(spectrum)):
        rand = get_rand()
        spectrum[i] = spectrum[i] * np.exp(1j*2*np.pi*rand)
    
    #Because the noise_total array only has the positive frequencies, append the 
    #negative frequencies in the proper place, and then insert the zero frequency 
    #term
    spectrum = np.append(spectrum,np.conj(np.flip(spectrum,0)))
    spectrum = np.insert(spectrum,0,0.)
    noise = np.fft.ifft(spectrum)
    return noise.real * noise_trace_multiplier

pulse_dict = {}
#Get trace_num random noise traces
for i in range(0,trace_num):
    pulse_dict[str(i)] = get_noise_trace(noise_total)[1:4097] + I_L

plt.plot(get_noise_trace(noise_total))
plt.show()

"""
"""
#Import the raw pulse and the noise spectrum
data = np.load('NPZ of Linear Data Mock Data.npz')
t_L = data['time']
del data.f
data.close()
for i in range(0,819):
    t_L = np.append(t_L,t_L[-1] + 0.00002441)

def get_rand():
    return np.random.random()

def get_noise_trace():
    #Multiply each of the noise terms by a complex exponential, giving the noise a 
    #random phase at each bin
    data = np.load('NPZ of Frequency Data Mock Data.npz')
    noise_total = data['total']
    del data.f
    data.close()
    noise_total = noise_total.astype(complex)
    for i in range(0,len(noise_total)):
        rand = get_rand()
        noise_total[i] = noise_total[i] * np.exp(1j*2*np.pi*rand)
    noise_total = np.append(noise_total,np.conj(np.flip(noise_total,0)))
    noise_total = np.insert(noise_total,0,0.)
    noise = np.fft.ifft(noise_total)

    x1 = np.random.normal(1.60218e-17,1.60218e-17*percent_variance)
    x2 = np.random.normal(8.0e-11,8.0e-11*percent_variance)
    x3 = np.random.normal(2.048e-8,2.048e-8*percent_variance)
    I_L = compute_pulse(x1,x2,x3)
    return noise.real[1:4097] + I_L

pulse_dict = {}
#Get trace_num random noise traces
for i in range(0,trace_num):
    pulse_dict[str(i)] = get_noise_trace() 

pulse_ave = np.empty(0)

pulse_stdevs = np.empty(0)
stdev_trace_dict = {}
for i in range(0,stdev_trace_num):
    stdev_trace_dict[str(i)] = get_noise_trace()[1:4097]
for i in range(0,4096):
    hold = np.empty(0)
    for n in range(0,stdev_trace_num):
        hold = np.append(hold,stdev_trace_dict[str(n)][i])
    pulse_stdevs = np.append(pulse_stdevs,np.std(hold))

pulse_stdevs = pulse_stdevs / np.sqrt(trace_num)
"""
x_1 = 1.60218e-17/20.
x_2 = 8.0e-11
x_3 = 2.048e-8
I_L = compute_pulse(x_1,x_2,x_3,percent_variance)

#Import the raw pulse and the noise spectrum
data = np.load('NPZ of Linear Data Electronics Noise Spectrum.npz')
t_L = data['time']
del data.f
data.close()

for i in range(0,819):
    t_L = np.append(t_L,t_L[-1] + 0.00002441)

#Define a random generator for the noise traces
def get_rand():
    return np.random.random()

#Define a function to generate a single noise trace
def get_noise_trace():
    #Multiply each of the noise terms by a complex exponential, giving the noise a 
    #random phase at each bin
    data = np.load('NPZ of Frequency Data Electronics Noise Spectrum.npz')
    noise_total = data['total']
    del data.f
    data.close()
    noise_total = noise_total.astype(complex)
    new_noise = []
    for i in range(0, len(noise_total), 10):
        new_noise.append(noise_total[i])
    
    for i in range(0,len(new_noise)):
        rand = get_rand()
        new_noise[i] = new_noise[i] * np.exp(1j*2.*np.pi*rand)
    
    #Because the noise_total array only has the positive frequencies, append the 
    #negative frequencies in the proper place, and then insert the zero frequency 
    #term
    new_noise = np.append(new_noise,np.conj(np.flip(new_noise[0:2047],0)))
    new_noise[2048]=new_noise[2048] + np.conj(new_noise[2048])
    new_noise = np.insert(new_noise,0,0.)
    
    noise = np.fft.ifft(new_noise)
    
    return noise.real * noise_trace_multiplier

print("getting standard deviation noise traces")
pulse_stdevs = np.empty(0)
stdev_trace_dict = {}
for i in range(0,stdev_trace_num):
    print(i)
    stdev_trace_dict[str(i)] = get_noise_trace()
print("computing standard deviations")
for i in range(0,4096):
    hold = np.empty(0)
    print(i)
    for n in range(0,stdev_trace_num):
        hold = np.append(hold,stdev_trace_dict[str(n)][i])
    pulse_stdevs = np.append(pulse_stdevs,np.std(hold,ddof = 1))

pulse_stdevs = pulse_stdevs / np.sqrt(trace_num)
print("computing first batch of pulses")
pulse_dict = {}
#Get trace_num random noise traces
for i in range(0,trace_num):
    print(i)
    noise_trace = get_noise_trace()
    pulse_dict[str(i)] = noise_trace[0:4096] + I_L

pulse_ave = np.empty(0)
for i in range(0,4096):
    hold = np.empty(0)
    for n in range(0,trace_num):
        hold = np.append(hold,pulse_dict[str(n)][i])
    pulse_ave = np.append(pulse_ave,np.mean(hold))
    
print("computing second batch of pulses")
pulse_dict2 = {}
#Get trace_num random noise traces
for i in range(0,trace_num):
    print(i)
    noise_trace = get_noise_trace()
    pulse_dict2[str(i)] = noise_trace[0:4096] + I_L

pulse_ave2 = np.empty(0)
for i in range(0,4096):
    hold = np.empty(0)
    for n in range(0,trace_num):
        hold = np.append(hold,pulse_dict2[str(n)][i])
    pulse_ave2 = np.append(pulse_ave2,np.mean(hold))
"""
plt.plot(t_L,pulse_dict["0"])
plt.title("Single Noisy Pulse",fontsize = 40)
plt.xlabel("Time [s]",fontsize = 30)
plt.ylabel("Current Amplitude [A]",fontsize = 30)
plt.tick_params(axis = 'y',labelsize = 25)
plt.tick_params(axis = 'x',labelsize = 30)
plt.savefig("Single Noisey Pulse.png")
plt.show()

plt.plot(t_L,pulse_ave)
plt.title("Averaged Noisy Pulse",fontsize = 40)
plt.xlabel("Time [s]",fontsize = 30)
plt.ylabel("Current Amplitude [A]",fontsize = 30)
plt.tick_params(axis = 'y',labelsize = 25)
plt.tick_params(axis = 'x',labelsize = 30)
plt.savefig("Averaged Noisey Pulse.png")
plt.show()
"""

def least_squares(x1,x2,x3):
    least_squares = 0.
    dI = compute_pulse(x1,x2,x3,0.)
    for i in range(0,len(pulse_ave)):
        least_squares = least_squares + (pulse_ave[i]-dI[i])**2./(pulse_stdevs[i])**2.
    return least_squares

m = Minuit(least_squares,x1 = 2e-17/20., limit_x1 = (1.0e-19/20.,1.0e-15/20.),x2 = 1.0e-10,limit_x2 = (1.0e-12,1.0e-9),x3 = 5.0e-8,limit_x3 = (1.0e-9,1.0e-7))
m.migrad()

print("Energy Estimate     = " + str(m.values['x1']) + " +/- " + str(m.errors['x1']) + " sigma from true = " + str((x_1-m.values['x1'])/m.errors['x1']) + " with true value = " + str(x_1))
print("Fall Param Estimate = " + str(m.values['x2']) + " +/- " + str(m.errors['x2'])+ " sigma from true = " + str((x_2-m.values['x2'])/m.errors['x2']) + " with true value = " + str(x_2))
print("Rise Param Estimate = " + str(m.values['x3']) + " +/- " + str(m.errors['x3'])+ " sigma from true = " + str((x_3-m.values['x3'])/m.errors['x3']) + " with true value = " + str(x_3))

plt.errorbar(t_L,pulse_ave,yerr = pulse_stdevs,color = "y")
plt.plot(t_L,pulse_ave)
plt.title("Averaged Pulse",fontsize = 20)
plt.xlabel("Time [s]",fontsize = 15)
plt.ylabel("Current Amplitude [A]",fontsize = 18)
plt.tick_params(axis = 'y',labelsize = 10)
plt.tick_params(axis = 'x',labelsize = 10)
plt.savefig("Averaged Pulse with Error Bars.png")
plt.show()

#for i in range(0,trace_num):
#    plt.plot(pulse_dict[str(i)])
#    plt.hold(True)
#plt.errorbar(t_L,pulse_ave,yerr = pulse_stdevs,color = "y",linewidth = 0.2)
#plt.plot(t_L,pulse_ave,label = 'average pulse')
plt.errorbar(t_L,pulse_ave - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.),yerr = pulse_stdevs,color = "y",linewidth = 0.2)
#plt.plot(t_L,compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.),label = 'fit')
plt.scatter(t_L,pulse_ave - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.))
#plt.ylim(-2e-13,2e-13)
#plt.rcParams["figure.figsize"] = [12,9]
#plt.legend()
plt.title("(Averaged Pulse) - (Fit)",fontsize = 20)
plt.xlabel("Time [s]",fontsize = 15)
plt.ylabel("Current Amplitude [A]",fontsize = 18)
plt.tick_params(axis = 'y',labelsize = 10)
plt.tick_params(axis = 'x',labelsize = 10)
plt.savefig("Difference with Error Bars.png")
plt.show()

plt.plot(t_L,I_L - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.))
plt.errorbar(t_L,I_L - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.),yerr = pulse_stdevs,color = "y")
plt.title("(True Pulse) - (Fit)",fontsize = 20)
plt.xlabel("Time [s]",fontsize = 15)
plt.ylabel("Current Amplitude [A]",fontsize = 18)
plt.tick_params(axis = 'y',labelsize = 10)
plt.tick_params(axis = 'x',labelsize = 10)
plt.savefig("True Pulse Difference with Error Bars.png")
plt.show()

"""
plt.plot(t_L,pulse_ave,label = 'average pulse')
plt.plot(t_L,compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.),label = 'fit',linewidth = 0.75)
plt.legend()
plt.xlabel("Time [s]",fontsize = 18)
plt.ylabel("Current Amplitude [A]",fontsize = 18)
plt.tick_params(axis = 'y',labelsize = 10)
plt.tick_params(axis = 'x',labelsize = 10)
plt.title("Pulse With Fit",fontsize = 25)
plt.savefig("Pulse With Fit.png")
plt.show()
"""

#plt.plot(t_L,np.abs(pulse_ave - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.))/pulse_stdevs)
#plt.show()

chisquared = least_squares(m.values['x1'],m.values['x2'],m.values['x3'])
dof = 4096. - 3.

print("Chi Squared = " + str(chisquared))
#print(chisquared/dof)

print("Probability of Chi squared = " + str(1.0 - stats.chi2.cdf(chisquared,dof)))

print("variance of pull distribution = " + str(np.std((pulse_ave - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.))/pulse_stdevs)))
print("average of pull distribution = " + str(np.mean((pulse_ave - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.))/pulse_stdevs)))

def Gauss(x,mu,sigma):
    return np.exp(-(x-mu)**2./2./sigma**2.)/(np.sqrt(2.*np.pi*sigma**2))

n,bins,patches = plt.hist((pulse_ave - compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.))/pulse_stdevs, bins = 30)
xvals = []
for i in range(0,len(n)):
    i = int(i)
    xvals.append((bins[i] + bins[i+1])/2.)
sigmas = []
for i in range(0,len(n)):
    if n[i] == 0.:
        sigmas.append(np.sqrt(1./sum(n)))
    else:
        sigmas.append(np.sqrt(n[i]/sum(n)))
nsum = sum(n)
n = n/nsum
fit,cov = curve_fit(Gauss,xvals,n,p0 = [0.,1.],sigma = sigmas)

print(fit,cov)
print((1.-fit[1])/cov[1,1])
plt.plot(xvals,Gauss(xvals,fit[0],fit[1])*nsum*(bins[1] - bins[2])*-1)
plt.title("Pull Distribution with Fit, 40 bins",fontsize = 20)
plt.xlabel("((Data)-(Fit))/$\sigma$",fontsize = 13)
plt.ylabel("Counts",fontsize = 16)
plt.tick_params(axis = 'y',labelsize = 10)
plt.tick_params(axis = 'x',labelsize = 10)
plt.savefig("Pull Distribution with Fit.png")
plt.show()

"""
chicdf = []
xvals = []
for i in range(0,2000):
    chicdf.append(stats.chi2.cdf(2396 + i,dof))
    xvals.append(2396 + i)

plt.plot(xvals,chicdf)
plt.show()

chipdf = []
xvals = []
for i in range(2000,5000):
    chipdf.append(stats.chi2.pdf(i,dof))
    xvals.append(i)
    
plt.plot(xvals,chipdf)
plt.show()
"""
fit = compute_pulse(m.values['x1'],m.values['x2'],m.values['x3'],0.)

fit_CDF = np.empty(0)
data_CDF = np.empty(0)
ind_data_CDF = np.empty(0)
for i in range(0,len(pulse_ave)):
    fit_CDF = np.append(fit_CDF,sum(fit[0:i]))
    data_CDF = np.append(data_CDF,sum(pulse_ave[0:i]))
    ind_data_CDF = np.append(ind_data_CDF,sum(pulse_ave2[0:i]))
fit_CDF = fit_CDF / fit_CDF[-1]
data_CDF = data_CDF / data_CDF[-1]
ind_data_CDF = ind_data_CDF / ind_data_CDF[-1]

plt.plot(t_L,fit_CDF,label = 'fit cdf')
plt.plot(t_L,data_CDF,label = 'data cdf')
plt.title("CDFs for the KS Test",fontsize = 20)
plt.xlabel("Time[s]",fontsize = 18)
plt.ylabel("Cumulative Probability",fontsize = 18)
plt.tick_params(axis = 'y',labelsize = 10)
plt.tick_params(axis = 'x',labelsize = 10)
plt.legend()
plt.savefig("CDFs for KS Test.png")
plt.show()

plt.plot(t_L,np.sqrt(len(fit_CDF))*np.abs(fit_CDF - data_CDF))
plt.title("Difference in Cumulative Probability",fontsize = 20)
plt.xlabel("Time[s]",fontsize = 18)
plt.ylabel("$\sqrt{N}\|F_0(x)-F_1(x)\|$",fontsize = 18)
plt.tick_params(axis = 'y',labelsize = 10)
plt.tick_params(axis = 'x',labelsize = 10)
plt.savefig("Difference in CDFs.png")
plt.show()

ks_stat = np.sqrt(len(fit_CDF))*np.max(np.abs(fit_CDF - data_CDF))
ks_stat_ind = np.sqrt(len(fit_CDF))*np.max(np.abs(fit_CDF - ind_data_CDF))
print("ks statistic = ", ks_stat)
print("ks statistic independent = ", ks_stat_ind)

ks_test = special.kolmogorov(ks_stat)
print("ks Probability = ", ks_test)
