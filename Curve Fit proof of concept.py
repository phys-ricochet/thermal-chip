import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions
from numpy.linalg import inv #import inversion function
from numpy import linalg as LA #Import Linear Algebra
from scipy.integrate import quad, odeint # Import integrator and Nonlinear Solver
from scipy.optimize import root, minimize # Equilibrium point finder
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object
import yaml
from scipy.optimize import curve_fit

stream = open("mock data generator.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')
        
### To make the program run faster we make some values global variables 
Rn =  p['Rn']           # Normal Resistance of TES [Ohms]
Tc = p['Tc']            # Tc of the TES [Kelvin]
alpha0 = p['alpha0']    # Temperature sensitivity [unitless]
beta0 = p['beta0']      # Current Sensitivity [unitless]

p['VGC'] = p['AGC']*p['LGC']                                            #Volume that the parameter file Conductances and Capacities are at
p['Lm'] = p['Am']*p['conductivity_m']*p['Tb']/(p['G_mwb2']*p['TGC'])    #Length of the meander, calculated from the user provided conductance in the parameter file

### Heat Capacities
#This is a function to calculate the heat capacities for us
def capacity(T,c1,c3,c5,A,L):
    C = c1*(T/p['TGC'])*A*L/p['VGC']+c3*(T/p['TGC'])**3*A*L/p['VGC']+c5*(T/p['TGC'])**5*A*L/p['VGC'] #scale for volume and temperature
    return C
#This is a function that is used to compute the Debye function for the heat capacity of the Silicone Oil (Polydimethylsiloxane i.e. PDMS)
def debye_integrand(x):
    D = x**3/(np.exp(x) - 1.0)
    return D

### Thermal Conductances
# Function to calculate non electron phonon coupling conductances
def conductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*(p['LGC']*A/(p['AGC']*L))#scale for area, length, and temperature
    return G
# Function to calculate electron phonon coupling conductances
def ephconductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*A*L/p['VGC'] #scale for volume and temperature
    return G

def calc_params():
    cg          = 6.947*3.0*(p['Tc']/69.0)**3.0*quad(debye_integrand,0,69.0/p['Tc'])[0]*0.8*p['Ag']*p['Lg']*1.0e+6/74.1539 #Heat Capacity of "Glue", currently PDMS [J/K]
    ca          = 1.*capacity(p['Tc'],p['ca_1_05'],p['ca_3_05'],0.0,p['Aa'],p['La'])                                               #Heat Capacity of Absorber [J/K]
    cau1        = capacity(p['Tc'],p['cau1_1_05'],p['cau1_3_05'],0.0,p['Aau1'],p['Lau1'])                                       #Heat Capacity of Gold Pad 1 [J/K]
    cwb1        = capacity(p['Tc'],p['cwb1_1_05'],p['cwb1_3_05'],0.0,p['Awb1'],p['Lwb1'])                                       #Heat Capacity of Wirebond 1 [J/K]
    cau2        = capacity(p['Tc'],p['cau2_1_05'],p['cau2_3_05'],0.0,p['Aau2'],p['Lau2'])                                       #Heat Capacity of Gold Pad 2 [J/K]
    csi         = capacity(p['Tc'],p['csi_1_05'],p['csi_3_05'],0.0,p['Asi'],p['Lsi'])                                           #Heat Capacity of Silicon Wafer [J/K]
    cte         = capacity(p['Tc'],p['cte_1_05'],p['cte_3_05'],0.0,p['Ate'],p['Lte'])                                           #Heat Capacity of TES Electron System [J/K]
    cm          = capacity(p['Tb'],p['cm_1_05'],p['cm_3_05'],0.0,p['Am'],p['Lm'])+cau2                                     #Heat Capacity of Meander [J/K]
    cwb2        = capacity(p['Tb'],p['cwb2_1_05'],p['cwb2_3_05'],0.0,p['Awb2'],p['Lwb2'])                                       #Heat Capacity of Wirebond 2 [J/K]
    ctot        = ca + cau1 +  cg  +  cwb1  +  cau2  +  csi  +  cte  +  cm  +  cwb2      #Heat Capacity of Total [J/K]
    G_ab        = conductance(p['Tc'],p['G_ab_05'],p['nab'],p['Ab'],p['Lb'])                  #Thermal Conductance of Absorber to Bath [W/K]
    G_ag        = conductance(p['Tc'],p['G_ag_05'],p['nag'],p['Ag'],p['Lg'])                  #Thermal Conductance of Absorber to Glue [W/K]
    G_gsi       = 1*conductance(p['Tc'],p['G_gsi_05'],p['ngsi'],p['Ag'],p['Lg'])               #Thermal Conductance of Glue to Silicon Wafer [W/K]
    G_au1wb1    = conductance(p['Tc'],p['G_au1wb1_05'],p['nau1wb1'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Gold Pad 1 to Wirebond 1 [W/K]
    G_wb1au2    = 1*conductance(p['Tc'],p['G_wb1au2_05'],p['nwb1au2'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Wirebond 1 to Gold Pad 2 [W/K]
    G_au2te     = 1*conductance(p['Tc'],p['G_au2te_05'],p['nau2te'],p['Aau2'],p['Lau2'])     #Thermal Conductance of Gold Pad 2 to TES [W/K]
    G_tem       = 1*conductance(p['Tc'],p['G_tem_05'],p['ntem'],p['Ate'],p['Lte'])             #Thermal Conductance of TES to Meander [W/K]
    G_wb2b      = conductance(p['Tb'],p['G_wb2b_05'],p['nwb2b'],p['Awb2'],p['Lwb2'])        #Thermal Conductance of Wirebond 2 to Bath [W/K]
    G_aau1      = ephconductance(p['Tc'],p['G_aau1_05'],p['naau1'],p['Aau1'],p['Lau1'])     #Thermal Conductance of Absorber to Gold Pad 2 [W/K]
    G_siau2     = 1*ephconductance(p['Tc'],p['G_siau2_05'],p['nsiau2'],p['Aau2'],p['Lau2'])  #Thermal Conductance of Silicon Wafer to Gold Pad 2 [W/K]
    G_site      = 1*ephconductance(p['Tc'],p['G_site_05'],p['nsite'],p['Ate'],p['Lte'])       #Thermal Conductance of Silicon Wafer to TES [W/K]
    G_sim       = 1*p['G_sim_05']*(p['Tc']/p['TGC'])**(p['nsim'] - 1.)*(p['Am']*p['Lm']+p['Aau2']*p['Lau2'])/p['VGC']            #Thermal Conductance of Silicon Wafer to Meander [W/K]
    kab         = G_ab/(p['nab']*p['Tc']**(p['nab']-1.))                       #Conductance Constant Absorber to Bath [W/K^4]
    kaau1       = G_aau1/(p['naau1']*p['Tc']**(p['naau1']-1.))                 #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
    kag         = G_ag/(p['nag']*p['Tc']**(p['nag']-1.))                       #Conductance Constant Absorber to Glue [W/K^3.5]
    kau1wb1     = G_au1wb1/(p['nau1wb1']*p['Tc']**(p['nau1wb1']-1.))           #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
    kgsi        = G_gsi/(p['ngsi']*p['Tc']**(p['ngsi']-1.))                    #Conductance Constant Glue to Silicon [W/K^3.5]
    kwb1au2     = G_wb1au2/(p['nwb1au2']*p['Tc']**(p['nwb1au2']-1.))           #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
    ksiau2      = G_siau2/(p['nsiau2']*p['Tc']**(p['nsiau2']-1.))              #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
    kau2te      = G_au2te/(p['nau2te']*p['Tc']**(p['nau2te']-1.))              #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
    ksite       = G_site/(p['nsite']*p['Tc']**(p['nsite']-1.))                 #Conductance Constant Silicon to TES Phonon System [W/K^4]
    ksim        = G_sim/(p['nsim']*p['Tc']**(p['nsim']-1.))                    #Conductance Constant Silicon to Meander [W/K^4]
    ktem        = G_tem/(p['ntem']*p['Tc']**(p['ntem']-1.))                    #Conductance Constant TES Electron System to MEander [W/K^2]
    kmwb2       = p['G_mwb2']/(p['nmwb2']*p['Tb']**(p['nmwb2']-1.))                 #Conductance Constant Meander to Wirebond 2 [W/K^2]
    kwb2b       = G_wb2b/(p['nwb2b']*p['Tb']**(p['nwb2b']-1.))                 #Conductance Constant Wirecond 2 to Bath [W/K^4]
    R0          = Rn  * p['frac']                       #Equilibrium resistance of TES [Ohms]
    P_0         = p['I0_NL']**2* R0                      #Equilibrium power through TES [J]
    V_0         = p['I0_NL']* R0                         #Equilibrium Voltage [V]
    P0          = kmwb2 *((p['Tc'])**2-(p['Tb'])**2)    #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
    I0_P        = np.sqrt( P0 / R0 )                  #Equilibrium Current calculated from power, used in resistance equation
    vb          = I0_P  * p['Rl']                       #Voltage Bias
    return cg ,  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_ag , G_gsi , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, kab , kaau1 , kag , kau1wb1 , kgsi , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, R0 , P_0 , V_0 , P0 , I0_P , vb


def compute_pulse(x1,x2,x3):
    #Calculate the parameters from the input vector x
    cg ,  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_ag , G_gsi , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, kab , kaau1 , kag , kau1wb1 , kgsi , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, R0 , P_0 , V_0 , P0 , I0_P , vb = calc_params()
    p['E'] = x1
    p['G_mwb2'] = x2
    G_aau1 = x3
    
    # Define the matrix for linear solver
    def N():
        cg ,  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_ag , G_gsi , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, kab , kaau1 , kag , kau1wb1 , kgsi , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, R0 , P_0 , V_0 , P0 , I0_P , vb = calc_params()
        
        p['E'] = x1
        p['G_mwb2'] = x2
        G_aau1 = x3

        N = np.array([[-1.* R0 *(1.+ beta0 )/ p['L'] , -1.* alpha0 * V_0 /( Tc * p['L'] ),0,0,0,0,0,0,0,0],
             [(2.+ beta0 )* V_0 /( cte ),-( G_au2te + G_site + G_tem - alpha0 * P_0 / Tc )/ cte ,0,0,0,0, G_au2te / cte , G_site / cte , G_tem / cte ,0],
             [0,0,-( G_ab + G_aau1 + G_ag )/ ca , G_aau1 / ca , G_ag / ca ,0,0,0,0,0],
             [0,0, G_aau1 / cau1 ,-( G_aau1 + G_au1wb1 )/ cau1 ,0, G_au1wb1 / cau1 ,0,0,0,0],
             [0,0, G_ag / cg ,0,-( G_ag + G_gsi )/ cg ,0,0, G_gsi / cg ,0,0],
             [0,0,0, G_au1wb1 / cwb1 ,0,-( G_au1wb1 + G_wb1au2 )/ cwb1 , G_wb1au2 / cwb1 ,0,0,0],
             [0, G_au2te / cau2 ,0,0,0, G_wb1au2 / cau2 ,-( G_au2te + G_wb1au2 + G_siau2 )/ cau2 , G_siau2 / cau2 ,0,0],
             [0, G_site / csi ,0,0, G_gsi / csi ,0, G_siau2 / csi ,-( G_gsi + G_siau2 + G_site + G_sim )/ csi , G_sim / csi ,0],
             [0, G_tem / cm ,0,0,0,0,0, G_sim / cm ,-( G_tem + G_sim + p['G_mwb2'] )/ cm , p['G_mwb2'] / cm ],
             [0,0,0,0,0,0,0,0, p['G_mwb2'] / cwb2 ,-( p['G_mwb2'] + G_wb2b )/ cwb2 ]])
        return N
    matrix = N()
    #Solve the linear case
    Eig, P = LA.eig(matrix) # Compute eigenvalues (Eig) and eigenvectors (P) of M
    P_1 = inv(P) # Compute inverse of the eigenvectors
    phi0 = np.array([0,0,p['E']/ca,0,0,0,0,0,0,0])  # Pulse to Absorber
    A = P_1.dot(phi0) #Dot the inverse of the eigenvector with the input pulse to get our coefficients
    tau = 1.0/Eig #get the time constants from the eigenvalues
    LINt = np.linspace(0,p['lintime'],num = int(p['numlinpts']),endpoint=True) #create an array of times to evaluate our solutions at   
    exp_vec = map(lambda x,y: y*np.exp(LINt/x),tau,A) # Form the exponential basis terms
    deltaT_exp = P.dot(exp_vec) # create a vector of the solutions
    dI, dT_Te, dT_a, dT_Au1, dT_g, dT_wb1, dT_Au2, dT_Si, dT_m, dT_wb2 = map(np.array, deltaT_exp)

    return dI

data = np.load('NPZ of Linear Data Mock Data.npz')
I_L = data['current']
t_L = data['time']
del data.f
data.close()

stdevs = np.ones(3277) * 1e-10

def least_squares(x1,x2,x3):
    least_squares = 0.
    dI = compute_pulse(x1,x2,x3)
    for i in range(0,len(I_L)):
        least_squares = least_squares + (I_L[i]-dI[i])**2./(stdevs[i])**2.
    return least_squares

from iminuit import Minuit

m = Minuit(least_squares,x1 = 1.7e-17, limit_x1 = (1.0e-19,1.0e-15),x2 = 9.0e-11,limit_x2 = (1.0e-12,1.0e-9),x3 = 3.0e-8,limit_x3 = (1.0e-9,1.0e-7))
m.migrad()
print(m.values)  # {'x': 2,'y': 3,'z': 4}
print(m.errors)  # {'x': 1,'y': 1,'z': 1}

print(least_squares(m.values['x1'],m.values['x2'],m.values['x3']))