import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
from math import * 
from scipy import integrate
import inspect, os
import scipy as sp
import sys, traceback
from pylab import *
matplotlib.rc('axes.formatter', useoffset=False)

### Import the Linear Data and the Frequency Data

data = np.load('NPZ of Linear Data Electronics Noise Spectrum.npz')
I_L = data['current']
t_L = data['time']
del data.f
data.close()

data = np.load('NPZ of Frequency Data Electronics Noise Spectrum.npz')
noise_total = data['total']
frequencies = data['freqs']
del data.f
data.close()

noise_total = noise_total.astype(complex)

pulse_frequencies = []

for i in range(1,2049):
    pulse_frequencies.append(i*10.)
pulse_frequencies = np.array(pulse_frequencies)

#Plot the noise before we mess with it
plt.semilogx(noise_total)
plt.title("Noise Total Before IFFT")
plt.show()

#The pulse does not have a baseline in the beginning, so this adds the baseline
#by inserting the equilibrium value (I_L[-1]) at the beginning.  This is done
#until there are 4096 points in the trace
for i in range(0,819):
    I_L = np.insert(I_L,0,I_L[-1])
    t_L = np.append(t_L,t_L[-1] + 0.00002441)

I_L = I_L/20.

#Plot what the pulse looks like before the FFT
plt.plot(t_L,I_L)
plt.title("Pulse Before FFT")
plt.show()

#FFT the pulse
pulsefft = np.fft.fft(I_L)

plt.plot(pulsefft.imag)
plt.show()


#Plot the pulse's FFT, the constraints on the plot are becuse the output of the
#FFT is given by FFT[0]-zero frequency term, First half of FFT is positive freq
#second half of FFT is negative freq, from most negative to least
plt.loglog(pulse_frequencies, np.abs(pulsefft[0:len(pulsefft)/2]))
plt.title("FFT of Pulse")
plt.show()

#For proof that we haven't totally screwed up, IFFT the FFT
pulsefftifft = np.fft.ifft(pulsefft)

#Plot the pulse and the IFFT(FFT(Pulse)) on top of each other to check they
#agree
plt.plot(t_L,pulsefftifft,label = 'FFT and IFFT')
plt.plot(t_L,I_L,label = 'raw')
plt.legend()
plt.title("Pulse after FFT and IFFT")
plt.show()

#Define a random number generator function for the random phases
def get_rand():
    return np.random.random()

z = 1j

#Multiply each of the noise terms by a complex exponential, giving the noise a 
#random phase at each bin
for i in range(0,len(noise_total)):
    rand = get_rand()
    noise_total[i] = noise_total[i] * np.exp(z*2*np.pi*rand)

#Because the noise_total array only has the positive frequencies, append the 
#negative frequencies in the proper place, and then insert the zero frequency 
#term

noise_total = np.append(noise_total,np.conj(np.flip(noise_total,0)))
noise_total = np.insert(noise_total,0,0.)
print(noise_total[1000:1005])

plt.plot(noise_total.imag)
plt.show()

#Option to tncrease the noise to show that it is definitely there
#noise_total = noise_total*50.

#Set the zero frequency term
#noise_total[0] = 3.0e-11

#Test array of all ones, to make sure everything is what we expect
#noise_total = np.ones(4097)*1e-11

#Add random phases to the flat noise curve
#for i in range(0,len(noise_total)):
#    rand = get_rand()
#    noise_total[i] = noise_total[i] * np.exp(1j*2*np.pi*rand)

#Set the zero frequency term to zero
#noise_total[0] = 0.0

#IFFT the noise to get a time domain trace we can work with
noise = np.fft.ifft(noise_total).real

hold = []
for i in range(1,40961,10):
    hold.append(noise[i])

noise = np.array(hold)

print(noise.shape)

#Plot this trace.  Bounds on array make sure the times line up as we expect
#plt.plot(t_L[1:4096],noise[1:4096])
plt.plot(t_L[1:4096],noise[1:4096])
plt.title("Sample Noise Trace",fontsize = 40)
plt.ylabel("Amplitude [A/$\sqrt{Hz}$]",fontsize = 30)
plt.xlabel("Time [s]",fontsize = 30)
plt.tick_params(axis = 'y',labelsize = 30)
plt.tick_params(axis = 'x',labelsize = 30)
plt.savefig("Sample Noise Trace.png")
plt.show()

#Plot the pulse with noise added to it
plt.plot(t_L,I_L+noise[0:4096])
plt.plot(t_L,I_L)
plt.title("Noisy Pulse")
plt.xlabel("Time [s]")
plt.ylabel("Current Amplitude [A]")
plt.savefig("Noisey Pulse.png")
plt.show()
