import numpy as np  # Import Numpy
import matplotlib.pyplot as plt # Import plotting functions
from scipy.integrate import odeint # Import Nonlinear Solver
from scipy.optimize import root,broyden1 # Alternative equilibrium point finder
from scipy.optimize import minimize,differential_evolution
np.set_printoptions(threshold=np.nan) #Let us print a complete object if necessary


### Needs to be done:
    # Make values read from external source
    # Figure out why there are so many solutions to the equations
    # Differences between f1 and f2 in vectorfield


### Define the resistance function, based on Tali's Igor Code ###

def resistance(T,I):
    A = 3.52*np.sqrt(kb*cte/(hbar*Rn*Tc)) #[Amps/sqrt[K]]
    R = ((Rn/2.)*(1+np.tanh((T-Tc+((I/A)**2.)**(1./3.))*(alpha0/Tc))))
    return R

R = resistance(0.085,9.998e-5)
print(R)

### Set up the equations to be solved for the equilibrium point ###

def equations(w,p):
    # variables to solve for
    I,v,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2,Tb = w
    # parameters 
    L,Rl,alpha0,Rn,Ccap,kb,hbar,Tc,vb,ca,kab,kaau1,kag,cau1,kau1wb1,cg,kgsi,cwb1,kwb1au2,cau2,kau2te,csi,ksiau2,ksitp,ksim,cte,ktpte,ktem,ctp,cm,kmwb2,cwb2,kwb2b,cb,nab,naau1,nag,nau1wb1,ngsi,nwb1au2,nsiau2,nau2te,nsitp,nsim,ntpte,ntem,nmwb2,nwb2b = p
    ### Array of functions to solve, typed in Python ###
    f1 = [(v-I*resistance(Tte,I))/L,
         (vb-Rl*I-v)/(Rl*Ccap),
         (-kab*(Ta**nab-Tb**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**(nag)-Tg**(nag)))/ca,
         (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1))/cau1,
         (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi))/cg,
         (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2))/cwb1,
         (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te))/cau2,
         (-kgsi*(Tsi**ngsi-Tg**ngsi)-ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim))/csi,
         ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem)))/cte,
         (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte))/ctp,
         (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2))/cm,
         (-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-Tb**nwb2b))/cwb2,
         (-kab*(Tb**nab-Ta**nab)-kwb2b*(Tb**nwb2b-Twb2**nwb2b))/cb]
    ### The function above but with none of the division by the left hand side components ###
    g1 = [v-I*resistance(Tte,I),
         vb-Rl*I-v,
         -kab*(Ta**nab-Tb**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**nag-Tg**nag),
         -kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1),
         -kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi),
         -kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2),
         -ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te),
         -kgsi*(Tsi**ngsi-Tg**ngsi)-ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim),
         (resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem)),
         -ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte),
         -ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2),
         -kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-Tb**nwb2b),
         -kab*(Tb**nab-Ta**nab)-kwb2b*(Tb**nwb2b-Twb2**nwb2b)]
    ### Test Function from Matlab with coefficients ###
    ### As of June 14 2017 this is the function that is converging where we expect it to.  More testing needs to be done to see if this works for real though ###
    f2 = [(v - I*resistance(Tte,I))/L,
         (vb - Rl*I - v)/(Rl*Ccap),
         (-kab *(Ta**(nab) - Tb**(nab)) - kaau1 *(Ta**(naau1) - Tau1**(naau1)) - kag *((Ta)**(nag) - (Tg)**(nag)))/ca,
         (-kaau1 *(Tau1**(naau1) - Ta**(naau1)) - kau1wb1 *(Tau1**(nau1wb1) - Twb1**(nau1wb1)))/cau1,
         (-kag *(pow(Tg,nag) - pow(Ta,nag)) - kgsi *(pow(Tg,ngsi) - pow(Tsi,ngsi)))/cg,
         (-kau1wb1 *(Twb1**(nau1wb1) - Tau1**(nau1wb1)) - kwb1au2 *(Twb1**(nwb1au2) - Tau2**(nwb1au2)))/cwb1,
         (-ksiau2 *(Tau2**(nsiau2) - Tsi**(nsiau2)) - kwb1au2 *(Tau2**(nwb1au2) - Twb1**(nwb1au2)) - kau2te *(Tau2**(nau2te) - Tte**(nau2te)))/cau2,
         (-kgsi *(Tsi**(ngsi) - Tg**(ngsi)) - ksiau2 *(Tsi**(nsiau2) - Tau2**(nsiau2)) - ksitp *(Tsi**(nsitp) - Ttp**(nsitp)) - ksim *(Tsi**(nsim) - Tm**(nsim)))/csi,
         (I**2*resistance(Tte,I) - kau2te *(Tte**(nau2te) - Tau2**(nau2te)) - ktpte *(Tte**(ntpte) - Ttp**(ntpte)) - ktem *(Tte**(ntem) - Tm**(ntem)))/cte,
         (-ksitp *(Ttp**(nsitp) - Tsi**(nsitp)) - ktpte *(Ttp**(ntpte) - Tte**(ntpte)))/ctp,
         (-ktem *(Tm**(ntem) - Tte**(ntem)) - ksim *(Tm**(nsim) - Tsi**(nsim)) - kmwb2 *(Tm**(nmwb2) - Twb2**(nmwb2)))/cm,
         (-kmwb2 *(Twb2**(nmwb2) - Tm**(nmwb2)) - kwb2b *(Twb2**(nwb2b) - Tb**(nwb2b)))/cwb2,
         (-kab *(Tb**(nab) - Ta**(nab)) - kwb2b *(Tb**(nwb2b) - Twb2**(nwb2b)))/cb]
    ### Test Function I used in Matlab, without coefficients ###
    g2 = [v - I*resistance(Tte,I),
         vb - Rl*I - v,
         -kab *(Ta**(nab) - Tb**(nab)) - kaau1* (Ta**(naau1) - Tau1**(naau1)) - kag *(Ta**(nag) - Tg**(nag)),
         -kaau1* (Tau1**(naau1) - Ta**(naau1)) - kau1wb1 *(Tau1**(nau1wb1) - Twb1**(nau1wb1)),
         -kag *(Tg**(nag) - Ta**(nag)) - kgsi* (Tg**(ngsi) - Tsi**(ngsi)),
         -kau1wb1* (Twb1**(nau1wb1) - Tau1**(nau1wb1)) - kwb1au2* (Twb1**(nwb1au2) - Tau2**(nwb1au2)),
         -ksiau2 *(Tau2**(nsiau2) - Tsi**(nsiau2)) - kwb1au2 *(Tau2**(nwb1au2) - Twb1**(nwb1au2)) - kau2te *(Tau2**(nau2te) - Tte**(nau2te)),
         -kgsi *(Tsi**(ngsi) - Tg**(ngsi)) - ksiau2 *(Tsi**(nsiau2) - Tau2**(nsiau2)) - ksitp *(Tsi**(nsitp) - Ttp**(nsitp)) - ksim *(Tsi**(nsim) - Tm**(nsim)),
         I**2*resistance(Tte,I) - kau2te* (Tte**(nau2te) - Tau2**(nau2te)) - ktpte *(Tte**(ntpte) - Ttp**(ntpte)) - ktem *(Tte**(ntem) - Tm**(ntem)),
         -ksitp* (Ttp**(nsitp) - Tsi**(nsitp)) - ktpte *(Ttp**(ntpte) - Tte**(ntpte)),
         -ktem *(Tm**(ntem) - Tte**(ntem)) - ksim *(Tm**(nsim) - Tsi**(nsim)) - kmwb2 *(Tm**(nmwb2) - Twb2**(nmwb2)),
         -kmwb2 *(Twb2**(nmwb2) - Tm**(nmwb2)) - kwb2b* (Twb2**(nwb2b) - Tb**(nwb2b)),
         -kab *(Tb**(nab) - Ta**(nab)) - kwb2b *(Tb**(nwb2b) - Twb2**(nwb2b))]
    return f2

### Set up the Nonliear ODEs ###
def vectorfield(w,t,p):
    # Variables
    I,v,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2,Tb = w 
    # Parameters
    L,Rl,alpha0,Rn,Ccap,kb,hbar,Tc,vb,ca,kab,kaau1,kag,cau1,kau1wb1,cg,kgsi,cwb1,kwb1au2,cau2,ksiau2,kau2te,csi,ksitp,ksim,cte,ktpte,ktem,ctp,cm,kmwb2,cwb2,kwb2b,cb,nab,naau1,nag,nau1wb1,ngsi,nwb1au2,nsiau2,nau2te,nsitp,nsim,ntpte,ntem,nmwb2,nwb2b = p
    ### Array of the functions to solve, typed in Python ###
    f1 = [(v-I*resistance(Tte,I))/L,
         (vb-Rl*I-v)/(Rl*Ccap),
         (-kab*(Ta**nab-Tb**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**nag-Tg**nag))/ca,
         (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1))/cau1,
         (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi))/cg,
         (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2))/cwb1,
         (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te))/cau2,
         (-kgsi*(Tsi**ngsi-Tg**ngsi)-ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim))/csi,
         ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem)))/cte,
         (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte))/ctp,
         (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2))/cm,
         (-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-Tb**nwb2b))/cwb2,
         (-kab*(Tb**nab-Ta**nab)-kwb2b*(Tb**nwb2b-Twb2**nwb2b))/cb]
    ### Test Function from Matlab with coefficients ###
    ### This is the one that is giving the converging answers as of June 14 2017 ###
    f2 = [(v - I*resistance(Tte,I))/L,
         (vb - Rl*I - v)/Rl*Ccap,
         (-kab *(Ta**(nab) - Tb**(nab)) - kaau1 *(Ta**(naau1) - Tau1**(naau1)) - kag *(Ta**(nag) - Tg**(nag)))/ca,
         (-kaau1* (Tau1**(naau1) - Ta**(naau1)) - kau1wb1 *(Tau1**(nau1wb1) - Twb1**(nau1wb1)))/cau1,
         (-kag *(Tg**(nag) - Ta**(nag)) - kgsi *(Tg**(ngsi) - Tsi**(ngsi)))/cg,
         (-kau1wb1* (Twb1**(nau1wb1) - Tau1**(nau1wb1)) - kwb1au2 *(Twb1**(nwb1au2) - Tau2**(nwb1au2)))/cwb1,
         (-ksiau2 *(Tau2**(nsiau2) - Tsi**(nsiau2)) - kwb1au2 *(Tau2**(nwb1au2) - Twb1**(nwb1au2)) - kau2te *(Tau2**(nau2te) - Tte**(nau2te)))/cau2,
         (-kgsi *(Tsi**(ngsi) - Tg**(ngsi)) - ksiau2 *(Tsi**(nsiau2) - Tau2**(nsiau2)) - ksitp *(Tsi**(nsitp) - Ttp**(nsitp)) - ksim *(Tsi**(nsim) - Tm**(nsim)))/csi,
         (I**2*resistance(Tte,I) - kau2te *(Tte**(nau2te) - Tau2**(nau2te)) - ktpte *(Tte**(ntpte) - Ttp**(ntpte)) - ktem *(Tte**(ntem) - Tm**(ntem)))/cte,
         (-ksitp *(Ttp**(nsitp) - Tsi**(nsitp)) - ktpte *(Ttp**(ntpte) - Tte**(ntpte)))/ctp,
         (-ktem *(Tm**(ntem) - Tte**(ntem)) - ksim *(Tm**(nsim) - Tsi**(nsim)) - kmwb2 *(Tm**(nmwb2) - Twb2**(nmwb2)))/cm,
         (-kmwb2 *(Twb2**(nmwb2) - Tm**(nmwb2)) - kwb2b* (Twb2**(nwb2b) - Tb**(nwb2b)))/cwb2,
         (-kab *(Tb**(nab) - Ta**(nab)) - kwb2b* (Tb**(nwb2b) - Twb2**(nwb2b)))/cb]
    return f2

### Externally Define the Parameters ###
### The ultimate goal will to be to read these from a text file ###

### Physical Constants ###
kb=1.38e-23             #Boltzman Constant [J/K]
hbar=1.05e-34           #Plank's Constant over 2 pi [J*s]   

### Electrical Components ###
L=1e-7                  #Inductor in Thevenin Equivalent [Henries]
Rl=5e3                  #Load Resistor [Ohms]
alpha0=100.             #Temperature Sensetivity [Unitless]
Rn=1.                   #normal resistance of the TES [Ohms]
Ccap=1e6                #Capacitor in Thevinin equivalent Circuit [Farads]
Tc=0.08                 #Temperature Half way up the transition [K], NOT AT BIAS.  Number is a guess from Pyle's Thesis
vb=1.                   #Bias Voltage [V]

### Heat Capacities ###
ca=6.44e-10             #Heat Capacity of Absorber [J/K]
cau1=4.47e-11           #Heat Capacity of Gold Pad 1 [J/K]
cg=6.87e-11             #Heat Capacity of Glue [J/K]
cwb1=2.81e-10           #Heat Capacity of Wirebond 1 [J/K]
cau2=1.12e-13           #Heat Capacity of Gold Pad 2 [J/K]
csi=1.93e-12            #Heat Capacity of Silicon [J/K]
cte=3.41e-10            #Heat Capacity of TES Electron System [J/K]
ctp=3.12e-10            #Heat Capacity of TES Phonon system [J/K]
cm=1.12e-13             #Heat Capacity of Meander [J/K]
cwb2=2.81e-10           #Heat Capacity of Wirebond 2 [J/K]
cb=100.                 #Heat Capacity of Bath [J/K]

### Conductance Constants ###
kab=0                  #Conductance Constant Absorber to Bath [W/K^4]
kaau1=7.8e-5            #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
kag=1.4e-5              #Conductance Constant Absorber to Glue [W/K^3.5]
kau1wb1=6.98e-4          #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
kgsi=1.4e-5             #Conductance Constant Glue to Silicon [W/K^3.5]
kwb1au2=6.98e-4          #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
ksiau2=9.8e-6          #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
kau2te=5.5e-5           #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
ksitp=1.2e-5           #Conductance Constant Silicon to TES Phonon System [W/K^4]
ksim=9.8e-6            #Conductance Constant Silicon to Meander [W/K^4]
ktpte=1.7e-1            #Conductance Constant TES Phonon System to TES Electron System [W/K^6]
ktem=5.5e-5             #Conductance Constant TES Electron System to MEander [W/K^2]
kmwb2=1.5e-6            #Conductance Constant Meander to Wirebond 2 [W/K^2]
kwb2b=1.8e-2            #Conductance Constant Wirecond 2 to Bath [W/K^4]

### Exponents for Power ###

nab=4                  #Exponent Absorber to Bath [Unitless]
naau1=2                #Exponent Absorber to Gold Pad 1 [Unitless]
nag=3.5                  #Exponent Absorber to Glue [Unitless] 
nau1wb1=2              #Exponent Gold Pad 1 o Wirebond 1 [Unitless]
ngsi=3.5                 #Exponent Glue to Silicon Chip [Unitless]
nwb1au2=2              #Exponent Wirebond 1 to Gold Pad 2 [Unitless]
nsiau2=4               #Exponent Silicon Chip to Gold Pad 2 [Unitless]
nau2te=2               #Exponent Gold Pad 2 to TES Electron System [Unitless]
nsitp=4                #Exponent Silicon Chip to TES Phonon System [Unitless]
nsim=4                 #Exponent Silicon chip to Meander [Unitless]
ntpte=6                #Exponent TES Phonon System to TES Electron System [Unitless]
ntem=2                 #Exponent TES Electron System to Meander [Unitless]
nmwb2=2                #Exponent Meander to Wirebond 2 [Unitless]
nwb2b=4                #Exponent Wirecond 2 to Bath [Unitless]


### Solver Parameters ###
abserr = 1.0e-8         #Absolute Error threshold in Solver
relerr = 1.0e-6         #Relative Error Threshold in solver
stoptime = 1.           #How long do you want the solution to be for [s]
numpoints = 1000        #How many time points do you want in your solution
t = [stoptime * float(i) / (numpoints - 1) for i in range(numpoints)] #Make an array of those time points

### Create Input Arrays ###
### Parameters ###
p = [L,Rl,alpha0,Rn,Ccap,kb,hbar,Tc,vb,ca,kab,kaau1,kag,cau1,kau1wb1,cg,kgsi,cwb1,kwb1au2,cau2,kau2te,csi,ksiau2,ksitp,ksim,cte,ktpte,ktem,ctp,cm,kmwb2,cwb2,kwb2b,cb,nab,naau1,nag,nau1wb1,ngsi,nwb1au2,nsiau2,nau2te,nsitp,nsim,ntpte,ntem,nmwb2,nwb2b]
# Initial Guesses   

"""
#Test for many initial conditions
x0 = np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])
x1 = x0*10.
x2 = x1*2.
x3 = x0*0.
test1=[]
error1=[]
for i in range(1,100):
    x0 = np.array([float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i)])
    x0 = 0.1*x0
    initials0 = root(equations,x0,args = (p),jac = False,method='lm')
    #if initials0.x[4]>(2.*x0[4]):
    #    initials0.x[4]=x0[4]
    print (x0[1],initials0.x[4])
    test1.append(initials0.x[4])  
    if initials0.success == False:
        error1.append(x0[4])
 
test2=[]
error2=[]
for i in range(1,100):
    x0 = np.array([float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i),float(i)])
    x0 = 0.1*x0
    initials0 = root(equations,x0,args = (p),jac = False,method='lm')
    if initials0.x[4]>(2.*x0[4]):
        initials0.x[4]=x0[4]
    test2.append(initials0.x[4])
    if initials0.success == False:
        error1.append(x0[4])

print(error1)
print(error2)
test=[]
for i in range(0,len(test1)):
    test.append(test1[i]-test2[i])

plt.plot(test1)  
"""
### Minimization Method ###

def sum_of_squares(w):
    I,v,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2,Tb = w
    # parameters 
    ### Physical Constants ###
    kb=1.38e-23             #Boltzman Constant [J/K]
    hbar=1.05e-34           #Plank's Constant over 2 pi [J*s]   
    
    ### Electrical Components ###
    L=1e-7                  #Inductor in Thevenin Equivalent [Henries]
    Rl=5e3                  #Load Resistor [Ohms]
    alpha0=100.             #Temperature Sensetivity [Unitless]
    Rn=1.                   #normal resistance of the TES [Ohms]
    Ccap=1e6                #Capacitor in Thevinin equivalent Circuit [Farads]
    Tc=0.08                 #Temperature Half way up the transition [K], NOT AT BIAS.  Number is a guess from Pyle's Thesis
    vb=0.5                   #Bias Voltage [V]
    
    ### Heat Capacities ###
    ca=6.44e-10             #Heat Capacity of Absorber [J/K]
    cau1=4.47e-11           #Heat Capacity of Gold Pad 1 [J/K]
    cg=6.87e-11             #Heat Capacity of Glue [J/K]
    cwb1=2.81e-10           #Heat Capacity of Wirebond 1 [J/K]
    cau2=1.12e-13           #Heat Capacity of Gold Pad 2 [J/K]
    csi=1.93e-12            #Heat Capacity of Silicon [J/K]
    cte=3.41e-10            #Heat Capacity of TES Electron System [J/K]
    ctp=3.12e-10            #Heat Capacity of TES Phonon system [J/K]
    cm=1.12e-13             #Heat Capacity of Meander [J/K]
    cwb2=2.81e-10           #Heat Capacity of Wirebond 2 [J/K]
    cb=100.                 #Heat Capacity of Bath [J/K]
    
    ### Conductance Constants ###
    kab=0                  #Conductance Constant Absorber to Bath [W/K^4]
    kaau1=7.8e-5            #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
    kag=1.4e-5              #Conductance Constant Absorber to Glue [W/K^3.5]
    kau1wb1=6.98e-4          #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
    kgsi=1.4e-5             #Conductance Constant Glue to Silicon [W/K^3.5]
    kwb1au2=6.98e-4          #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
    ksiau2=9.8e-6          #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
    kau2te=5.5e-5           #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
    ksitp=1.2e-5           #Conductance Constant Silicon to TES Phonon System [W/K^4]
    ksim=9.8e-6            #Conductance Constant Silicon to Meander [W/K^4]
    ktpte=1.7e-1            #Conductance Constant TES Phonon System to TES Electron System [W/K^6]
    ktem=5.5e-5             #Conductance Constant TES Electron System to MEander [W/K^2]
    kmwb2=1.5e-6            #Conductance Constant Meander to Wirebond 2 [W/K^2]
    kwb2b=1.8e-2            #Conductance Constant Wirecond 2 to Bath [W/K^4]
    
    ### Exponents for Power ###
    
    nab=4                  #Exponent Absorber to Bath [Unitless]
    naau1=2                #Exponent Absorber to Gold Pad 1 [Unitless]
    nag=3.5                  #Exponent Absorber to Glue [Unitless] 
    nau1wb1=2              #Exponent Gold Pad 1 o Wirebond 1 [Unitless]
    ngsi=3.5                 #Exponent Glue to Silicon Chip [Unitless]
    nwb1au2=2              #Exponent Wirebond 1 to Gold Pad 2 [Unitless]
    nsiau2=4               #Exponent Silicon Chip to Gold Pad 2 [Unitless]
    nau2te=2               #Exponent Gold Pad 2 to TES Electron System [Unitless]
    nsitp=4                #Exponent Silicon Chip to TES Phonon System [Unitless]
    nsim=4                 #Exponent Silicon chip to Meander [Unitless]
    ntpte=6                #Exponent TES Phonon System to TES Electron System [Unitless]
    ntem=2                 #Exponent TES Electron System to Meander [Unitless]
    nmwb2=2                #Exponent Meander to Wirebond 2 [Unitless]
    nwb2b=4                #Exponent Wirecond 2 to Bath [Unitless]
    ### Array of functions to solve, typed in Python ###
    f1 = [(v-I*resistance(Tte,I)),
         (vb-Rl*I-v),
         (-kab*(Ta**nab-Tb**nab)-kaau1*(Ta**naau1-Tau1**naau1)-kag*(Ta**(nag)-Tg**(nag))),
         (-kaau1*(Tau1**naau1-Ta**naau1)-kau1wb1*(Tau1**nau1wb1-Twb1**nau1wb1)),
         (-kag*(Tg**nag-Ta**nag)-kgsi*(Tg**ngsi-Tsi**ngsi)),
         (-kau1wb1*(Twb1**nau1wb1-Tau1**nau1wb1)-kwb1au2*(Twb1**nwb1au2-Tau2**nwb1au2)),
         (-ksiau2*(Tau2**nsiau2-Tsi**nsiau2)-kwb1au2*(Tau2**nwb1au2-Twb1**nwb1au2)-kau2te*(Tau2**nau2te-Tte**nau2te)),
         (-kgsi*(Tsi**ngsi-Tg**ngsi)-ksiau2*(Tsi**nsiau2-Tau2**nsiau2)-ksitp*(Tsi**nsitp-Ttp**nsitp)-ksim*(Tsi**nsim-Tm**nsim)),
         ((I**2.*resistance(Tte,I)-kau2te*(Tte**nau2te-Tau2**nau2te)-ktpte*(Tte**ntpte-Ttp**ntpte)-ktem*(Tte**ntem-Tm**ntem))),
         (-ksitp*(Ttp**nsitp-Tsi**nsitp)-ktpte*(Ttp**ntpte-Tte**ntpte)),
         (-ktem*(Tm**ntem-Tte**ntem)-ksim*(Tm**nsim-Tsi**nsim)-kmwb2*(Tm**nmwb2-Twb2**nmwb2)),
         (-kmwb2*(Twb2**nmwb2-Tm**nmwb2)-kwb2b*(Twb2**nwb2b-Tb**nwb2b)),
         (-kab*(Tb**nab-Ta**nab)-kwb2b*(Tb**nwb2b-Twb2**nwb2b))]
    s = 0.
    for i in range(0,len(f1)):
        s = s + f1[i]**2
    return s

i = .02
bnds = [(0.,10*i),(0.,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i),(.07,10*i)]

initials0 = differential_evolution(sum_of_squares, bnds, maxiter = 10000)

print(initials0)

"""    
### Find Initial Conditions ###

x0 = np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])
#Method 2, converges for all guesses (14 June 2017)
initials0 = root(equations,x0,args = (p),jac = False,method='lm')
initials0prime = root(equations,initials0.x,args = (p),jac = False,method='lm')
print(initials0.x)
print(initials0prime.x)


initials1 = root(equations,x1,args = (p),jac = False,method='lm')
initials2 = root(equations,x2,args = (p),jac = False,method='lm')
initials3 = root(equations,x3,args = (p),jac = False,method='lm')

#Print whether the guesses led to a successful determination of the initial conditions, and print those initial conditions
print("0",initials0.success,initials0.x)
print("1",initials1.success,initials1.x)
print("2",initials2.success,initials2.x)
print("3",initials3.success,initials3.x)




#Put the initial conditions into an array
w0 = initials0.x
#Set the pulse height to 2 in the absorber [K]
w0[2]=0.02

### Solve the System of Nonlinear ODEs ###
wsol = odeint(vectorfield, w0, t, args=(p,),
              atol=abserr, rtol=relerr)

### Transpose the solution so we can easily plot it using the map function as in the linear case ###
solution = np.transpose(wsol)

### Separate out the solution into individual arrays that can be plotted ###
I, v, Ta, Tau1, Tg, Twb1, Tau2, Tsi, Tte, Ttp, Tm, Twb2, Tb = map(np.array, solution)

### Plot the results ###

### Make the plots bigger ###
fig = plt.figure(figsize=(18, 18))

#Plot absorber Temperature
plt.subplot(10,1,1)
plt.plot(Ta,label="Absorber Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [mu s]")
plt.legend()
plt.title("Linear Solver")
fig.subplots_adjust(hspace=.5)

#Plot Current
plt.subplot(10,1,2)
plt.plot(I,label="Current") 
plt.ylabel("Current [I]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot Voltage
plt.subplot(10,1,3)
plt.plot(v,label="Voltage") 
plt.ylabel("Voltage [V]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
plt.subplot(10,1,4)
plt.plot(Tau1,label="Gold Pad 1 Temperature") 
plt.plot(Twb1,label="Wirebond 1 Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot Glue Temperature on same graph
plt.subplot(10,1,5)
plt.plot(Tg,label="Glue Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot Silicon chip Temperature
plt.subplot(10,1,6)
plt.plot(Tsi,label="Silicon Chip Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot TES Electron and Phonon System Temperatures on same graph
plt.subplot(10,1,7)
plt.plot(Tte,label="TES Electron System Temperature")
plt.plot(Ttp,label="TES Phonon System Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot Gold Pad 2 Temperature
plt.subplot(10,1,8)
plt.plot(Tau2,label="Gold Pad 2 Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot Meander and Wirebond 2 Temperatures on same graph
plt.subplot(10,1,9)
plt.plot(Tm,label="Meander Temperature")
plt.plot(Twb2,label="Wirebond 2 Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [mu s]")
plt.legend()

#Plot Bath Temperature
plt.subplot(10,1,10)
plt.plot(Tb,label="Bath Temperature")
plt.ylabel("Temperature [K]")
plt.legend()
plt.xlabel("Time [mu s]")

plt.show() #Display the figure
"""