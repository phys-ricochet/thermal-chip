import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions
from numpy.linalg import inv #import inversion function
from numpy import linalg as LA #Import Linear Algebra
from scipy.integrate import quad, odeint # Import integrator and Nonlinear Solver
from scipy.optimize import root # Equilibrium point finder
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object
import yaml
from scipy.optimize import curve_fit
import sys

### This script is written to be run as either a single case, taken from the 
### input file, or run in parallel.  If you would like to run one case, change
### the input file to the desired file, and run the function main, with the 
### desired arguments. If you would like to then plot, run the function 
### make_plots with the desired options.  If you would like to run this in
### parallel, comment in the lines below the yaml input code, as these change 
### the parameters in the code based on the arguments given by GNU Parallel.
### To use GNU parallel, use the following script:
### nohup parallel -j4  python main.py ::: args1 ::: args2 ::: args3 ::: args4
### and this will run all combinations of all arguments.  Last, before you run,
### make sure the for_process function is uncommented, and main is commented
### out.
###     -Doug Pinckney, 4 August, 2017


### First we import the parameter file ###

stream = open("input with sib.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')

### Next, if we are running in parallel, take the arguments of the terminal window and use them in this code ### 
#p['G_mwb2'] = float(sys.argv[1])  # Change meander-wirebond 2 condctance
#p['Aau1'] = float(sys.argv[1])    # Change the area of gold pad 1
#p['Lte'] = float(sys.argv[1])     # Change the length of the TES
#p['Ate'] = float(sys.argv[2])     # Change the area of the TES


        
### Now we derive the parameters we need from those given in the parameter file ###
p['VGC'] = p['AGC']*p['LGC']                                            #Volume that the parameter file Conductances and Capacities are at
p['Lm'] = p['Am']*p['conductivity_m']*p['Tb']/(p['G_mwb2']*p['TGC'])    #Length of the meander, calculated from the user provided conductance in the parameter file
#print(p['Lm']*1e3,p['Lm']*10.0e-6*1.0e3/0.0019) #compute the length that the meander would take up on the chip.  Assumes that you can get 1.9mm every 10 micrometers (becuase 2mm wide chip and 5 micrometer width)

### To make the program run faster we make some values global variables 
Rn =  p['Rn']           # Normal Resistance of TES [Ohms]
Tc = p['Tc']            # Tc of the TES [Kelvin]
alpha0 = p['alpha0']    # Temperature sensitivity [unitless]
beta0 = p['beta0']      # Current Sensitivity [unitless]

### Heat Capacities
#This is a function to calculate the heat capacities for us
def capacity(T,c1,c3,c5,A,L):
    C = c1*(T/p['TGC'])*A*L/p['VGC']+c3*(T/p['TGC'])**3*A*L/p['VGC']+c5*(T/p['TGC'])**5*A*L/p['VGC'] #scale for volume and temperature
    return C
#This is a function that is used to compute the Debye function for the heat capacity of the Silicone Oil (Polydimethylsiloxane i.e. PDMS)
def debye_integrand(x):
    D = x**3/(np.exp(x) - 1.0)
    return D

### Thermal Conductances
# Function to calculate non electron phonon coupling conductances
def conductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*(p['LGC']*A/(p['AGC']*L))#scale for area, length, and temperature
    return G
# Function to calculate electron phonon coupling conductances
def ephconductance(T,g,n,A,L):
    G = g*(T/p['TGC'])**(n - 1.)*A*L/p['VGC'] #scale for volume and temperature
    return G

def calc_params():
    ca          = capacity(p['Tc'],p['ca_1_05'],p['ca_3_05'],0.0,p['Aa'],p['La'])                                               #Heat Capacity of Absorber [J/K]
    cau1        = capacity(p['Tc'],p['cau1_1_05'],p['cau1_3_05'],0.0,p['Aau1'],p['Lau1'])                                       #Heat Capacity of Gold Pad 1 [J/K]
    cwb1        = capacity(p['Tc'],p['cwb1_1_05'],p['cwb1_3_05'],0.0,p['Awb1'],p['Lwb1'])                                       #Heat Capacity of Wirebond 1 [J/K]
    cau2        = capacity(p['Tc'],p['cau2_1_05'],p['cau2_3_05'],0.0,p['Aau2'],p['Lau2'])                                       #Heat Capacity of Gold Pad 2 [J/K]
    csi         = capacity(p['Tb'],p['csi_1_05'],p['csi_3_05'],0.0,p['Asi'],p['Lsi'])                                           #Heat Capacity of Silicon Wafer [J/K]
    cte         = capacity(p['Tc'],p['cte_1_05'],p['cte_3_05'],0.0,p['Ate'],p['Lte'])                                           #Heat Capacity of TES Electron System [J/K]
    cm          = capacity(p['Tb'],p['cm_1_05'],p['cm_3_05'],0.0,p['Am'],p['Lm'])+cau2                                     #Heat Capacity of Meander [J/K]
    cwb2        = capacity(p['Tb'],p['cwb2_1_05'],p['cwb2_3_05'],0.0,p['Awb2'],p['Lwb2'])                                       #Heat Capacity of Wirebond 2 [J/K]
    ctot        = ca + cau1  +  cwb1  +  cau2  +  csi  +  cte  +  cm  +  cwb2      #Heat Capacity of Total [J/K]
    G_ab        = conductance(p['Tc'],p['G_ab_05'],p['nab'],p['Ab'],p['Lb'])                  #Thermal Conductance of Absorber to Bath [W/K]
    G_au1wb1    = conductance(p['Tc'],p['G_au1wb1_05'],p['nau1wb1'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Gold Pad 1 to Wirebond 1 [W/K]
    G_wb1au2    = conductance(p['Tc'],p['G_wb1au2_05'],p['nwb1au2'],p['Awb1'],p['Lwb1'])  #Thermal Conductance of Wirebond 1 to Gold Pad 2 [W/K]
    G_au2te     = conductance(p['Tc'],p['G_au2te_05'],p['nau2te'],p['Aau2'],p['Lau2'])     #Thermal Conductance of Gold Pad 2 to TES [W/K]
    G_tem       = conductance(p['Tc'],p['G_tem_05'],p['ntem'],p['Ate'],p['Lte'])             #Thermal Conductance of TES to Meander [W/K]
    G_wb2b      = conductance(p['Tb'],p['G_wb2b_05'],p['nwb2b'],p['Awb2'],p['Lwb2'])        #Thermal Conductance of Wirebond 2 to Bath [W/K]
    G_sib       = 1*conductance(p['Tb'],p['G_sib_05'],p['nsib'],p['Asib'],p['Lsib'])
    G_aau1      = ephconductance(p['Tc'],p['G_aau1_05'],p['naau1'],p['Aau1'],p['Lau1'])     #Thermal Conductance of Absorber to Gold Pad 2 [W/K]
    G_siau2     = ephconductance(p['Tc'],p['G_siau2_05'],p['nsiau2'],p['Aau2'],p['Lau2'])  #Thermal Conductance of Silicon Wafer to Gold Pad 2 [W/K]
    G_site      = ephconductance(p['Tc'],p['G_site_05'],p['nsite'],p['Ate'],p['Lte'])       #Thermal Conductance of Silicon Wafer to TES [W/K]
    G_sim       = p['G_sim_05']*(p['Tb']/p['TGC'])**(p['nsim'] - 1.)*(p['Am']*p['Lm']+p['Aau2']*p['Lau2'])/p['VGC']            #Thermal Conductance of Silicon Wafer to Meander [W/K]
    kab         = G_ab/(p['nab']*p['Tc']**(p['nab']-1.))                       #Conductance Constant Absorber to Bath [W/K^4]
    kaau1       = G_aau1/(p['naau1']*p['Tc']**(p['naau1']-1.))                 #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
    kau1wb1     = G_au1wb1/(p['nau1wb1']*p['Tc']**(p['nau1wb1']-1.))           #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
    kwb1au2     = G_wb1au2/(p['nwb1au2']*p['Tc']**(p['nwb1au2']-1.))           #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
    ksiau2      = G_siau2/(p['nsiau2']*p['Tc']**(p['nsiau2']-1.))              #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
    kau2te      = G_au2te/(p['nau2te']*p['Tc']**(p['nau2te']-1.))              #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
    ksite       = G_site/(p['nsite']*p['Tc']**(p['nsite']-1.))                 #Conductance Constant Silicon to TES Phonon System [W/K^4]
    ksim        = G_sim/(p['nsim']*p['Tb']**(p['nsim']-1.))                    #Conductance Constant Silicon to Meander [W/K^4]
    ktem        = G_tem/(p['ntem']*p['Tc']**(p['ntem']-1.))                    #Conductance Constant TES Electron System to MEander [W/K^2]
    kmwb2       = p['G_mwb2']/(p['nmwb2']*p['Tb']**(p['nmwb2']-1.))                 #Conductance Constant Meander to Wirebond 2 [W/K^2]
    kwb2b       = G_wb2b/(p['nwb2b']*p['Tb']**(p['nwb2b']-1.))                 #Conductance Constant Wirecond 2 to Bath [W/K^4]
    ksib        = G_sib/(p['nsib']*p['Tb']**(p['nsib'] - 1.))
    R0          = Rn  * p['frac']                       #Equilibrium resistance of TES [Ohms]
    P_0         = p['I0_NL']**2* R0                      #Equilibrium power through TES [J]
    V_0         = p['I0_NL']* R0                         #Equilibrium Voltage [V]
    P0          = kmwb2 *((p['Tc'])**2-(p['Tb'])**2)    #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
    I0_P        = np.sqrt( P0 / R0 )                  #Equilibrium Current calculated from power, used in resistance equation
    vb          = I0_P  * p['Rl']                       #Voltage Bias
    return  ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, G_sib, kab , kaau1 , kau1wb1 , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, ksib, R0 , P_0 , V_0 , P0 , I0_P , vb

### Actually compute the variables 
ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, G_sib, kab , kaau1 , kau1wb1 , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, ksib, R0 , P_0 , V_0 , P0 , I0_P , vb = calc_params()

# Defines z to be the imaginary unit
z = 1j

# Now we need to define a function for the resistance of the TES in terms of Current and Temperature
def resistance(Ttes,Ites):
    R = Rn/2 *(1 + np.tanh((Ttes - Tc) * alpha0/Tc + (Ites - I0_P)*beta0 / I0_P))
    return R

# Equations for equilibrium point calculation.  They are weighted because it helps the solver, and ax = 0 has the same solutions are x = 0
def equations(w):
    # variables to solve for
    I,Ta,Tau1,Twb1,Tau2,Tsi,Tte,Tm,Twb2 = w
    ### Array of functions to solve ###
    f1 = [1e13*((resistance(Tte,I)- R0 )),
             1e9*(- kab *(Ta**p['nab']-(p['Tb'])**p['nab'])- kaau1 *(Ta**p['naau1']-Tau1**p['naau1']))// ca ,
             1e8*(- kaau1 *(Tau1**p['naau1']-Ta**p['naau1'])- kau1wb1 *(Tau1**p['nau1wb1']-Twb1**p['nau1wb1']))/ cau1 ,
             1e8*(- kau1wb1 *(Twb1**p['nau1wb1']-Tau1**p['nau1wb1'])- kwb1au2 *(Twb1**p['nwb1au2']-Tau2**p['nwb1au2']))/ cwb1 ,
             1e9*(- ksiau2 *(Tau2**p['nsiau2']-Tsi**p['nsiau2'])- kwb1au2 *(Tau2**p['nwb1au2']-Twb1**p['nwb1au2'])- kau2te *(Tau2**p['nau2te']-Tte**p['nau2te']))// cau2 ,
             1e1*(- ksib * (Tsi**p['nsib']-(p['Tb'])**p['nsib']) -  ksiau2 *(Tsi**p['nsiau2']-Tau2**p['nsiau2'])- ksite *(Tsi**p['nsite']-Tte**p['nsite'])- ksim *(Tsi**p['nsim']-Tm**p['nsim']))/ csi ,
             1e8*((I**2.*resistance(Tte,I)- kau2te *(Tte**p['nau2te']-Tau2**p['nau2te'])- ksite *(Tte**p['nsite']-Tsi**p['nsite'])- ktem *(Tte**p['ntem']-Tm**p['ntem'])))/ cte ,
             1e6*(- ktem *(Tm**p['ntem']-Tte**p['ntem'])- ksim *(Tm**p['nsim']-Tsi**p['nsim'])- kmwb2 *(Tm**p['nmwb2']-Twb2**p['nmwb2']))/ cm ,
             1e12*(- kmwb2 *(Twb2**p['nmwb2']-Tm**p['nmwb2'])- kwb2b *(Twb2**p['nwb2b']-(p['Tb'])**p['nwb2b']))/ cwb2 ]
    return f1

# Matrix for linear solver
def N():
    ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, G_sib, kab , kaau1 , kau1wb1 , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, ksib, R0 , P_0 , V_0 , P0 , I0_P , vb = calc_params()
    N = np.array([[-1.* R0 *(1.+ beta0 )/ p['L'] , -1.* alpha0 * V_0 /( Tc * p['L'] ),0,0,0,0,0,0,0],
             [(2.+ beta0 )* V_0 /( cte ),-( G_au2te + G_site + G_tem - alpha0 * P_0 / Tc )/ cte ,0,0,0, G_au2te / cte , G_site / cte , G_tem / cte ,0],
             [0,0,-( G_ab + G_aau1)/ ca , G_aau1 / ca,0,0,0,0,0],
             [0,0, G_aau1 / cau1 ,-( G_aau1 + G_au1wb1 )/ cau1 , G_au1wb1 / cau1 ,0,0,0,0],
             [0,0,0, G_au1wb1 / cwb1 ,-( G_au1wb1 + G_wb1au2 )/ cwb1 , G_wb1au2 / cwb1 ,0,0,0],
             [0, G_au2te / cau2 ,0,0, G_wb1au2 / cau2 ,-( G_au2te + G_wb1au2 + G_siau2 )/ cau2 , G_siau2 / cau2 ,0,0],
             [0, G_site / csi ,0,0,0, G_siau2 / csi ,-( G_siau2 + G_site + G_sim + G_sib)/ csi , G_sim / csi ,0],
             [0, G_tem / cm ,0,0,0,0, G_sim / cm ,-( G_tem + G_sim + p['G_mwb2'] )/ cm , p['G_mwb2'] / cm ],
             [0,0,0,0,0,0,0, p['G_mwb2'] / cwb2 ,-( p['G_mwb2'] + G_wb2b )/ cwb2 ]])
    return N
# Matrix in frequency space
def matrix(w):
    N = np.array([[1.* R0 *(1.+ beta0 )+z*w*p['L'],-1.,1.* alpha0 * V_0 /( Tc ),0,0,0,0,0,0,0],
             [p['Rl'],1.+z*w*p['Rl']*p['Ccap'],0,0,0,0,0,0,0,0],  
             [-(2.+ beta0 )* V_0 ,0,( G_au2te + G_site + G_tem - alpha0 * P_0 / Tc )+z*w* cte ,0,0,0,- G_au2te ,- G_site ,- G_tem ,0],
             [0,0,0,( G_ab + G_aau1 )+z*w* ca ,- G_aau1 ,0,0,0,0,0],
             [0,0,0,- G_aau1 ,( G_aau1 + G_au1wb1 )+z*w* cau1 ,- G_au1wb1 ,0,0,0,0],
             [0,0,0,0,- G_au1wb1 ,( G_au1wb1 + G_wb1au2 )+z*w* cwb1 ,- G_wb1au2 ,0,0,0],
             [0,0,- G_au2te ,0,0,- G_wb1au2 ,( G_au2te + G_wb1au2 + G_siau2 )+z*w* cau2 ,- G_siau2 ,0,0],
             [0,0,- G_site ,0,0,0,- G_siau2 ,( G_siau2 + G_site + G_sim +G_sib)+z*w* csi ,- G_sim ,0],
             [0,0,- G_tem ,0,0,0,0,- G_sim ,( G_tem + G_sim +p['G_mwb2'])+z*w* cm ,-p['G_mwb2']],
             [0,0,0,0,0,0,0,0,-p['G_mwb2'],(p['G_mwb2'] + G_wb2b )+z*w* cwb2 ]]) 
    return N

# Model of phonon noise
def phononnoise(G,T1,T2,n):
    noise = np.sqrt(2.*p['kb']*G*(T1**(2) + T2**(2)))
    return noise

# Invert the noise matrix
def S(w):
    s = inv(matrix(w))
    return s

# Exponential function definition
def exponential(x, N, tau, a):
    val = N*np.exp(-x/tau)+a
    return val


# This is the main function
def main(N,lin_flag,nonlin_flag,freq_flag):
    ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, G_sib, kab , kaau1 , kau1wb1 , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, ksib, R0 , P_0 , V_0 , P0 , I0_P , vb = calc_params()
    #print(ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, G_sib, kab , kaau1 , kau1wb1 , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, ksib, R0 , P_0 , V_0 , P0 , I0_P , vb)
    #print("ca ,  cau1 ,  cwb1 ,  cau2 ,  csi ,  cte ,  cm ,  cwb2 ,  ctot, G_ab , G_au1wb1 , G_wb1au2 , G_au2te , G_tem , G_wb2b , G_aau1 , G_siau2 , G_site , G_sim, G_sib, kab , kaau1 , kau1wb1 , kwb1au2 , ksiau2 , kau2te , ksite , ksim , ktem , kmwb2 , kwb2b, ksib, R0 , P_0 , V_0 , P0 , I0_P , vb")
    #print("Finding equilibrium point...")
    # First we find the equilibrium point
    x0 = [I0_P, Tc , Tc , Tc , Tc , Tc , Tc , Tc ,p['Tb']]    #Initial Guess
    equilibrium = root(equations,x0,method = 'lm')                                              #Use Root with method LM to find the solution
    eq = equilibrium.x  
    #print(eq)
    I0_P = eq[0]
    print(eq[0])
    if nonlin_flag == 1:
        #print("Solving the ODEs...")
        # Nonlinear ODEs
        def vectorfield(w,t):
            ### Variables ###
            I,Ta,Tau1,Twb1,Tau2,Tsi,Tte,Tm,Twb2 = w 
            ### Array of the functions to solve ###
            f1 = [ (I0_P*R0  - I*resistance(Tte,I))/ p['L'] ,
                 (((p['E']/(p['c']*np.sqrt(2*np.pi)))*np.exp(-(t - p['t0'])**2/(2*p['c']**2)))- kab *(Ta**p['nab']-p['Tb']**p['nab'])- kaau1 *(Ta**p['naau1']-Tau1**p['naau1']))/ ca , 
                 (- kaau1 *(Tau1**p['naau1']-Ta**p['naau1'])- kau1wb1 *(Tau1**p['nau1wb1']-Twb1**p['nau1wb1']))/ cau1 ,
                 (- kau1wb1 *(Twb1**p['nau1wb1']-Tau1**p['nau1wb1'])- kwb1au2 *(Twb1**p['nwb1au2']-Tau2**p['nwb1au2']))/ cwb1 ,
                 (- ksiau2 *(Tau2**p['nsiau2']-Tsi**p['nsiau2'])- kwb1au2 *(Tau2**p['nwb1au2']-Twb1**p['nwb1au2'])- kau2te *(Tau2**p['nau2te']-Tte**p['nau2te']))/ cau2 ,
                 (- ksib * (Tsi**p['nsib']-(p['Tb'])**p['nsib']) -  ksiau2 *(Tsi**p['nsiau2']-Tau2**p['nsiau2'])- ksite *(Tsi**p['nsite']-Tte**p['nsite'])- ksim *(Tsi**p['nsim']-Tm**p['nsim']))/ csi ,
                 ((I**2.*resistance(Tte,I)- kau2te *(Tte**p['nau2te']-Tau2**p['nau2te'])- ksite *(Tte**p['nsite']-Tsi**p['nsite'])- ktem *(Tte**p['ntem']-Tm**p['ntem'])))/ cte ,
                 (- ktem *(Tm**p['ntem']-Tte**p['ntem'])- ksim *(Tm**p['nsim']-Tsi**p['nsim'])- kmwb2 *(Tm**p['nmwb2']-Twb2**p['nmwb2']))/ cm ,
                 (- kmwb2 *(Twb2**p['nmwb2']-Tm**p['nmwb2'])- kwb2b *(Twb2**p['nwb2b']-(p['Tb']**p['nwb2b'])))/ cwb2 ] 
            #print(f1)
            #print("-------------")
            return f1
        # Now find a solution to the nonlinear ODEs
        # First make an array of the time points
        ODEt = [p['stoptime'] * float(i) / (p['numpoints'] - 1) for i in range(int(p['numpoints']))] 
        
        # Run the ODE solver
        #odeint(equation function,initial condition,time array,absolute error,relative error)
        hold_value = p['E']
        p['E'] = 0.0
        wsol = odeint(vectorfield, eq, ODEt, full_output = 0, atol=p['abserr'], rtol=p['relerr'], mxstep = int(p['mxstep']))
        print(resistance(wsol[-1][6],wsol[-1][0]))
        count = 1
        while(np.any(np.absolute(wsol[0]-wsol[-1]) > 1.0e-8)):
            if count > 100:
                raise ValueError('Over 100 iterations trying to perfect the equilibrium point')
            print("Number of Nonlinear Solver iterations = ",count)
            wsol = odeint(vectorfield, wsol[-1], ODEt, full_output = 0, atol=p['abserr'], rtol=p['relerr'], mxstep = int(p['mxstep']))
            count = count + 1
        #print("A stable point was found, now computing the pulse...")
        p['E'] = hold_value
        pulse = odeint(vectorfield, wsol[-1], ODEt, full_output = 0, atol=p['abserr'], rtol=p['relerr'], mxstep = int(p['mxstep']), hmax = p['hmax'])
        nonlinearsol = np.transpose(pulse)
    else:
        nonlinearsol = 0
        ODEt = 0
    if lin_flag == 1:
        #print("Solving the linar case...")
        # Now we can also solve the time domain by finding the eigenvalues
        Eig, P = LA.eig(N) # Compute eigenvalues (Eig) and eigenvectors (P) of M
        P_1 = inv(P) # Compute inverse of the eigenvectors
        phi0 = np.array([0,0,p['E']/ca,0,0,0,0,0,0])  # Pulse to Absorber
        A = P_1.dot(phi0) #Dot the inverse of the eigenvector with the input pulse to get our coefficients
        tau = 1.0/Eig #get the time constants from the eigenvalues
        LINt = np.linspace(0,p['lintime'],num = int(p['numlinpts']),endpoint=True) #create an array of times to evaluate our solutions at   
        exp_vec = map(lambda x,y: y*np.exp(LINt/x),tau,A) # Form the exponential basis terms
        deltaT_exp = P.dot(exp_vec) # create a vector of the solutions
    else:
        deltaT_exp = 0
        LINt = 0
    if freq_flag == 1:
        #print("Working in frequency space...")
        # Finally we can do a frequency domain analysis
        # Calculate the noise source terms
        eint = np.sqrt(4.*p['kb']*eq[6]*R0)      #Internal Johnson Noise
        eext = np.sqrt(4.*p['kb']*p['Tb']*p['Rl'])     #external Johnson Noise across shunt resistor at bath temperature
        pau2te = phononnoise( G_au2te,eq[4],eq[6],p['nau2te'])
        ptem = phononnoise( G_tem,eq[6],eq[7],p['ntem'])
        psite = phononnoise( G_site,eq[5],eq[6],p['nsite'])
        paau1 = phononnoise( G_aau1,eq[1],eq[2],p['naau1'])
        pab = phononnoise( G_ab,eq[1],p['Tb'], p['nau2te'])
        pau1wb1 = phononnoise( G_au1wb1,eq[2],eq[3],p['nau1wb1'])
        pwb1au2 = phononnoise( G_wb1au2,eq[3],eq[4],p['nwb1au2'])
        psiau2 = phononnoise( G_siau2,eq[5],eq[4],p['nsiau2'])
        psim = phononnoise( G_sim,eq[5],eq[7],p['nsim'])
        pmwb2 = phononnoise(p['G_mwb2'],eq[7],eq[8],p['nmwb2'])
        pwb2b = phononnoise( G_wb2b,eq[8],p['Tb'],p['nwb2b'])
        psib = phononnoise( G_sib,eq[5],p['Tb'],p['nsib'])
        pulse = 0.
        
        # Assemble the noise sources into the vectors
        x_eint      = np.array([eint,0,   -eint*eq[0],     0,     0,        0,       0,      0,      0,      0])
        x_eext      = np.array([0,   eext,0,               0,     0,        0,       0,      0,      0,      0])
        x_pau2te    = np.array([0,   0,   pau2te,          0,     0,        0,       -pau2te,0,      0,      0])
        x_ptem      = np.array([0,   0,   -ptem,           0,     0,        0,       0,      0,      ptem,   0])
        x_psite     = np.array([0,   0,   psite,           0,     0,        0,       0,      -psite, 0,      0])
        x_paau1     = np.array([0,   0,   0,               -paau1,paau1,    0,       0,      0,      0,      0])
        x_pab       = np.array([0,   0,   0,               -pab,  0,        0,       0,      0,      0,      0])
        x_pulse     = np.array([0,   0,   0,               pulse, 0,        0,       0,      0,      0,      0])
        x_pau1wb1   = np.array([0,   0,   0,               0,     -pau1wb1, pau1wb1, 0,      0,      0,      0])
        x_pwb1au2   = np.array([0,   0,   0,               0,     0,        -pwb1au2,pwb1au2,0,      0,      0])
        x_psiau2    = np.array([0,   0,   0,               0,     0,        0,       psiau2, -psiau2,0,      0])
        x_psim      = np.array([0,   0,   0,               0,     0,        0,       0,      -psim,  psim,   0])
        x_pmwb2     = np.array([0,   0,   0,               0,     0,        0,       0,      0,      -pmwb2, pmwb2])
        x_pwb2b     = np.array([0,   0,   0,               0,     0,        0,       0,      0,      0,      -pwb2b])
        x_psib      = np.array([0,   0,   0,               0,     0,        0,       0,      -psib,  0,      0])
        
        # Define the actual noise terms
        def noise_eint(w):
            y = np.matmul(S(w),x_eint)
            return np.abs(y[0])
        def noise_eext(w):
            y = np.matmul(S(w),x_eext)
            return np.abs(y[0])
        def noise_pau2te(w):
            y = np.matmul(S(w),x_pau2te)
            return np.abs(y[0])
        def noise_ptem(w):
            y = np.matmul(S(w),x_ptem)
            return np.abs(y[0])
        def noise_psite(w):
            y = np.matmul(S(w),x_psite)
            return np.abs(y[0])
        def noise_paau1(w):
            y = np.matmul(S(w),x_paau1)
            return np.abs(y[0])
        def noise_pab(w):
            y = np.matmul(S(w),x_pab)
            return np.abs(y[0])
        def noise_pulse(w):
            y = np.matmul(S(w),x_pulse)
            return np.abs(y[0])
        def noise_pau1wb1(w):
            y = np.matmul(S(w),x_pau1wb1)
            return np.abs(y[0])
        def noise_pwb1au2(w):
            y = np.matmul(S(w),x_pwb1au2)
            return np.abs(y[0])
        def noise_psiau2(w):
            y = np.matmul(S(w),x_psiau2)
            return np.abs(y[0])
        def noise_psim(w):
            y = np.matmul(S(w),x_psim)
            return np.abs(y[0])
        def noise_pmwb2(w):
            y = np.matmul(S(w),x_pmwb2)
            return np.abs(y[0])
        def noise_pwb2b(w):
            y = np.matmul(S(w),x_pwb2b)
            return np.abs(y[0])
        def noise_psib(w):
            y = np.matmul(S(w),x_psib)
            return np.abs(y[0])
        def total_noise(w):
            noise = np.sqrt(noise_eint(w)**2+noise_eext(w)**2+noise_pau2te(w)**2+noise_ptem(w)**2+noise_psite(w)**2+noise_paau1(w)**2+noise_pab(w)**2+noise_pulse(w)**2+noise_pau1wb1(w)**2+noise_pwb1au2(w)**2+noise_psiau2(w)**2+noise_psim(w)**2+noise_pmwb2(w)**2+noise_pwb2b(w)**2+noise_psib(w)**2)
            return noise
        
        # Define the NEPs
        def NEP_eint(w):
            nep = np.abs(noise_eint(w)/S(w)[0,3])
            return nep
        def NEP_eext(w):
            nep = np.abs(noise_eext(w)/S(w)[0,3])
            return nep
        def NEP_pau2te(w):
            nep = np.abs(noise_pau2te(w)/S(w)[0,3])
            return nep
        def NEP_ptem(w):
            nep = np.abs(noise_ptem(w)/S(w)[0,3])
            return nep
        def NEP_psite(w):
            nep = np.abs(noise_psite(w)/S(w)[0,3])
            return nep
        def NEP_paau1(w):
            nep = np.abs(noise_paau1(w)/S(w)[0,3])
            return nep
        def NEP_pab(w):
            nep = np.abs(noise_pab(w)/S(w)[0,3])
            return nep
        def NEP_pulse(w):
            nep = np.abs(noise_pulse(w)/S(w)[0,3])
            return nep
        def NEP_pau1wb1(w):
            nep = np.abs(noise_pau1wb1(w)/S(w)[0,3])
            return nep
        def NEP_pwb1au2(w):
            nep = np.abs(noise_pwb1au2(w)/S(w)[0,3])
            return nep
        def NEP_psiau2(w):
            nep = np.abs(noise_psiau2(w)/S(w)[0,3])
            return nep
        def NEP_psim(w):
            nep = np.abs(noise_psim(w)/S(w)[0,3])
            return nep
        def NEP_pmwb2(w):
            nep = np.abs(noise_pmwb2(w)/S(w)[0,3])
            return nep
        def NEP_pwb2b(w):
            nep = np.abs(noise_pwb2b(w)/S(w)[0,3])
            return nep
        def NEP_psib(w):
            nep = np.abs(noise_psib(w)/S(w)[0,3])
            return nep
        def integrand(f):
            w = 2*np.pi*f
            NEP_total_squared = NEP_eint(w)**2+NEP_eext(w)**2+NEP_pau2te(w)**2+NEP_ptem(w)**2+NEP_psite(w)**2+NEP_paau1(w)**2+NEP_pab(w)**2+NEP_pulse(w)**2+NEP_pau1wb1(w)**2+NEP_pwb1au2(w)**2+NEP_psiau2(w)**2+NEP_psim(w)**2+NEP_pmwb2(w)**2+NEP_pwb2b(w)**2 + NEP_psib(w)**2
            integrand = 1./NEP_total_squared
            return integrand
    
        # Integrate the NEP
        integral = quad(integrand,0,np.inf)
        # Compute the resolution of our detector
        resolution = 2.35*np.sqrt(4.*integral[0])**(-1.)
        # Convert the resolution to eV
        resolution_ev = resolution/p['eVtoJ']
        print("Resolution in eV = ",resolution_ev)
        # Compute an ideal TES resolution
        rough_resolution = 2.355*np.sqrt(4*p['kb']*Tc**2*ctot/alpha0*np.sqrt(p['nmwb2']/2.))
        # Convert this to eV
        rough_resolution_ev = rough_resolution/p['eVtoJ']
        #print("Ideal Resolution in eV = ",rough_resolution_ev)
    else:
        resolution_ev = 0
    return deltaT_exp,nonlinearsol,ODEt,LINt,eq,resolution_ev

# This is the plotting function
def make_plots(dT,T,ODEt,LINt,eq,linear_flag,nonlinear_flag,freq_flag,npz_flag,save_fig_flag,savename): #(data for linear case, data for nonlinear case, times for nonlinear case, times for linear case, equilibrium point, plot linear case?, plot nonlinar case?, plot frequency case?, save data to NPZ files?, save figures?, filename for save)
    print("Plotting...")
    ODEt = np.array(ODEt) #make sure that the nonlinear solver times are in an array
    dI, dT_Te, dT_a, dT_Au1, dT_wb1, dT_Au2, dT_Si, dT_m, dT_wb2 = map(np.array, dT) #assign linear solutions to arrays
    I, Ta, Tau1, Twb1, Tau2, Tsi, Tte, Tm, Twb2 = map(np.array, T) #assign nonlinear solutions to arrays
    start = int(np.where(Tte == np.max(Tte))[0]) #set the starting point for the time values in the curve fit
    p0=[1.0e-5,.01,Tte[0]] #give some approximate starting values for the curve fit p0[0]*e^(t/p[1])+p[2]
    fitParams, fitCovariance = curve_fit(exponential, ODEt[start:len(ODEt)-1], Tte[start:len(Tte)-1],p0)	# curve_fit(function_fit, x_fitted, y_fitted, initial parameters)
    amp,tau, offset = fitParams #assign the result of the fit to these variables
    print("tau = ", tau)

    if npz_flag == 1:
        save_name = 'NPZ of Linear Data ' + str(savename) #+ identifier #Create an NPZ filename 
        np.savez(save_name, time = LINt, current = dI, absorber_temp = dT_a, gold_pad_1_temp = dT_Au1, wirebond_1_temp = dT_wb1, gold_pad_2_temp = dT_Au2, silicon_temp = dT_Si, tes_electron_temp = dT_Te, meander_temp = dT_m, wirebond_2_temp = dT_wb2) #index = index
        save_name = 'NPZ of Nonlinear Data ' + str(savename) #+ identifier #Create an NPZ filename 
        np.savez(save_name, time = ODEt, current = I, absorber_temp = Ta, gold_pad_1_temp = Tau1, wirebond_1_temp = Twb1, gold_pad_2_temp = Tau2, silicon_temp = Tsi, tes_electron_temp = Tte, meander_temp = Tm, wirebond_2_temp = Twb2) #index = index
    if linear_flag == 1:
        fig = plt.figure(figsize=(18, 18)) #make the plots bigger

        #Plot absorber Temperature
        plt.subplot(2,1,1)
        plt.plot(LINt,dT_a,label="Absorber Temperature")
        plt.plot(LINt,dT_Au1,label="Gold Pad 1 Temperature") 
        plt.plot(LINt,dT_wb1,label="Wirebond 1 Temperature")
        plt.plot(LINt,dT_Si,label="Silicon Chip Temperature")
        plt.plot(LINt,dT_Te,label="TES Electron System Temperature")
        plt.plot(LINt,dT_Au2,label="Gold Pad 2 Temperature")
        plt.plot(LINt,dT_m,label="Meander Temperature")
        plt.plot(LINt,dT_wb2,label="Wirebond 2 Temperature")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [0.1 ms]")
        plt.legend()
        plt.title("Linear Solver")
        fig.subplots_adjust(hspace=.5)
        
        #Plot Current
        plt.subplot(2,1,2)
        plt.plot(dI,label="Current") 
        plt.ylabel("Current [I]")
        plt.xlabel("Time [" + str(p['lintime']/p['numlinpts']) + "s]")
        plt.legend()
        
        if save_fig_flag == 1:
             namepng = 'linear ' + str(savename) + '.png'   
             plt.savefig(namepng) #Save the figure to current directory
             nameeps = 'linear ' + str(savename) + '.eps'   
             plt.savefig(nameeps) #Save the figure to current directory
       
        plt.show() #Display the figure

    if nonlinear_flag == 1:
        fig = plt.figure(figsize=(18, 18)) #make the plots bigger

        ### Linear Plot ###
        
        #Plot temperatures
        plt.subplot(5,1,1)
        plt.plot(ODEt - p['t0'],Ta,label="Absorber Temperature")
        plt.plot(ODEt - p['t0'],Tau1,label="Gold Pad 1 Temperature") 
        plt.plot(ODEt - p['t0'],Twb1,label="Wirebond 1 Temperature")
        #plt.plot(ODEt - p['t0'],Tsi,label="Silicon Chip Temperature")
        plt.plot(ODEt - p['t0'],Tau2,label="Gold Pad 2 Temperature")
        #plt.plot(ODEt - p['t0'],Tm,label="Meander Temperature")
        #plt.plot(ODEt - p['t0'],Twb2,label="Wirebond 2 Temperature")
        plt.plot(ODEt - p['t0'],Tte,label="TES Electron System Temperature",color = "k")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.title("Nonlinear Solver")
        plt.legend()
        
        #Plot Current
        plt.subplot(5,1,2)
        plt.plot(ODEt - p['t0'],I,label="Current") 
        plt.ylabel("Current [I]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        
        plt.subplot(5,1,3)
        plt.plot(ODEt - p['t0'],resistance(Tte,I),label = "Resistance")
        plt.ylabel("Resistance [Ohms]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        #plot power
        plt.subplot(5,1,4)
        plt.plot(ODEt - p['t0'],(I**2)*resistance(Tte,I),label = "Power")
        plt.ylabel("Power [Watts]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        if save_fig_flag == 1:
             namepng = 'nonlinear ' + str(savename) + '.png'   
             plt.savefig(namepng) #Save the figure to current directory
             nameeps = 'nonlinear ' + str(savename) + '.eps'   
             plt.savefig(nameeps) #Save the figure to current directory

        plt.show() #Display the figure
        
    if nonlinear_flag == 2:
        fig = plt.figure(figsize=(18, 18)) #make the plots bigger

        ### Plot mostly individually ###
        
        #Plot absorber Temperature
        plt.subplot(10,1,1)
        plt.plot(ODEt - p['t0'],Ta,label="Absorber Temperature")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        plt.title("Nonlinear Solver")
        fig.subplots_adjust(hspace=.5)
        
        #Plot Current
        plt.subplot(10,1,2)
        plt.plot(ODEt - p['t0'],I,label="Current") 
        plt.ylabel("Current [I]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        #Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
        plt.subplot(10,1,3)
        plt.plot(ODEt - p['t0'],Tau1,label="Gold Pad 1 Temperature") 
        plt.plot(ODEt - p['t0'],Twb1,label="Wirebond 1 Temperature")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        #Plot Silicon chip Temperature
        plt.subplot(10,1,5)
        plt.plot(ODEt - p['t0'],Tsi,label="Silicon Chip Temperature")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        #Plot TES Electron and Phonon System Temperatures on same graph
        plt.subplot(10,1,6)
        plt.plot(ODEt - p['t0'],Tte,label="TES Electron System Temperature")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        #Plot Gold Pad 2 Temperature
        plt.subplot(10,1,7)
        plt.plot(ODEt - p['t0'],Tau2,label="Gold Pad 2 Temperature")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        #Plot Meander and Wirebond 2 Temperatures on same graph
        plt.subplot(10,1,8)
        plt.plot(ODEt - p['t0'],Tm,label="Meander Temperature")
        plt.plot(ODEt - p['t0'],Twb2,label="Wirebond 2 Temperature")
        plt.ylabel("Temperature [K]")
        plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
        plt.legend()
        
        if save_fig_flag == 1:
             namepng = 'nonlinear ' + str(savename) + '.png'   
             plt.savefig(namepng) #Save the figure to current directory
             nameeps = 'nonlinear ' + str(savename) + '.eps'   
             plt.savefig(nameeps) #Save the figure to current directory

        plt.show() #Display the figure
    if freq_flag == 1:
        fig = plt.figure(figsize=(18, 18)) #make the plots bigger

        eint = np.sqrt(4.*p['kb']*eq[6]*R0)      #Internal Johnson Noise
        eext = np.sqrt(4.*p['kb']*p['Tb']*p['Rl'])     #external Johnson Noise across shunt resistor at bath temperature
        pau2te = phononnoise( G_au2te,eq[4],eq[6],p['nau2te'])
        ptem = phononnoise( G_tem,eq[6],eq[7],p['ntem'])
        psite = phononnoise( G_site,eq[5],eq[6],p['nsite'])
        paau1 = phononnoise( G_aau1,eq[1],eq[2],p['naau1'])
        pab = phononnoise( G_ab,eq[1],p['Tb'], p['nau2te'])
        pau1wb1 = phononnoise( G_au1wb1,eq[2],eq[3],p['nau1wb1'])
        pwb1au2 = phononnoise( G_wb1au2,eq[3],eq[4],p['nwb1au2'])
        psiau2 = phononnoise( G_siau2,eq[5],eq[4],p['nsiau2'])
        psim = phononnoise( G_sim,eq[5],eq[7],p['nsim'])
        pmwb2 = phononnoise(p['G_mwb2'],eq[7],eq[8],p['nmwb2'])
        pwb2b = phononnoise( G_wb2b,eq[8],p['Tb'],p['nwb2b'])
        psib = phononnoise( G_sib,eq[5],p['Tb'],p['nsib'])
        pulse = 0.
        
        # Assemble the noise sources into the vectors
        x_eint      = np.array([eint,0,   -eint*eq[0],     0,     0,        0,       0,      0,      0,      0])
        x_eext      = np.array([0,   eext,0,               0,     0,        0,       0,      0,      0,      0])
        x_pau2te    = np.array([0,   0,   pau2te,          0,     0,        0,       -pau2te,0,      0,      0])
        x_ptem      = np.array([0,   0,   -ptem,           0,     0,        0,       0,      0,      ptem,   0])
        x_psite     = np.array([0,   0,   psite,           0,     0,        0,       0,      -psite, 0,      0])
        x_paau1     = np.array([0,   0,   0,               -paau1,paau1,    0,       0,      0,      0,      0])
        x_pab       = np.array([0,   0,   0,               -pab,  0,        0,       0,      0,      0,      0])
        x_pulse     = np.array([0,   0,   0,               pulse, 0,        0,       0,      0,      0,      0])
        x_pau1wb1   = np.array([0,   0,   0,               0,     -pau1wb1, pau1wb1, 0,      0,      0,      0])
        x_pwb1au2   = np.array([0,   0,   0,               0,     0,        -pwb1au2,pwb1au2,0,      0,      0])
        x_psiau2    = np.array([0,   0,   0,               0,     0,        0,       psiau2, -psiau2,0,      0])
        x_psim      = np.array([0,   0,   0,               0,     0,        0,       0,      -psim,  psim,   0])
        x_pmwb2     = np.array([0,   0,   0,               0,     0,        0,       0,      0,      -pmwb2, pmwb2])
        x_pwb2b     = np.array([0,   0,   0,               0,     0,        0,       0,      0,      0,      -pwb2b])
        x_psib      = np.array([0,   0,   0,               0,     0,        0,       0,      -psib,  0,      0])
        
        # Define the actual noise terms
        def noise_eint(w):
            y = np.matmul(S(w),x_eint)
            return np.abs(y[0])
        def noise_eext(w):
            y = np.matmul(S(w),x_eext)
            return np.abs(y[0])
        def noise_pau2te(w):
            y = np.matmul(S(w),x_pau2te)
            return np.abs(y[0])
        def noise_ptem(w):
            y = np.matmul(S(w),x_ptem)
            return np.abs(y[0])
        def noise_psite(w):
            y = np.matmul(S(w),x_psite)
            return np.abs(y[0])
        def noise_paau1(w):
            y = np.matmul(S(w),x_paau1)
            return np.abs(y[0])
        def noise_pab(w):
            y = np.matmul(S(w),x_pab)
            return np.abs(y[0])
        def noise_pulse(w):
            y = np.matmul(S(w),x_pulse)
            return np.abs(y[0])
        def noise_pau1wb1(w):
            y = np.matmul(S(w),x_pau1wb1)
            return np.abs(y[0])
        def noise_pwb1au2(w):
            y = np.matmul(S(w),x_pwb1au2)
            return np.abs(y[0])
        def noise_psiau2(w):
            y = np.matmul(S(w),x_psiau2)
            return np.abs(y[0])
        def noise_psim(w):
            y = np.matmul(S(w),x_psim)
            return np.abs(y[0])
        def noise_pmwb2(w):
            y = np.matmul(S(w),x_pmwb2)
            return np.abs(y[0])
        def noise_pwb2b(w):
            y = np.matmul(S(w),x_pwb2b)
            return np.abs(y[0])
        def noise_psib(w):
            y = np.matmul(S(w),x_psib)
            return np.abs(y[0])
        def total_noise(w):
            noise = np.sqrt(noise_eint(w)**2+noise_eext(w)**2+noise_pau2te(w)**2+noise_ptem(w)**2+noise_psite(w)**2+noise_paau1(w)**2+noise_pab(w)**2+noise_pulse(w)**2+noise_pau1wb1(w)**2+noise_pwb1au2(w)**2+noise_psiau2(w)**2+noise_psim(w)**2+noise_pmwb2(w)**2+noise_pwb2b(w)**2+noise_psib(w)**2)
            return noise
        
        frequencies = np.logspace(p['freq_min_exp'],p['freq_max_exp'],int(p['log_points']))
        array_eint = []
        array_eext = []
        array_pau2te = []
        array_ptem = []
        array_psite = []
        array_paau1 = []
        array_pab = []
        array_pulse = []
        array_pau1wb1 = []
        array_pwb1au2 = []
        array_psiau2 = []
        array_psim = []
        array_pmwb2 = []
        array_pwb2b = []
        array_total = []
        array_psib  = []
        for i in range(0,len(frequencies)):
                array_eint.append(noise_eint(2.*np.pi*frequencies[i]))
                array_eext.append(noise_eext(2.*np.pi*frequencies[i]))
                array_pau2te.append(noise_pau2te(2.*np.pi*frequencies[i]))
                array_ptem.append(noise_ptem(2.*np.pi*frequencies[i]))
                array_psite.append(noise_psite(2.*np.pi*frequencies[i]))
                array_paau1.append(noise_paau1(2.*np.pi*frequencies[i]))
                array_pab.append(noise_pab(2.*np.pi*frequencies[i]))
                array_pulse.append(noise_pulse(2.*np.pi*frequencies[i]))
                array_pau1wb1.append(noise_pau1wb1(2.*np.pi*frequencies[i]))
                array_pwb1au2.append(noise_pwb1au2(2.*np.pi*frequencies[i]))
                array_psiau2.append(noise_psiau2(2.*np.pi*frequencies[i]))
                array_psim.append(noise_psim(2.*np.pi*frequencies[i]))
                array_pmwb2.append(noise_pmwb2(2.*np.pi*frequencies[i]))
                array_pwb2b.append(noise_pwb2b(2.*np.pi*frequencies[i]))
                array_total.append(total_noise(2.*np.pi*frequencies[i]))
                array_psib.append(noise_psib(2.*np.pi*frequencies[i]))
        if npz_flag == 1:
            save_name = 'NPZ of Frequency Data ' + str(savename) #+ identifier #Create an NPZ filename 
            np.savez(save_name, freqs = frequencies, eint = array_eint, eext = array_eext, pau2te = array_pau2te, ptem = array_ptem, psite = array_psite, paau1 = array_paau1, pab = array_pab, pulse = array_pulse, pau1wb1 = array_pau1wb1, pwb1au2 = array_pwb1au2, psiau2 = array_psiau2, psim = array_psim, pmwb2 = array_pmwb2, pwb2b = array_pwb2b, total = array_total) #index = index

        plt.loglog(frequencies,array_eint,label = 'eint')
        plt.loglog(frequencies,array_eext,label = 'eext')
        plt.loglog(frequencies,array_pau2te,label = 'pau2te')
        plt.loglog(frequencies,array_ptem,label = 'ptem')
        plt.loglog(frequencies,array_psite,label = 'psite')
        plt.loglog(frequencies,array_paau1,label = 'paau1')
        plt.loglog(frequencies,array_pab,label = 'pab')
        plt.loglog(frequencies,array_pulse,label = 'pulse')
        plt.loglog(frequencies,array_pau1wb1,label = 'pau1wb1',linestyle = ':')
        plt.loglog(frequencies,array_pwb1au2,label = 'pwb1au2',linestyle = ':')
        plt.loglog(frequencies,array_psiau2,label = 'psiau2',linestyle = ':')
        plt.loglog(frequencies,array_psim,label = 'psim',linestyle = ':')
        plt.loglog(frequencies,array_pmwb2,label = 'pmwb2',linestyle = ':')
        plt.loglog(frequencies,array_pwb2b,label = 'pwb2b',linestyle = ':')
        plt.loglog(frequencies,array_total,label = 'total',linewidth = 2.,color = 'k')
        plt.loglog(frequencies,array_psib,label = 'psib',linestyle = ':')
        #axes = plt.gca()
        #axes.set_ylim([1e-14,2e-11])
        plt.title("Frequency vs Noise")
        plt.ylabel("Log Current Noise [A/$\sqrt{Hz}$]")
        plt.xlabel("Log Frequency")
        plt.legend()
        plt.grid()
        if save_fig_flag == 1:
             namepng = 'Frequency Spectrum ' + str(savename) + '.png'   
             plt.savefig(namepng) #Save the figure to current directory
             nameeps = 'Frequency Spectrum ' + str(savename) + '.eps'   
             plt.savefig(nameeps) #Save the figure to current directory

        plt.show()

### Use this to run the main fuction in its entirety
deltaT_exp, nonlinearsol,ODEt,LINt,eq,resolution_ev = main(N(),1,1,1)

#print(p['G_mwb2'])

### Use this to plot the results of the above
make_plots(deltaT_exp,nonlinearsol,ODEt,LINt,eq,0,0,0,0,0,'silicon to bath')

### Script used to run the parallel processing 
def for_process(base,N,G,Aau1,L,A): #(base for file name, matrix for linear case, new value for Gmwb2, new value for Aau1, new value for Ltes, new value for Ates)
    fname = base + str(G) + "_" + str(Aau1) + "_" + str(L) + "_" + str(A) + ".txt" #create a filename unique to this run of the parameters
    f = open(fname,'w+') #open the file with the above filename
    length_on_chip = p['Lm']*10.0e-6*1.0e3/0.0019 #compute the length that the meander would take up on the chip.  Assumes that you can get 1.9mm every 10 micrometers (becuase 2mm wide chip and 5 micrometer width)
    if length_on_chip < 3: # if the length is less than 3mm (leaving 1mm for the rest of the components), continue computing
        deltaT_exp, nonlinearsol,ODEt,LINt,eq,resolution_ev = main(N,0,1,0) #Compute a nonlinear pulse
        I, Ta, Tau1, Twb1, Tau2, Tsi, Tte, Tm, Twb2 = map(np.array, nonlinearsol) #assign the solution to an array
        start = int(np.where(Tte == np.max(Tte))[0]) #set the starting point for the time values in the curve fit
        p0=[1.0e-5,.01,Tte[0]] #give some approximate starting values for the curve fit p0[0]*e^(t/p[1])+p[2]
        fitParams, fitCovariance = curve_fit(exponential, ODEt[start:len(ODEt)-1], Tte[start:len(Tte)-1],p0)	# curve_fit(function_fit, x_fitted, y_fitted, initial parameters)
        amp,tau, offset = fitParams #assign the result of the fit to these variables
        print("tau = ", tau)
        if tau < 0.01: #if the time constant is less than 50ms
            deltaT_exp, nonlinearsol,ODEt,LINt,eq,resolution_ev = main(N,0,0,1) #compute the frequency domain analysis
            #Write the success to the output file with the determined resolution
            f.write("Met constraints with G = " + str(G) + " V = " + str(Aau1) + " L = " + str(L) + " A = " + str(A) + " and resolution " + str(resolution_ev)+ " and tau = " + str(tau) + '\n')
        if tau >= 0.01: #if the time constant is greater than or equal to 50ms
            # Write the failure to the output file  
            f.write("DID NOT MEET CONSTRAINTS with G = " + str(G) + " V = " + str(Aau1) + " L = " + str(L) + " A = " + str(A)+ '\n')
    else: #if the length is longer tha 3mm write the faulure to the output file
        f.write("DID NOT MEET CONSTRAINTS with G = " + str(G) + " V = " + str(Aau1) + " L = " + str(L) + " A = " + str(A)+ '\n')
    f.close() #close the file
    
#for_process("1Optimization_Results_",N(),4.7e-11,sys.argv[1],2.0e-4,1.2e-10)
