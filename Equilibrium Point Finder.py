import numpy as np  # Import Numpy
from scipy.optimize import root# Alternative equilibrium point finder
np.set_printoptions(threshold=np.nan) #Let us print the full array if necessary
import yaml

#Use YAML to read the input file and turn it into a Python Dictionary
stream = open("input.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')

### Derived Parameters ###
p['R0']     =p['Rn'] * p['frac']          #Resistance at equilibrium

### Conductance Constants ###

p['kab']    = p['G_ab']/(p['nab']*p['Tc']**(p['nab']-1.))                #Conductance Constant Absorber to Bath [W/K^4]
p['kaau1']  = p['G_aau1']/(p['naau1']*p['Tc']**(p['naau1']-1.))           #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
p['kag']    = p['G_ag']/(p['nag']*p['Tc']**(p['nag']-1.))            #Conductance Constant Absorber to Glue [W/K^3.5]
p['kau1wb1']= p['G_au1wb1']/(p['nau1wb1']*p['Tc']**(p['nau1wb1']-1.))         #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
p['kgsi']   = p['G_gsi']/(p['ngsi']*p['Tc']**(p['ngsi']-1.))           #Conductance Constant Glue to Silicon [W/K^3.5]
p['kwb1au2']= p['G_wb1au2']/(p['nwb1au2']*p['Tc']**(p['nwb1au2']-1.))         #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
p['ksiau2'] = p['G_siau2']/(p['nsiau2']*p['Tc']**(p['nsiau2']-1.))      #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
p['kau2te'] = p['G_au2te']/(p['nau2te']*p['Tc']**(p['nau2te']-1.))          #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
p['ksim']   = p['G_sim']/(p['nsim']*p['Tc']**(p['nsim']-1.))           #Conductance Constant Silicon to Meander [W/K^4]
p['ksite']  = p['G_site']/(p['nsite']*p['Tc']**(p['nsite']-1.))           #Conductance Constant TES Phonon System to TES Electron System [W/K^6]
p['ktem']   = p['G_tem']/(p['ntem']*p['Tc']**(p['ntem']-1.))            #Conductance Constant TES Electron System to MEander [W/K^2]
p['kmwb2']  = p['G_mwb2']/(p['nmwb2']*p['Tc']**(p['nmwb2']-1.))           #Conductance Constant Meander to Wirebond 2 [W/K^2]
p['kwb2b']  = p['G_wb2b']/(p['nwb2b']*p['Tc']**(p['nwb2b']-1.))           #Conductance Constant Wirecond 2 to Bath [W/K^4]

###Power Calculation ###

p['P0']     = p['kmwb2']*((p['Tc'])**2-(p['Tb'])**2)  #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
p['I0_P']   = np.sqrt(p['P0']/p['R0'])              #Equilibrium Current calculated from power, used in resistance equation
p['vb']     = p['I0_P'] * p['Rl']                    #Voltage Bias
#print("P0",p['P0'],"I0",p['I0_P'],"vb",p['vb'])          #Print the equilibrium power, current, and bias voltage


### Definition of Functions ###

def resistance(Ttes,Ites):
    R = p['Rn']/2 *(1 + np.tanh((Ttes - p['Tc']) * p['alpha0']/p['Tc'] + (Ites - p['I0_P'])*p['beta0']/p['I0_P']))
    return R


def equations(w):
    # variables to solve for
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Tm,Twb2 = w
    ### Array of functions to solve ###
    f1 = [1e13*((resistance(Tte,I)-p['R0'])),
             1e9*(-p['kab']*(Ta**p['nab']-(p['Tb'])**p['nab'])-p['kaau1']*(Ta**p['naau1']-Tau1**p['naau1'])-p['kag']*(Ta**p['nag']-Tg**p['nag']))//p['ca'],
             1e8*(-p['kaau1']*(Tau1**p['naau1']-Ta**p['naau1'])-p['kau1wb1']*(Tau1**p['nau1wb1']-Twb1**p['nau1wb1']))/p['cau1'],
             1e9*(-p['kag']*(Tg**p['nag']-Ta**p['nag'])-p['kgsi']*(Tg**p['ngsi']-Tsi**p['ngsi']))/p['cg'],
             1e8*(-p['kau1wb1']*(Twb1**p['nau1wb1']-Tau1**p['nau1wb1'])-p['kwb1au2']*(Twb1**p['nwb1au2']-Tau2**p['nwb1au2']))/p['cwb1'],
             1e9*(-p['ksiau2']*(Tau2**p['nsiau2']-Tsi**p['nsiau2'])-p['kwb1au2']*(Tau2**p['nwb1au2']-Twb1**p['nwb1au2'])-p['kau2te']*(Tau2**p['nau2te']-Tte**p['nau2te']))//p['cau2'],
             10*(-p['kgsi']*(Tsi**p['ngsi']-Tg**p['ngsi']) - p['ksiau2']*(Tsi**p['nsiau2']-Tau2**p['nsiau2'])-p['ksite']*(Tsi**p['nsite']-Tte**p['nsite'])-p['ksim']*(Tsi**p['nsim']-Tm**p['nsim']))/p['csi'],
             1e8*((I**2.*resistance(Tte,I)-p['kau2te']*(Tte**p['nau2te']-Tau2**p['nau2te'])-p['ksite']*(Tte**p['nsite']-Tsi**p['nsite'])-p['ktem']*(Tte**p['ntem']-Tm**p['ntem'])))/p['cte'],
             1e6*(-p['ktem']*(Tm**p['ntem']-Tte**p['ntem'])-p['ksim']*(Tm**p['nsim']-Tsi**p['nsim'])-p['kmwb2']*(Tm**p['nmwb2']-Twb2**p['nmwb2']))/p['cm'],
             1e12*(-p['kmwb2']*(Twb2**p['nmwb2']-Tm**p['nmwb2'])-p['kwb2b']*(Twb2**p['nwb2b']-(p['Tb'])**p['nwb2b']))/p['cwb2']]
    return f1


def equations_unweighted(w):
    # variables to solve for
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = w
    ### Array of functions to solve ###
    f1 = [1*((resistance(Tte,I)-p['R0'])),
             1*(-p['kab']*(Ta**p['nab']-(p['Tb'])**p['nab'])-p['kaau1']*(Ta**p['naau1']-Tau1**p['naau1'])-p['kag']*(Ta**p['nag']-Tg**p['nag']))//p['ca'],
             1*(-p['kaau1']*(Tau1**p['naau1']-Ta**p['naau1'])-p['kau1wb1']*(Tau1**p['nau1wb1']-Twb1**p['nau1wb1']))/p['cau1'],
             1*(-p['kag']*(Tg**p['nag']-Ta**p['nag'])-p['kgsi']*(Tg**p['ngsi']-Tsi**p['ngsi']))/p['cg'],
             1*(-p['kau1wb1']*(Twb1**p['nau1wb1']-Tau1**p['nau1wb1'])-p['kwb1au2']*(Twb1**p['nwb1au2']-Tau2**p['nwb1au2']))/p['cwb1'],
             1*(-p['ksiau2']*(Tau2**p['nsiau2']-Tsi**p['nsiau2'])-p['kwb1au2']*(Tau2**p['nwb1au2']-Twb1**p['nwb1au2'])-p['kau2te']*(Tau2**p['nau2te']-Tte**p['nau2te']))//p['cau2'],
             1*(-p['kgsi']*(Tsi**p['ngsi']-Tg**p['ngsi']) - p['ksiau2']*(Tsi**p['nsiau2']-Tau2**p['nsiau2'])-p['ksitp']*(Tsi**p['nsitp']-Ttp**p['nsitp'])-p['ksim']*(Tsi**p['nsim']-Tm**p['nsim']))/p['csi'],
             1*((I**2.*resistance(Tte,I)-p['kau2te']*(Tte**p['nau2te']-Tau2**p['nau2te'])-p['ktpte']*(Tte**p['ntpte']-Ttp**p['ntpte'])-p['ktem']*(Tte**p['ntem']-Tm**p['ntem'])))/p['cte'],
             1*(-p['ksitp']*(Ttp**p['nsitp']-Tsi**p['nsitp'])-p['ktpte']*(Ttp**p['ntpte']-Tte**p['ntpte']))/p['ctp'],
             1*(-p['ktem']*(Tm**p['ntem']-Tte**p['ntem'])-p['ksim']*(Tm**p['nsim']-Tsi**p['nsim'])-p['kmwb2']*(Tm**p['nmwb2']-Twb2**p['nmwb2']))/p['cm'],
             1*(-p['kmwb2']*(Twb2**p['nmwb2']-Tm**p['nmwb2'])-p['kwb2b']*(Twb2**p['nwb2b']-(p['Tb'])**p['nwb2b']))/p['cwb2']]
    
    return f1


### Find the Equilibrium Point ###


x0 = [12e-6,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.01]            #Initial Guess
solution = root(equations,x0,method = 'lm') #Use Root to find the solution
print(equations(solution.x))                                        #Print the equations evaluated at the equilibrium point
print(solution.x)                                                   #Print the solutions themselves