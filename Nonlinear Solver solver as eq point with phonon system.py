import numpy as np  # Import Numpy
import matplotlib.pyplot as plt # Import plotting functions
from scipy.integrate import odeint # Import Nonlinear Solver
np.set_printoptions(threshold=np.nan) #Let us print the full array if necessary
import yaml

stream = open("input copy with TES phonon System still in.txt", "r")
p = yaml.load(stream)
stream.close()
for v in p.values():
    if type(v) != float:
        raise ValueError('An input was not a float!')  
#for k in p:
#    print(k,p[k])

### Derived Parameters ###

### Electrical Components ###
p['R0']     =p['Rn'] * p['frac']          #Resistance at equilibrium

### Heat Capacity for Igor ###
#else_heat_cap = p['ca']+p['cau1']+p['cg'] +p['cwb1']+p['cau2']+p['csi']+p['ctp']+p['cm']
#total_cap = else_heat_cap + p['cwb2'] + p['cm']
#print("capacity",else_heat_cap)

### Conductivity for Igor ###
#tes_abs_G = 1/(1/p['G_aau1']+2/p['G_au1wb1']+1/p['G_au2te'])
#total_conductance = tes_abs_G + p['G_mwb2'] + p['G_wb2b'] 
#print("conductivity",tes_abs_G)

### Conductance Constants ###

p['kab']    = p['G_ab']/(p['nab']*p['Tc']**(p['nab']-1.))                #Conductance Constant Absorber to Bath [W/K^4]
p['kaau1']  = p['G_aau1']/(p['naau1']*p['Tc']**(p['naau1']-1.))           #Conductance Constant Absorber to Gold Pad 1 [W/K^2]
p['kag']    = p['G_ag']/(p['nag']*p['Tc']**(p['nag']-1.))            #Conductance Constant Absorber to Glue [W/K^3.5]
p['kau1wb1']= p['G_au1wb1']/(p['nau1wb1']*p['Tc']**(p['nau1wb1']-1.))         #Conductance Constant Gold Pad 1 to Wirebond 1 [W/K^2]
p['kgsi']   = p['G_gsi']/(p['ngsi']*p['Tc']**(p['ngsi']-1.))           #Conductance Constant Glue to Silicon [W/K^3.5]
p['kwb1au2']= p['G_wb1au2']/(p['nwb1au2']*p['Tc']**(p['nwb1au2']-1.))         #Conductance Constant Wirebond 1 to Gold pad 2 [W/K^2]
p['ksiau2'] = p['G_siau2']/(p['nsiau2']*p['Tc']**(p['nsiau2']-1.))      #Conductance Constant Silicon to Gold Pad 2 [W/K^4]
p['kau2te'] = p['G_au2te']/(p['nau2te']*p['Tc']**(p['nau2te']-1.))          #Conductance Constant Gold Pad 2 to TES Electron System [W/K^2]
p['ksitp']  = p['G_sitp']/(p['nsitp']*p['Tc']**(p['nsitp']-1.))          #Conductance Constant Silicon to TES Phonon System [W/K^4]
p['ksim']   = p['G_sim']/(p['nsim']*p['Tc']**(p['nsim']-1.))           #Conductance Constant Silicon to Meander [W/K^4]
p['ktpte']  = p['G_tpte']/(p['ntpte']*p['Tc']**(p['ntpte']-1.))           #Conductance Constant TES Phonon System to TES Electron System [W/K^6]
p['ktem']   = p['G_tem']/(p['ntem']*p['Tc']**(p['ntem']-1.))            #Conductance Constant TES Electron System to MEander [W/K^2]
p['kmwb2']  = p['G_mwb2']/(p['nmwb2']*p['Tc']**(p['nmwb2']-1.))           #Conductance Constant Meander to Wirebond 2 [W/K^2]
p['kwb2b']  = p['G_wb2b']/(p['nwb2b']*p['Tc']**(p['nwb2b']-1.))           #Conductance Constant Wirecond 2 to Bath [W/K^4]

###Power Calculation ###

p['P0']     = p['kmwb2']*((p['Tc'])**2-(p['Tb'])**2)  #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
p['I0_P']   = np.sqrt(p['P0']/p['R0'])              #Equilibrium Current calculated from power, used in resistance equation
p['vb']     = p['I0_P'] * p['Rl']                    #Voltage Bias
#print("P0",p['P0'],"I0",p['I0_P'],"vb",p['vb'])          #Print the equilibrium power, current, and bias voltage





### Definition of Functions ###


### Define the resistance function ###

def resistance(Ttes,Ites):
    R = p['Rn']/2 *(1 + np.tanh((Ttes - p['Tc']) * p['alpha0']/p['Tc'] + (Ites - p['I0_P'])*p['beta0']/p['I0_P']))
    return R

### Set up the Nonliear ODEs ###

#vectorfield(variables,time array)
def vectorfield(w,t):
    ### Variables ###
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = w 
    ### Array of the functions to solve ###
    f1 = [(p['I0_P']*p['R0'] - I*resistance(Tte,I))/p['L'],
         (((p['E']/(p['c']*np.sqrt(2*np.pi)))*np.exp(-(t - p['t0'])**2/(2*p['c']**2)))-p['kab']*(Ta**p['nab']-(p['Tb'])**p['nab'])-p['kaau1']*(Ta**p['naau1']-Tau1**p['naau1'])-p['kag']*(Ta**p['nag']-Tg**p['nag']))/p['ca'],
         (-p['kaau1']*(Tau1**p['naau1']-Ta**p['naau1'])-p['kau1wb1']*(Tau1**p['nau1wb1']-Twb1**p['nau1wb1']))/p['cau1'],
         (-p['kag']*(Tg**p['nag']-Ta**p['nag'])-p['kgsi']*(Tg**p['ngsi']-Tsi**p['ngsi']))/p['cg'],
         (-p['kau1wb1']*(Twb1**p['nau1wb1']-Tau1**p['nau1wb1'])-p['kwb1au2']*(Twb1**p['nwb1au2']-Tau2**p['nwb1au2']))/p['cwb1'],
         (-p['ksiau2']*(Tau2**p['nsiau2']-Tsi**p['nsiau2'])-p['kwb1au2']*(Tau2**p['nwb1au2']-Twb1**p['nwb1au2'])-p['kau2te']*(Tau2**p['nau2te']-Tte**p['nau2te']))/p['cau2'],
         (-p['kgsi']*(Tsi**p['ngsi']-Tg**p['ngsi']) - p['ksiau2']*(Tsi**p['nsiau2']-Tau2**p['nsiau2'])-p['ksitp']*(Tsi**p['nsitp']-Ttp**p['nsitp'])-p['ksim']*(Tsi**p['nsim']-Tm**p['nsim']))/p['csi'],
         ((I**2.*resistance(Tte,I)-p['kau2te']*(Tte**p['nau2te']-Tau2**p['nau2te'])-p['ktpte']*(Tte**p['ntpte']-Ttp**p['ntpte'])-p['ktem']*(Tte**p['ntem']-Tm**p['ntem'])))/p['cte'],
         (-p['ksitp']*(Ttp**p['nsitp']-Tsi**p['nsitp'])-p['ktpte']*(Ttp**p['ntpte']-Tte**p['ntpte']))/p['ctp'],
         (-p['ktem']*(Tm**p['ntem']-Tte**p['ntem'])-p['ksim']*(Tm**p['nsim']-Tsi**p['nsim'])-p['kmwb2']*(Tm**p['nmwb2']-Twb2**p['nmwb2']))/p['cm'],
         (-p['kmwb2']*(Twb2**p['nmwb2']-Tm**p['nmwb2'])-p['kwb2b']*(Twb2**p['nwb2b']-(p['Tb'])**p['nwb2b']))/p['cwb2']]
    #print(t)
    #print I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2
    #print(f1)
    #print("--------------------")
    return f1





### Solve the System of ODEs ###

t = [p['stoptime'] * float(i) / (p['numpoints'] - 1) for i in range(int(p['numpoints']))] #Make an array of the time points

#Put the initial conditions into an array

#      0  1   2   3   4    5   6   7   8   9  10
#w0 = [I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2]

#Initials from Ziqing Minimizer
#w0 = [1.288765e-04,9.945124e-02,9.948128e-02,9.947379e-02,9.953269e-02,9.959576e-02,9.952707e-02,9.985873e-02,9.985867e-02,9.937393e-02,8.014015e-02]
#Initials for G_mwb2=3e-7
#w0 = [0.000102063349559,0.100020637726,0.100021192109,0.0999648025488,0.10002125406,0.10002131601,0.0999088892956,0.100022102219,0.100022038446,0.0995468688251,0.0801415071554]
#Initials for G_mwb2=3e-8
#w0 = [3.28498273476e-05,0.099997922294,0.0999979696537,0.0999933043136,0.099997974946,0.0999979802383,0.0999886858,0.0999980804345,0.0999980366777,0.0999492713927,0.0800075301895]
#w0 = [6.68539323983e-06,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.01]
#w01 = [6.70646959962e-06,0.0199994220114,0.0199994231258,0.0199994215639,0.0199994231384,0.019999423151,0.0199994211474,0.0199994231676,0.0199994211474,0.0199981272729,0.0100509958481]
#w0 = [1.7316562264e-06,2.00000237e-02,0.02,0.02,0.02,0.02,2.00000236e-02,0.02,0.02,0.02,0.01]
#Initials from Root Finder
w0 = [  1.73184513e-06,  2.00000237e-02,   2.00000237e-02,   2.00000236e-02,2.00000237e-02,   2.00000237e-02,   2.00000236e-02,   2.00000237e-02,2.00000236e-02,   1.99999371e-02,   1.00034358e-02]
#w0 = [1.7318517528e-06,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.01]
#w0 = [1.73165509491e-06,0.0200000913168,0.0200000913936,0.020000091289,0.0200000913944,0.0200000913952,0.0200000912611,0.0200000913964,0.0200000912611,0.0200000047115,0.0100034358387]
#w0 = [  5.53402737e-06,   1.93694626e-02,   1.95542771e-02,1.92971497e-02,   1.95563003e-02,   1.95583234e-02,1.92240146e-02,   1.95609856e-02,   1.92240146e-02,1.95609008e-02,   1.00032370e-02]
#w0 = [1.73184199019e-06,0.0200000475315,0.0200000481487,0.0200000473627,0.0200000481693,0.0200000481945,0.0200000473352,0.0200000482275,0.0200000473352,0.019999961543,0.0100034358189]
#w0 = [1.73177311476e-06,0.0200000636667,0.0200000640849,0.0200000635499,0.0200000640983,0.0200000641144,0.0200000635223,0.0200000641357,0.0200000635223,0.0199999774511,0.0100034358262]

#print(vectorfield(w0,0))
print("vf",vectorfield(w0,0))

def dfun(w):
    I,Ta,Tau1,Tg,Twb1,Tau2,Tsi,Tte,Ttp,Tm,Twb2 = w
    f = [-resistance(Tte,I)+I*p['Rn']/2 *(1 - np.tanh((Tte - p['Tc']) * p['alpha0']/p['Tc'] + (I - p['I0_P'])*p['beta0']/p['I0_P'])**2)*p['beta0']/p['I0_p'],
         -p['kab']*p['nab']*(Ta**(p['nab']-1.))-p['kaau1']*p['naau1']*(Ta**(p['naau1']-1.))-p['kag']*(Ta**(p['nag']-1.)),
         (-p['kag']*p['nag']*(Tg**(p['nag']-1.))-p['kgsi']*p['ngsi']*(Tg**(p['ngsi']-1.))),
         (-p['kau1wb1']*p['nau1wb1']*(Twb1**(p['nau1wb1']-1.))-p['kwb1au2']*p['nwb1au2']*(Twb1**(p['nwb1au2']-1.))),
         (-p['ksiau2']*p['nsiau2']*(Tau2**(p['nsiau2']-1.))-p['kwb1au2']*p['nwb1au2']*(Tau2**(p['nwb1au2']-1.))-p['kau2te']*p['nau2te']*(Tau2**(p['nau2te']-1.))),
         (-p['kgsi']*p['ngsi']*(Tsi**(p['ngsi']-1.)) - p['ksiau2']*p['nsiau2']*(Tsi**(p['nsiau2']-1.))-p['ksitp']*p['nsitp']*(Tsi**(p['nsitp']-1.))-p['ksim']*p['nsim']*(Tsi**(p['nsim']-1.))),
         ((2*I*resistance(Tte,I)+I**2*p['Rn']/2 *(1 - np.tanh((Tte - p['Tc']) * p['alpha0']/p['Tc'] + (I - p['I0_P'])*p['beta0']/p['I0_P'])**2)*p['beta0']/p['I0_p']-p['kau2te']*p['nau2te']*(Tte**(p['nau2te']-1.))-p['ktpte']*p['ntpte']*(Tte**(p['ntpte']-1.))-p['ktem']*p['ntem']*(Tte**(p['ntem']-1.)))),
         (-p['ksitp']*p['nsitp']*(Ttp**(p['nsitp']-1.))-p['ktpte']*p['ntpte']*(Ttp**(p['ntpte']-1.))),
         (-p['ktem']*p['ntem']*(Tm**(p['ntem']-1.))-p['ksim']*p['nsim']*(Tm**(p['nsim']-1.))-p['kmwb2']*p['nmwb2']*(Tm**(p['nmwb2']-1.))),
         (-p['kmwb2']*p['nmwb2']*(Twb2**(p['nmwb2']-1.))-p['kwb2b']*p['nwb2b']*(Twb2**(p['nwb2b']-1.)))]
    return f

### Run the ODE Solver ###
#odeint(equation function,initial condition,time array,absolute error,relative error)
wsol = odeint(vectorfield, w0, t,full_output = 0,atol=p['abserr'], rtol=p['relerr'],mxstep = 1000000,hmax = 0.00001)


#print(wsol)


### Plot the results ###


### Transpose the solution so we can easily plot it using the map function as in the linear case ###
solution = np.transpose(wsol)

### Separate out the solution into individual arrays that can be plotted ###
I, Ta, Tau1, Tg, Twb1, Tau2, Tsi, Tte, Ttp, Tm, Twb2 = map(np.array, solution)

### Output to an NPZ file, named above ###
save_name = 'NPZ of Nonlinear Data' #+ identifier #Create an NPZ filename 
np.savez(save_name, time = t, current = I, absorber_temp = Ta, gold_pad_1_temp = Tau1, glue_temp = Tg, wirebond_1_temp = Twb1, gold_pad_2_temp = Tau2, silicon_temp = Tsi, tes_electron_temp = Tte, tes_phonon_temp = Ttp, meander_temp = Tm, wirebond_2_temp = Twb2) #index = index


fig = plt.figure(figsize=(18, 18)) #Make the plots bigger

#Plot flag
plot_flag = 3

if plot_flag == 0:
    print("Thanks for not making me plot")

if plot_flag == 1:

    ### Linear Plot ###
    
    #Plot temperatures
    plt.subplot(5,1,1)
    plt.plot(Ta,label="Absorber Temperature")
    plt.plot(Tau1,label="Gold Pad 1 Temperature") 
    plt.plot(Twb1,label="Wirebond 1 Temperature")
    plt.plot(Tg,label="Glue Temperature")
    plt.plot(Tsi,label="Silicon Chip Temperature")
    plt.plot(Ttp,label="TES Phonon System Temperature")
    plt.plot(Tau2,label="Gold Pad 2 Temperature")
    #plt.plot(Tm,label="Meander Temperature")
    #plt.plot(Twb2,label="Wirebond 2 Temperature")
    plt.plot(Tte,label="TES Electron System Temperature",color = "k")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.title("Nonlinear Solver")
    #plt.legend()
    
    #Plot Current
    plt.subplot(5,1,2)
    plt.plot(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    
    plt.subplot(5,1,3)
    plt.plot(resistance(Tte,I),label = "Resistance")
    plt.ylabel("Resistance [Ohms]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #plot power
    plt.subplot(5,1,4)
    plt.plot((I**2)*resistance(Tte,I),label = "Power")
    plt.ylabel("Power [Watts]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    plt.show() #Display the figure

if plot_flag == 2:
    
    ### Log Plot ###
    
    #Plot temperatures
    plt.subplot(5,1,1)
    plt.semilogy(Ta,label="Absorber Temperature")
    plt.semilogy(Tau1,label="Gold Pad 1 Temperature") 
    plt.semilogy(Twb1,label="Wirebond 1 Temperature")
    plt.semilogy(Tg,label="Glue Temperature")
    plt.semilogy(Tsi,label="Silicon Chip Temperature")
    plt.semilogy(Tte,label="TES Electron System Temperature")
    plt.semilogy(Ttp,label="TES Phonon System Temperature")
    plt.semilogy(Tau2,label="Gold Pad 2 Temperature")
    #plt.semilogy(Tm,label="Meander Temperature")
    #plt.semilogy(Twb2,label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.title("Nonlinear Solver Log 2mK Pulse")
    plt.legend()
    
    #Plot Current
    plt.subplot(5,1,2)
    plt.semilogy(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    
    #Plot Resistance
    plt.subplot(5,1,3)
    plt.semilogy(resistance(Tte,I),label = "Resistance")
    plt.ylabel("Resistance [Ohms]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #plot power
    plt.subplot(5,1,4)
    plt.semilogy((I**2)*resistance(Tte,I),label = "Power")
    plt.ylabel("Power [Watts]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    plt.show() #Display the figure
    
if plot_flag == 3:
    
    ### Plot mostly individually ###
    
    #Plot absorber Temperature
    plt.subplot(10,1,1)
    plt.plot(Ta,label="Absorber Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    plt.title("Nonlinear Solver")
    fig.subplots_adjust(hspace=.5)
    
    #Plot Current
    plt.subplot(10,1,2)
    plt.plot(I,label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
    plt.subplot(10,1,3)
    plt.plot(Tau1,label="Gold Pad 1 Temperature") 
    plt.plot(Twb1,label="Wirebond 1 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Glue Temperature on same graph
    plt.subplot(10,1,4)
    plt.plot(Tg,label="Glue Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Silicon chip Temperature
    plt.subplot(10,1,5)
    plt.plot(Tsi,label="Silicon Chip Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot TES Electron and Phonon System Temperatures on same graph
    plt.subplot(10,1,6)
    plt.plot(Tte,label="TES Electron System Temperature")
    plt.plot(Ttp,label="TES Phonon System Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 2 Temperature
    plt.subplot(10,1,7)
    plt.plot(Tau2,label="Gold Pad 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Meander and Wirebond 2 Temperatures on same graph
    plt.subplot(10,1,8)
    plt.plot(Tm,label="Meander Temperature")
    #plt.plot(Twb2,label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    plt.show() #Display the figure

if plot_flag == 4:
    lower = 9000
    upper = 15000
    ### Pulse displaced from t=0 ###
    
    #Plot absorber Temperature
    plt.subplot(10,1,1)
    plt.plot(Ta[lower:upper],label="Absorber Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    plt.title("Nonlinear Solver 1KeV Split gmwb2_3e-8 11 July 2017 corrected Cs")
    fig.subplots_adjust(hspace=.5)
    
    #Plot Current
    plt.subplot(10,1,2)
    plt.plot(I[lower:upper],label="Current") 
    plt.ylabel("Current [I]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
    plt.subplot(10,1,3)
    plt.plot(Tau1[lower:upper],label="Gold Pad 1 Temperature") 
    plt.plot(Twb1[lower:upper],label="Wirebond 1 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Glue Temperature on same graph
    plt.subplot(10,1,4)
    plt.plot(Tg[lower:upper],label="Glue Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Silicon chip Temperature
    plt.subplot(10,1,5)
    plt.plot(Tsi[lower:upper],label="Silicon Chip Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot TES Electron and Phonon System Temperatures on same graph
    plt.subplot(10,1,6)
    plt.plot(Tte[lower:upper],label="TES Electron System Temperature")
    plt.plot(Ttp[lower:upper],label="TES Phonon System Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 2 Temperature
    plt.subplot(10,1,7)
    plt.plot(Tau2[lower:upper],label="Gold Pad 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Meander and Wirebond 2 Temperatures on same graph
    plt.subplot(10,1,8)
    plt.plot(Tm[lower:upper],label="Meander Temperature")
    #plt.plot(Twb2[lower:upper],label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
#plt.savefig('Nonlinear Solver 1KeV Split gmwb2_3e-8 11 July 2017 corrected Cs.png')



### Print eq values ###

print(I[999999])
print(Ta[999999])
print(Tau1[999999])
print(Tg[999999])
print(Twb1[999999])
print(Tau2[999999])
print(Tsi[999999])
print(Tte[999999])
print(Ttp[999999])
print(Tm[999999])
print(Twb2[999999])
print("resistance",resistance(Tte[999999],I[999999]))
print("power",resistance(Tte[999999],I[999999])*I[999999])

