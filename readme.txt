29 September 2019
-Doug Pinckney hdpinck@gmail.com

The basic start up for this code is to use "main_V2.py" and change the input file it takes.  Any input file with the format "input full * " will work.  For an explaination of what the other "main*.py" files mean check out the "Main_variations_explainations.txt" file as well as the file names themselves.

If you want to do some optimization with the code it helps to be able to run multiple processes in parallel.  This is done with gnu parallel, and instructions for using it are in the "main_V2.py" comments.  This outputs a file for each set of optimization parameters "1Optimization*.txt", and "result_combiner.py" takes the files listed in "file_names copy.txt" and turns them into one file for easy viewing.

