import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions

from numpy import linalg as LA #Import Linear Algebra
from numpy.linalg import inv #import inversion function
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object


### Electrical Components ###
R_L = 5000.                  #Load Resistor [Ohms]
R_tes_0 = 0.5              #Equilibium TES resistance [Ohms]
L = 1e-7                 #Circuit Inductance [Henries]
C_cap = 1e6               #Circuit Capacitance [Farads]
beta = 1.               #Current Sensitivity [Unitless]
P_0 = 5.19969028803e-09              #Equilibrium power through TES [J]
alpha = 100.             #Temperature Sensitivity [Unitless]
V_0 = 5.19615242271e-05            #Equilibrium Voltage [V]
T_Te_0 = 0.1           #Temperature of TES Electron System at equilibrium [K]

### Thermal Conductances ###
G_ab =  0.                #Conductance from Absorber to Bath [W/K]
G_aAu1 = 1.56e-5               #Conductance from Absorber to Gold Pad 1 [W/K]
G_ag = 1.6e-7          #Conductance from Absorber to Glue [W/K]
G_gSi = 1.6e-7             #Conductance from Glue to Silicon [W/K]
G_Au1wb1 = 1.396e-4            #Conductance from Gold Pad 1 to Wirebond 1 [W/K]
G_wb1Au2 = 1.396e-4           #Conductance from Wirebond 1 to Gold Pad 2 [W/K]
G_SiAu2 = 3.91e-8          #Conductance from Silicon Chip to Gold Pad 2 [W/K]
G_SiTp = 4.68e-8         #Conductance from Silicon Chip to TES phonon System [W/K]
G_TpTe = 1e-5        #Conductance from TES Phonon System to TES Electron System [W/K]
G_Au2Te = 1.1e-5       #Conductance from Gold Pad 2 to TES Electron System [W/K]
G_Tem = 1.1e-5      #Conductance from TES Electron System to Meander [W/K]
G_Sim = 3.91e-8     #Conductance from Silicon chip to Meander [W/K]
G_mwb2 = 3.0e-7    #Conductance from Meander to Wirebond 2 [W/K]
G_wb2b = 1.396e-4   #Conductance from Wirebond 2 to Bath [W/K]

### Heat Capacities ###
C_b = 10.                   #Heat Capacity of Bath [J/K]
C_a = 6.44e-10                  #Heat Capacity of Absorber [J/K]
C_g = 6.87e-11                 #Heat Capacity of Glue [J/K]
C_Au1 = 4.47e-11                #Heat Capacity of Gold Pad 1 [J/K]
C_Si = 1.93e-12               #Heat Capacity of Silicon Chip [J/K]
C_wb1 = 2.81e-10            #Heat Capacity of Wirebond 1 [J/K]
C_Au2 = 1.12e-13             #Heat Capacity of Gold Pad 2 [J/K]
C_Tp = 3.12e-13            #Heat Capacity of TES Phonon system [J/K]
C_Te = 3.41e-10           #Heat Capacity of TES Electron System [J/K]
C_m = 1.12e-13          #Heat Capacity of Meander [J/K]
C_wb2 = 2.81e-10         #Heat Capacity of Wirebond 2 [J/K]

### Enter the Matrix ###
N = np.array([[-R_tes_0*(1+beta)/L, -alpha*V_0/(T_Te_0*L),0,0,0,0,0,0,0,0,0],
             [(2+beta)*V_0/(C_Te),-(G_Au2Te+G_TpTe+G_Tem-alpha*P_0/T_Te_0)/C_Te,0,0,0,0,G_Au2Te/C_Te,0,G_TpTe/C_Te,G_Tem/C_Te,0],
             [0,0,-(G_ab+G_aAu1+G_ag)/C_a,G_aAu1/C_a,G_ag/C_a,0,0,0,0,0,0],
             [0,0,G_aAu1/C_Au1,-(G_aAu1+G_Au1wb1)/C_Au1,0,G_Au1wb1/C_Au1,0,0,0,0,0],
             [0,0,G_ag/C_g,0,-(G_ag+G_gSi)/C_g,0,0,G_gSi/C_g,0,0,0],
             [0,0,0,G_Au1wb1/C_wb1,0,-(G_Au1wb1+G_wb1Au2)/C_wb1,G_wb1Au2/C_wb1,0,0,0,0],
             [0,G_Au2Te/C_Au2,0,0,0,G_wb1Au2/C_Au2,-(G_Au2Te+G_wb1Au2+G_SiAu2)/C_Au2,G_SiAu2/C_Au2,0,0,0],
             [0,0,0,0,G_gSi/C_Si,0,G_SiAu2/C_Si,-(G_gSi+G_SiAu2+G_SiTp+G_Sim)/C_Si,G_SiTp/C_Si,G_Sim/C_Si,0],
             [0,G_TpTe/C_Tp,0,0,0,0,0,G_SiTp/C_Tp,-(G_TpTe+G_SiTp)/C_Tp,0,0],
             [0,G_Tem/C_m,0,0,0,0,0,G_Sim/C_m,0,-(G_Tem+G_Sim+G_mwb2)/C_m,G_mwb2/C_m],
             [0,0,0,0,0,0,0,0,0,G_mwb2/C_wb2,-(G_mwb2+G_wb2b)/C_wb2]])

### Solving the equations ###
Eig, P = LA.eig(N) # Compute eigenvalues (Eig) and eigenvectors (P) of M
P_1 = inv(P) # Compute inverse of the eigenvectors
#phi0 = np.array([2.5e-5,2.3e-5,1.295,2,1.295,1.295,1.295,1.295,1.295,1.295,1.295,1.295,1.295])  # Input pulse
phi0 = np.array([0,0,0.000001,0,0,0,0,0,0,0,0])  # Pulse to Absorber
#phi0 = np.array([0,0,0,0,0,0,0,0,1,0,0,0,0])
A = P_1.dot(phi0) #Dot the inverse of the eigenvector with the input pulse to get our coefficients

"""deltaT_Mat = P.dot(A)"""
tau = 1.0/Eig #get the time constants from the eigenvalues
print("time constants",tau)
t = np.linspace(0,0.03,num=300,endpoint=True) #create an array of times to evaluate our solutions at

    
exp_vec = map(lambda x,y: y*np.exp(t/x),tau,A) # Form the exponential basis terms
deltaT_exp = P.dot(exp_vec) # create a vector of the solutions

dI, dT_Te, dT_a, dT_Au1, dT_g, dT_wb1, dT_Au2, dT_Si, dT_Tp, dT_m, dT_wb2 = map(np.array, deltaT_exp) #assign solutions to arrays

### Plot The Results###

fig = plt.figure(figsize=(18, 18)) #make the plots bigger

#Plot absorber Temperature
plt.subplot(2,1,1)
plt.plot(dT_a,label="Absorber Temperature")
plt.plot(dT_Au1,label="Gold Pad 1 Temperature") 
plt.plot(dT_wb1,label="Wirebond 1 Temperature")
plt.plot(dT_g,label="Glue Temperature")
plt.plot(dT_Si,label="Silicon Chip Temperature")
plt.plot(dT_Te,label="TES Electron System Temperature")
plt.plot(dT_Tp,label="TES Phonon System Temperature")
plt.plot(dT_Au2,label="Gold Pad 2 Temperature")
plt.plot(dT_m,label="Meander Temperature")
plt.plot(dT_wb2,label="Wirebond 2 Temperature")
plt.ylabel("Temperature [K]")
plt.xlabel("Time [0.1 ms]")
plt.legend()
plt.title("Linear Solver")
fig.subplots_adjust(hspace=.5)

#Plot Current
plt.subplot(2,1,2)
plt.plot(dI,label="Current") 
plt.ylabel("Current [I]")
plt.xlabel("Time [0.1 ms]")
plt.legend()

#plt.savefig('Linear Solver Pulse to Si.png') #Save the figure to current directory
plt.show() #Display the figure
