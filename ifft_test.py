import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions
from numpy.linalg import inv #import inversion function
from numpy import linalg as LA #Import Linear Algebra
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object
from iminuit import Minuit
import yaml
from scipy.integrate import quad # Import integrator
from scipy import stats,special
from scipy.optimize import curve_fit

trace_num = 100
noise_trace_multiplier = 1.
percent_variance = 0.0
stdev_trace_num = 2000

stream = open("mock data generator.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')

#Import the raw pulse and the noise spectrum
data = np.load('NPZ of Linear Data Mock Pulse Data 1 march 2018.npz')
I_L = data['current']
t_L = data['time']
del data.f
data.close()

print(len(t_L))

data = np.load('NPZ of Frequency Data Mock Pulse Data 1 march 2018.npz')
noise_total = data['total']
print(len(noise_total))

for i in range(0,819):
    t_L = np.append(t_L,t_L[-1] + 0.00002441)

#Define a random generator for the noise traces
def get_rand():
    return np.random.random()

#Define a function to generate a single noise trace
def get_noise_trace():
    #Multiply each of the noise terms by a complex exponential, giving the noise a 
    #random phase at each bin
    data = np.load('NPZ of Frequency Data Mock Pulse Data 1 march 2018.npz')
    noise_total = data['total']
    del data.f
    data.close()
    noise_total = noise_total.astype(complex)
    for i in range(0,len(noise_total)):
        rand = get_rand()
        noise_total[i] = noise_total[i] * np.exp(1j*2*np.pi*rand)
    
    #Because the noise_total array only has the positive frequencies, append the 
    #negative frequencies in the proper place, and then insert the zero frequency 
    #term
    noise_total = np.append(noise_total,np.conj(np.flip(noise_total,0)))
    noise_total = np.insert(noise_total,0,0.)
    noise = np.fft.ifft(noise_total)
    hold = []
    for i in range(1,40961,10):
        hold.append(noise[i])
    noise = np.array(hold)
    return noise.real * noise_trace_multiplier
