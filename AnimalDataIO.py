from scipy.io import loadmat
import numpy

def getChannelsSingleFile(filename,verbose=False):
    if(verbose):
        print('Loading',filename)
    mat_data=loadmat(filename,squeeze_me=False)
    prop=mat_data['exp_prop']
    data=mat_data['data_post']

    Gains = numpy.array(prop['SRS'][0][0][0],dtype='f')
    Rfbs = numpy.array(prop['Rfb'][0][0][0],dtype='f')
    Turns = numpy.array(prop['turn_ratio'][0][0][0],dtype='f')
    chan_names = numpy.array(prop['chan_names'][0][0][0],dtype = 'str') #DP, adds channel names
    Fs = float(prop['sample_rate'][0][0][0])

    try:
        ttable=numpy.array([24*3600.0,3600.0,60.0,1.0])
        reltime = mat_data['t_rel_trig'].squeeze()
        abstime = mat_data['t_abs_trig'].squeeze()

        timestamp=abstime[:,2:].dot(ttable)+reltime
    except:
        timestamp = numpy.arange(0,len(data[:,:,0]))
        if(verbose):
            print('  No Timestamps Found')

    chanNum=len(Gains)
    if(chanNum > len(Rfbs)):
        Rfbs=numpy.append(Rfbs,1.0)
        Turns=numpy.append(Turns,1.0)
    
    dVdI=Turns*Rfbs*Gains
    dIdV=1.0/dVdI
    
    res=dict()
    for count in range(0,chanNum):
        res[chan_names[count]] = data[:,:,count] * dIdV
    
    res['dVdI']=dVdI
    res['Fs']=Fs
    res['prop']=prop
    res['filenum']=1
    res['time'] = timestamp
    res['chan_names'] = chan_names
    
    return res

def getChannels(filelist,verbose=False):
    
    if(type(filelist) == str):
        return getChannelsSingleFile(filelist,verbose=verbose)
    else:
        
        combined = dict()
        res1=getChannelsSingleFile(filelist[0],verbose=verbose)
        chan_names = res1['chan_names']
        for chan in range(0,len(chan_names)):
            combined[chan_names[chan]] = [res1[chan_names[chan]]]
        
        combined['dVdI']=res1['dVdI']
        combined['Fs']=res1['Fs']
        combined['prop']=res1['prop']
        combined['time']=[res1['time']]

        for count in range(1,len(filelist)):
            try:
                res = getChannelsSingleFile(filelist[count],verbose = verbose)
                for chan in range(0,len(chan_names)):
                    combined[chan_names[chan]].append(res[chan_names[chan]])
                    combined['time']/append(res['time'])
            except:
                print('Skipping ' + filelist[count])
                
        for chan in range(0,len(chan_names)):
            combined[chan_names[chan]] = numpy.concatenate(combined[chan_names[chan]])
        
        combined['time']=numpy.concatenate(combined['time'])
            
        print (combined['A'].shape)
        
        combined['filenum']=len(filelist)
        
        return combined

def loadDataset(filelist):
    traces=getChannels(filelist,verbose=True)
    
    chan_names = traces['prop']['chan_names']
    
    tlen=len(traces[chan_names[0]][0])
    
    dataset=dict()
    dataset['Pulses']=dict()
    dataset['Noise']=dict()
    dataset['Info']=dict()
    
    #chans=['A','B','Total']
    for chan in chan_names:
        dataset['Noise'][chan]=traces[chan][:,0:tlen/2]
        dataset['Pulses'][chan]=traces[chan][:,tlen/2:]
    
    for k in traces.keys():
        if k not in chan_names:
            dataset['Info'][k]=traces[k]
                        
    try:
        dataset['Info']['V_S1']=dataset['Info']['prop']['hvs1'][0][0][0][0]
        dataset['Info']['V_S2']=dataset['Info']['prop']['hvs2'][0][0][0][0]
        dataset['Info']['Voltage']=dataset['Info']['V_S2']-dataset['Info']['V_S1']
    except:
        print ('No Voltage Fields')
        dataset['Info']['V_S1']='NA'
        dataset['Info']['V_S2']='NA'
        dataset['Info']['Voltage']='NA'

    try:
        dataset['Info']['LaserRate']=dataset['Info']['prop']['ltr'][0][0][0]
    except:
        print ('No Laser Rate Field')
        dataset['Info']['LaserRate']='NA'

    try:
        dataset['Info']['LaserPulseWidth']=dataset['Info']['prop']['lwd'][0][0][0]
    except:
        print ('No Laser PW Field')
        dataset['Info']['LaserPulseWidth']='NA'

    try:
        dataset['Info']['LaserPower']=dataset['Info']['prop']['lpk'][0][0][0]
    except:
        print ('No Laser Power Field')
        dataset['Info']['LaserPower']='NA'

    try:
        dataset['Info']['LaserAttenuation']=dataset['Info']['prop']['laseratten'][0][0][0][0]
    except:
        print ('No Attenuation Field')
        dataset['Info']['LaserAttenuation']='NA'
            
    try:
        dataset['Info']['TriggerThreshold']=dataset['Info']['prop']['threshtrig'][0][0][0][0]
    except:
        print ('No Trigger Threshold Field')
        dataset['Info']['TriggerThreshold']='NA'

    try:
        dataset['Info']['Temp']=dataset['Info']['prop']['T_MC'][0][0][0][0]
    except:
        print ('No Temperature Field')
        dataset['Info']['Temp']='NA'

    return dataset