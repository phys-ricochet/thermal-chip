import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
from math import * 
from scipy import integrate
import inspect, os
import scipy as sp
import sys, traceback
from pylab import *
matplotlib.rc('axes.formatter', useoffset=False)

eq = [3.07580471e-06,   1.98915954e-02,   1.98915964e-02,   1.98915953e-02,1.98915964e-02,   1.98915964e-02,   1.98915952e-02,   1.98915964e-02,1.98914588e-02,   1.00013588e-02]

data = np.load('NPZ of Nonlinear Data Mock Data.npz')
I = data['current']
Ta = data['absorber_temp']
Tau1 = data['gold_pad_1_temp']
Tg = data['glue_temp']
Twb1 = data['wirebond_1_temp']
Tau2 = data['gold_pad_2_temp']
Tsi = data['silicon_temp']
Tte = data['tes_electron_temp']
Tm = data['meander_temp']
Twb2 = data['wirebond_2_temp']
t = data['time']
del data.f
data.close()

data = np.load('NPZ of Linear Data Mock Data.npz')
I_L = data['current']
Ta_L = data['absorber_temp']
Tau1_L = data['gold_pad_1_temp']
Tg_L = data['glue_temp']
Twb1_L = data['wirebond_1_temp']
Tau2_L = data['gold_pad_2_temp']
Tsi_L = data['silicon_temp']
Tte_L = data['tes_electron_temp']
Tm_L = data['meander_temp']
Twb2_L = data['wirebond_2_temp']
t_L = data['time']
del data.f
data.close()

data = np.load('NPZ of Frequency Data Mock Data.npz')
#print(np.max(Ta)-np.min(Ta))
### Make Some Plots ###

fig = plt.figure(figsize=(18,18)) #Make the plots biger 

lower = 133
upper = 1600
lin_upper = 2200

adjusted_t = 1e3*(t[lower:upper] - 0.002)
adjusted_t_L = 1e3*t_L
plt.subplots_adjust(bottom = 0)

#Plot Current
plt.subplot(11,1,1)
plt.plot(adjusted_t, 1e6*I[lower:upper],label="Current Nonlinear",color = '#126011',linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(I_L[0:lin_upper] + eq[0]),label = "Current Linear",color = '#bf159a',linewidth = 2,ls=':') 
plt.ylabel(r'Current [$\mu$A]',fontsize = 15)
plt.title("Model of a 100 keV Input to the Target",fontsize = 30,y=1.25)
plt.legend(fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot absorber Temperature
plt.subplot(10,1,2)
plt.plot(adjusted_t, 1e6*(Ta[lower:upper] - eq[1]),label="Target Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Ta_L[0:lin_upper]),label = "Target Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Gold Pad 1 Temperature
plt.subplot(10,1,3)
plt.plot(adjusted_t, 1e6*(Tau1[lower:upper] - eq[2]),label="Gold Pad 1 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Tau1_L[0:lin_upper]),label = "Gold Pad 1 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Wirebond 1 Temperature
plt.subplot(10,1,4)
plt.plot(adjusted_t, 1e6*(Twb1[lower:upper] - eq[4]),label="Wirebond 1 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Twb1_L[0:lin_upper]),label = "Wirebond 1 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off
        
#Plot Glue Temperature on same graph
plt.subplot(10,1,5)
plt.plot(adjusted_t, 1e6*(Tg[lower:upper] - eq[3]),label="Glue Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Tg_L[0:lin_upper]),label = "Glue Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Silicon chip Temperature
plt.subplot(10,1,6)
plt.plot(adjusted_t, 1e6*(Tsi[lower:upper] - eq[6]),label="Silicon Wafer Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Tg_L[0:lin_upper]),label = "Silicon Wafer Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot TES Electron System Temperature
plt.subplot(10,1,7)
plt.plot(adjusted_t, 1e6*(Tte[lower:upper] - eq[7]),label="TES Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Tte_L[0:lin_upper]),label = "TES Linear",linewidth = 2,ls=':')  
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Gold Pad 2 Temperature
plt.subplot(10,1,8)
plt.plot(adjusted_t, 1e6*(Tau2[lower:upper] - eq[5]),label="Gold Pad 2 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Tau2_L[0:lin_upper]),label = "Gold Pad 2 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Meander Temperature
plt.subplot(10,1,9)
plt.plot(adjusted_t, 1e6*(Tm[lower:upper] - eq[8]),label="Meander Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Tm_L[0:lin_upper]),label = "Meander Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.legend(fontsize = 15)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

#Plot Wirebond 2 Temperature
plt.subplot(10,1,10)
plt.plot(adjusted_t, 1e6*(Twb2[lower:upper] - eq[9]),label="Wirebond 2 Nonlinear", linewidth = 2)
plt.plot(adjusted_t_L[0:lin_upper], 1e6*(Twb2_L[0:lin_upper]),label = "Wirebond 2 Linear",linewidth = 2,ls=':') 
plt.ylabel(r'$\Delta$T [$\mu$K]',fontsize = 15)
plt.tick_params(axis = 'y',labelsize = 15)
plt.tick_params(axis = 'x',labelsize = 15)
plt.xlabel("Time [ms]",fontsize = 15)
plt.legend(fontsize = 15)

plt.savefig('100 KeV Pulse.jpg')

plt.show()
