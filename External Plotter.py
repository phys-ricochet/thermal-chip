import matplotlib.pyplot as plt # Import Plotting 
import numpy as np #import numerical functions
np.set_printoptions(threshold=np.nan) #make it so print returns the entire object
import yaml
import matplotlib
matplotlib.rc('axes.formatter', useoffset=False)
from scipy.optimize import curve_fit

stream = open("input full 20mK optimum.txt", "r")  #Open the file
p = yaml.load(stream)           #read the file to a dictionary
stream.close()                  #Close the file
for v in p.values():            #Check to make sure that all dictionary entries are floats and raise an error if they are not
    if type(v) != float:
        raise ValueError('An input was not a float!')

npzname = '1cm3 Si Absorber'
linear_flag = 0
nonlinear_flag = 0
freq_flag = 1
save_fig_flag = 1
savename = "LTD Proceedings"

kmwb2       = p['G_mwb2']/(p['nmwb2']*p['Tc']**(p['nmwb2']-1.))                 #Conductance Constant Meander to Wirebond 2 [W/K^2]
R0          = p['Rn']  * p['frac']                       #Equilibrium resistance of TES [Ohms]
P_0         = p['I0_NL']**2* R0                      #Equilibrium power through TES [J]
V_0         = p['I0_NL']* R0                         #Equilibrium Voltage [V]
P0          = kmwb2 *((p['Tc'])**2-(p['Tb'])**2)    #Equilibrium Power, calculated by the most restrictive connection between TES Electron System and Bath
I0_P        = np.sqrt( P0 / R0 )                  #Equilibrium Current calculated from power, used in resistance equation

def resistance(Ttes,Ites):
    R = p['Rn']/2 *(1 + np.tanh((Ttes - p['Tc']) * p['alpha0']/p['Tc'] + (Ites - I0_P)*p['beta0'] / I0_P))
    return R

def exponential(x, N, tau, a):
    val = N*np.exp(-x/tau)+a
    return val

data = np.load('NPZ of Nonlinear Data ' + npzname + '.npz')
I = data['current']
Ta = data['absorber_temp']
Tau1 = data['gold_pad_1_temp']
Tg = data['glue_temp']
Twb1 = data['wirebond_1_temp']
Tau2 = data['gold_pad_2_temp']
Tsi = data['silicon_temp']
Tte = data['tes_electron_temp']
Tm = data['meander_temp']
Twb2 = data['wirebond_2_temp']
ODEt = data['time']
del data.f
data.close()

data = np.load('NPZ of Linear Data ' + npzname + '.npz')
dI = data['current']
dTa = data['absorber_temp']
dTau1 = data['gold_pad_1_temp']
dTg = data['glue_temp']
dTwb1 = data['wirebond_1_temp']
dTau2 = data['gold_pad_2_temp']
dTsi = data['silicon_temp']
dTte = data['tes_electron_temp']
dTm = data['meander_temp']
dTwb2 = data['wirebond_2_temp']
LINt = data['time']
del data.f
data.close()

data = np.load('NPZ of Frequency Data ' + npzname + '.npz')
array_eint = data['eint']
array_eext = data['eext']
array_pau2te = data['pau2te']
array_ptem = data['ptem']
array_psite = data['psite']
array_pag = data['pag']
array_paau1 = data['paau1']
array_pab = data['pab']
array_pulse = data['pulse']
array_pgsi = data['pgsi']
array_pau1wb1 = data['pau1wb1']
array_pwb1au2 = data['pwb1au2']
array_psiau2 = data['psiau2']
array_psim = data['psim']
array_pmwb2 = data['pmwb2']
array_pwb2b = data['pwb2b']
array_total = data['total']
frequencies = data['freqs']
del data.f
data.close()


ODEt = np.array(ODEt) #make sure that the nonlinear solver times are in an array



if linear_flag == 1:
    fig = plt.figure(figsize=(18, 18)) #make the plots bigger

    #Plot absorber Temperature
    plt.subplot(2,1,1)
    #plt.plot(LINt,1e6*dTa,label="Absorber Temperature")
    #plt.plot(LINt,1e6*dTau1,label="Gold Pad 1 Temperature") 
    #plt.plot(LINt,1e6*dTwb1,label="Wirebond 1 Temperature")
    #plt.plot(LINt,1e6*dTg,label="Glue Temperature")
    #plt.plot(LINt,1e6*dTsi,label="Silicon Chip Temperature")
    plt.plot(LINt[0:1300],1e6*dTte[0:1300],label="TES Electron System Temperature")
    #plt.plot(LINt,1e6*dTau2,label="Gold Pad 2 Temperature")
    #plt.plot(LINt,1e6*dTm,label="Meander Temperature")
    #plt.plot(LINt,1e6*dTwb2,label="Wirebond 2 Temperature")
    plt.ylabel("Change in Temperature [$\mu$K]")
    plt.xlabel("Time [s]")
    plt.legend()
    plt.title("Linear Solver")
    """
    #Plot Current
    plt.subplot(2,1,2)
    plt.plot(LINt,dI,label="Current") 
    plt.ylabel("Current [A]")
    plt.xlabel("Time [10 $\mu$s]")
    plt.legend()
    """
    if save_fig_flag == 1:
        namepng = 'linear ' + str(savename) + '.png'   
        plt.savefig(namepng) #Save the figure to current directory
        nameeps = 'linear ' + str(savename) + '.eps'   
        plt.savefig(nameeps) #Save the figure to current directory

    plt.show() #Display the figure

if nonlinear_flag == 1:
    fig = plt.figure(figsize=(18, 18)) #make the plots bigger

    ### Linear Plot ###
    
    #Plot temperatures
    plt.subplot(2,1,1)
    #plt.plot(ODEt - p['t0'],1e3*Ta,label="Absorber Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tau1,label="Gold Pad 1 Temperature") 
    #plt.plot(ODEt - p['t0'],1e3*Twb1,label="Wirebond 1 Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tg,label="Glue Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tsi,label="Silicon Chip Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tau2,label="Gold Pad 2 Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Tm,label="Meander Temperature")
    #plt.plot(ODEt - p['t0'],1e3*Twb2,label="Wirebond 2 Temperature")
    plt.plot(ODEt[133:1000] - p['t0'],1e3*Tte[133:1000],label="TES Electron System Temperature",color = "k")
    plt.ylabel("Temperature [mK]")
    plt.xlabel("Time [s]")
    plt.title("Nonlinear Solver")
    plt.legend()
    """
    #Plot Current
    plt.subplot(5,1,2)
    plt.plot(ODEt - p['t0'],I,label="Current") 
    plt.ylabel("Current [A]")
    #plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    
    plt.subplot(5,1,3)
    plt.plot(ODEt - p['t0'],resistance(Tte,I),label = "Resistance")
    plt.ylabel("Resistance [Ohms]")
    #plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #plot power
    plt.subplot(5,1,4)
    plt.plot(ODEt - p['t0'],(I**2)*resistance(Tte,I),label = "Power")
    plt.ylabel("Power [Watts]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    """
    if save_fig_flag == 1:
        namepng = 'nonlinear ' + str(savename) + '.png'   
        plt.savefig(namepng) #Save the figure to current directory
        nameeps = 'nonlinear ' + str(savename) + '.eps'   
        plt.savefig(nameeps) #Save the figure to current directory

    plt.show() #Display the figure
    
if nonlinear_flag == 2:
    fig = plt.figure(figsize=(18, 18)) #make the plots bigger

    ### Plot mostly individually ###
    
    #Plot absorber Temperature
    plt.subplot(10,1,1)
    plt.plot(ODEt - p['t0'],Ta,label="Absorber Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    plt.title("Nonlinear Solver")
    fig.subplots_adjust(hspace=.5)
    
    #Plot Current
    plt.subplot(10,1,2)
    plt.plot(ODEt - p['t0'],I,label="Current") 
    plt.ylabel("Current [A]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 1 Temperature and Wirebond 1 Temperature on same graph
    plt.subplot(10,1,3)
    plt.plot(ODEt - p['t0'],Tau1,label="Gold Pad 1 Temperature") 
    plt.plot(ODEt - p['t0'],Twb1,label="Wirebond 1 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Glue Temperature on same graph
    plt.subplot(10,1,4)
    plt.plot(ODEt - p['t0'],Tg,label="Glue Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Silicon chip Temperature
    plt.subplot(10,1,5)
    plt.plot(ODEt - p['t0'],Tsi,label="Silicon Chip Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot TES Electron and Phonon System Temperatures on same graph
    plt.subplot(10,1,6)
    plt.plot(ODEt - p['t0'],Tte,label="TES Electron System Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Gold Pad 2 Temperature
    plt.subplot(10,1,7)
    plt.plot(ODEt - p['t0'],Tau2,label="Gold Pad 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    #Plot Meander and Wirebond 2 Temperatures on same graph
    plt.subplot(10,1,8)
    plt.plot(ODEt - p['t0'],Tm,label="Meander Temperature")
    plt.plot(ODEt - p['t0'],Twb2,label="Wirebond 2 Temperature")
    plt.ylabel("Temperature [K]")
    plt.xlabel("Time [" + str(p['stoptime']/p['numpoints']) + " s]")
    plt.legend()
    
    if save_fig_flag == 1:
         name = 'nonlinear' + str(savename) + '.png'   
         plt.savefig(name) #Save the figure to current directory
    plt.show() #Display the figure

if freq_flag == 1:
    fig = plt.figure(figsize=(7, 6)) #make the plots bigger
                
    plt.loglog(frequencies,array_eint,label = 'Internal Johnson Noise',linestyle = "solid",linewidth = 3, color = 'navy')
    plt.loglog(frequencies,array_eext,label = 'External Johnson Noise',linestyle = "solid",linewidth = 3, color = 'olive')
    plt.loglog(frequencies,array_psite,label = 'TFN Silicon Wafer-TES',linestyle = ":",linewidth = 3, color = 'navy')
    plt.loglog(frequencies,array_pgsi,label = 'TFN Glue-Silicon Wafer',linestyle = ":",linewidth = 3, color = 'olive')
    plt.loglog(frequencies,array_psiau2,label = 'TFN Silicon Wafer-Gold Pad 2',linestyle = ':',linewidth = 3, color = 'skyblue')
    plt.loglog(frequencies,array_pag,label = 'TFN Target-Glue',linestyle = ":",linewidth = 3, color = 'blanchedalmond')
    plt.loglog(frequencies,array_pau2te,label = 'TFN Gold Pad 2-TES',linestyle = "--",linewidth = 3, color = 'navy')
    plt.loglog(frequencies,array_paau1,label = 'TFN Target-Gold Pad 1',linestyle = "--",linewidth = 3, color = 'olive')
    plt.loglog(frequencies,array_pau1wb1,label = 'TFN Gold Pad 1-Wirebond 1',linestyle = "--",linewidth = 3, color = 'skyblue')
    plt.loglog(frequencies,array_pwb1au2,label = 'TFN Wirebond 1-Gold Pad 2',linestyle = '--',linewidth = 3, color = 'blanchedalmond')
    plt.loglog(frequencies,array_ptem,label = 'TFN TES-Meander',linestyle = "-.",linewidth = 3, color = 'navy')
    plt.loglog(frequencies,array_psim,label = 'TFN Silicon Wafer-Meander',linestyle = '-.',linewidth = 3, color = 'olive')
    plt.loglog(frequencies,array_pmwb2,label = 'TFN Meander-Wirebond 2',linestyle = '-.',linewidth = 3, color = 'skyblue')
    plt.loglog(frequencies,array_pwb2b,label = 'TFN Wirebond 2-Bath',linestyle = '-.',linewidth = 3, color = 'blanchedalmond')
    plt.loglog(frequencies,array_total,label = 'Total Noise',linewidth = 6,color = 'k')
    axes = plt.gca()
    axes.set_ylim([1e-21,1e-11])
    axes.set_xlim([1e-0,1e7])
    plt.tick_params(axis = 'y',labelsize = 15)
    plt.tick_params(axis = 'x',labelsize = 15)
    plt.title("Frequency vs Noise", fontsize = 25)
    plt.ylabel("Noise [A/$\sqrt{Hz}$]",fontsize = 20)
    plt.xlabel("Frequency [Hz]",fontsize = 20)
    box = axes.get_position()
    axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    axes.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 15)
    plt.grid()
    if save_fig_flag == 1:
        namepng = 'Frequency Spectrum ' + str(savename) + '.png'   
        plt.savefig(namepng) #Save the figure to current directory
        nameeps = 'Frequency Spectrum ' + str(savename) + '.eps'   
        plt.savefig(nameeps) #Save the figure to current directory
    plt.show()
    
if freq_flag == 2:
    fig = plt.figure(figsize=(6, 6)) #make the plots bigger
                
    plt.loglog(frequencies,array_eint,label = 'Internal Johnson Noise NEP',linestyle = "solid",linewidth = 2, color = 'navy')
    plt.loglog(frequencies,array_eext,label = 'External Johnson Noise NEP',linestyle = "solid",linewidth = 2, color = 'olive')
    plt.loglog(frequencies,array_psite,label = 'TFN Silicon Wafer-TES NEP',linestyle = ":",linewidth = 2, color = 'navy')
    plt.loglog(frequencies,array_pgsi,label = 'TFN Glue-Silicon Wafer NEP',linestyle = ":",linewidth = 2, color = 'olive')
    plt.loglog(frequencies,array_psiau2,label = 'TFN Silicon Wafer-Gold Pad 2 NEP',linestyle = ':',linewidth = 2, color = 'skyblue')
    plt.loglog(frequencies,array_pag,label = 'TFN Target-Glue NEP',linestyle = ":",linewidth = 2, color = 'blanchedalmond')
    plt.loglog(frequencies,array_pau2te,label = 'TFN Gold Pad 2-TES NEP',linestyle = "--",linewidth = 2, color = 'navy')
    plt.loglog(frequencies,array_paau1,label = 'TFN Target-Gold Pad 1 NEP',linestyle = "--",linewidth = 2, color = 'olive')
    plt.loglog(frequencies,array_pau1wb1,label = 'TFN Gold Pad 1-Wirebond 1 NEP',linestyle = "--",linewidth = 2, color = 'skyblue')
    plt.loglog(frequencies,array_pwb1au2,label = 'TFN Wirebond 1-Gold Pad 2 NEP',linestyle = '--',linewidth = 2, color = 'blanchedalmond')
    plt.loglog(frequencies,array_ptem,label = 'TFN TES-Meander NEP',linestyle = "-.",linewidth = 2, color = 'navy')
    plt.loglog(frequencies,array_psim,label = 'TFN Silicon Wafer-Meander NEP',linestyle = '-.',linewidth = 2, color = 'olive')
    plt.loglog(frequencies,array_pmwb2,label = 'TFN Meander-Wirebond 2 NEP',linestyle = '-.',linewidth = 2, color = 'skyblue')
    plt.loglog(frequencies,array_pwb2b,label = 'TFN Wirebond 2-Bath NEP',linestyle = '-.',linewidth = 2, color = 'blanchedalmond')
    plt.loglog(frequencies,array_total,label = 'Total NEP',linewidth = 6,color = 'k')
    axes = plt.gca()
    axes.set_ylim([1e-21,1e-8])
    axes.set_xlim([1e-0,1e7])
    plt.tick_params(axis = 'y',labelsize = 15)
    plt.tick_params(axis = 'x',labelsize = 15)
    plt.title("Frequency vs NEP", fontsize = 25)
    plt.ylabel("NEP [W/$\sqrt{Hz}$]",fontsize = 20)
    plt.xlabel("Frequency [Hz]",fontsize = 20)
    box = axes.get_position()
    axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    axes.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 15)
    plt.grid()
    if save_fig_flag == 1:
        namepng = 'NEP Spectrum ' + str(savename) + '.png'   
        plt.savefig(namepng) #Save the figure to current directory
        nameeps = 'NEP Spectrum ' + str(savename) + '.eps'   
        plt.savefig(nameeps) #Save the figure to current directory
    plt.show()
